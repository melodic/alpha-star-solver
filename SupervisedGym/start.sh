#!/bin/bash

# NOTE:
#   in order to stop whole app both processes must be killed
#   when only one of them will be killed app will not be operating
#   correctly without stopping the container
# TODO:
#   write script that stops both processes when either
#   of them stops (ex. python subproccess)

python3 -m celery -A gym_server.worker.celery_app worker --loglevel=INFO &
python3 -m uvicorn gym_server.app:app --host 0.0.0.0 --port 80
