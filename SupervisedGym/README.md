# SupervisedGym
> SupervisedGym is one of components in ML based proactive solver. Its role is to create and invoke ML based solvers given training data and constraint problem definition.

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Docker](#docker)
* [Usage](#usage)
    * [Configuration](#usage)
        * [Enviroment variables](#required-enviroment-variables)
        * [Training configuration](#training-configuration-anchor)
    * [Running docker image](#running-cpu-docker-image-anchor)
    * [Testing](#running-tests-inside-docker-container)
* [Room for Improvement](#room-for-improvement)
* [Contact](#contact)


## General Information
- This component is meant to automaticly train ML models and use them as solvers in melodic enviroment.
- This component needs to be connected to two other services. ModelDB and Controller.
- This component can be depoloyed as single service using Docker. In this directory there are two prepared Dockerfile's:
    - [Dockerfile.cpu](./Dockerfile.cpu) for deploying this service with ml training on CPU's.
    - [Dockerfile.cuda11.1](./Dockerfile.cuda11.1) for deploying this service with training on GPU's with cuda 11.1 drivers supported. [notes on running this service with CUDA](#running-gpu-docker-image-anchor)


## Technologies Used
 - Whole component is written in python 3.8.9
 - ML pipeline is created with support of:
    - [pytorch-lightning](https://www.pytorchlightning.ai/) for training models
    - [optuna](https://optuna.org/) for hyperparameter optimization
    - [scikit-learn](https://scikit-learn.org/stable/) and [pandas](https://pandas.pydata.org/) for data preprocessing and handling
 - [FastAPI](https://fastapi.tiangolo.com/) for REST API implementation
 - [Celery](https://docs.celeryproject.org/en/stable/) for Distributed Task Queue support
 - [pytest](https://github.com/pytest-dev/pytest/) for unittesting


## Features
- Generating new ML based solvers given training data and constraint problem from modelDB and saving them to modelDB.
- Using already generated solvers saved in modelDB to predict future app configurations.

## Docker
### Build docker image of component
 1. make sure you have docker installed
 2. run:

        $ docker build <path-to-this-directory> -f Dockerfile.cpu
    
    If you want to build version with ml training using only CPU's.

    If you want to build version with ml training using GPU's run:

        $ docker build <path-to-this-directory> -f Dockerfile.cuda11.1
    
    note that this GPU version needs more support [learn more in usage section](#usage)
    
## Usage
This component must be configured using enviroment variables and training_config.json file

### Required enviroment variables

#### RABBITMQ_USERNAME
The value of variable `RABBITMQ_USERNAME` specifies username of rabbitmq instance used as task queue.

Example:

    RABBITMQ_USERNAME=guest

### RABBITMQ_HOST
The value of variable `RABBITMQ_HOST` specifies host addres of rabbitmq instance used as task queue.

Example:

    RABBITMQ_HOST=localhost

### RABBITMQ_PASSWORD
The value of variable `RABBITMQ_PASSWORD` specifies password of rabbitmq instance used as task queue.

Example:

    RABBITMQ_PASSWORD=supersecret

### RABBITMQ_PORT
The value of variable `RABBITMQ_PORT` specifies specifies the port on which a rabbitmq instance is working.

Example:

    RABBITMQ_PORT=5672

### GROUP_ID
The value of variable `GROUP_ID` specifies name of column in training data that identifies from which experiment data comes from.

Example:

    GROUP_ID=experimentId

### PREDICTION_HORIZON
The value of variable `PREDICTION_HORIZON` specifies how much into the future we want the solver to predict.  
The actual horizon time (in seconds) is `PREDICTION_HORIZON` * `TIMESTAMP_DIFFERENCE`.

Example:

    PREDICTION_HORIZON=10

### DATA_SPLIT
The value of variable `DATA_SPLIT` specifies percentages of data in consecutively train, validation and test data sets. Each percentage should be seperated by comma.

Example representing 70% of data in train, 15% in validation and 15% in test data sets:

    DATA_SPLIT=70,15,15


### LOG_LEVEL
The value of variable `LOG_LEVEL` specifies level of logging. Available options consecutively from lowest to highest:
 1. NOTSET
 2. DEBUG
 3. INFO
 4. WARNING
 5. ERROR
 6. CRITICAL

Example:

    LOG_LEVEL=INFO

### TRAINING_CONFIG_FILE
The value of variable `TRAINING_CONFIG_FILE` specifies path to `training_config.json` inside the container.

Example:

    TRAINING_CONFIG_FILE=./training_config.json

---
### Optional enviroment variables

### MODELDB_HOST
The value of variable `MODELDB_HOST` specifies host addres of modelDB instance.
Defaults to: localhost

Example:

    MODELDB_HOST=localhost

### MODELDB_PORT
The value of variable `MODELDB_PORT` specifies the port on which of modelDB instance is working.
Defaults to: 27018

Example:

    MODELDB_PORT=27018

---
### <a name="training-configuration-anchor"></a> training_config.json
This file describes ML training and solver generation related configurations in json format.
main json object in this file should contain those attributes:

### models
This attribute specifies resources assigned to each type of trained model architecture.
Value of this attribute should be list containing resources objects.

Each resource object contains those attributes:

### model_type
The value of attribute `model_type` specifies to which model architecture `timeout` and `n_trials` attributes are applied.
There are 3 allowed values of this attribute. Each corresponds to another implementing of ml architecture.

| model_type value | model architecture               |
|------------------|----------------------------------|
| FeedForward      | FeedForward                      |
| StackedRNN       | StackedRNN                       |
| Transformer      | Attention based                  |

### timeout
The value of attribute `timeout` specifies maximal training time in seconds.

### n_trials
The value of attribute `n_trials` specifies number of model trainings performed in order to tune hyperparameters. Real number of trainings might be lower then `n_trials` if training time exceeds `timeout`.

Example of resource object representing that `FeedForward` architecture will be trained for maximally 5 hours, and pipeline will maximally go throught 200 training sessions of this architecture.
Note: (18000 seconds = 5 hours) 

```json
{
    "model_type": "FeedForward",
    "timeout": 18000,
    "n_trials": 200
}
```

### ensemble_size
The value of attribute `ensemble_size` specifies number of ml models from which ensembly will be generated.

### loss_to_weight
### This attribute introduces potential for malicious code executions be careful with it
The value of attribute `loss_to_weight` specifies parsable python code, which should represent function that takes:
 1. ml model validation loss as first argument (float)
 2. all ml models taken to ensemble losses on validation set (list of floats)
And should return weight of model in created ensemble which validation loss is given in first argument.
The bigger the weight the more important the model will be in ensemble. You shouldn't worry about numerical errors weights are then scaled to range (0, 1]

Example of `loss_to_weight` value which will produce ensemble with all models with same importance:

    "lambda x, y: 1"

Example of `loss_to_weight` value which was mostly used in development of this component:

    "lambda x, y: 1 / (x + 1)"


### fast_dev_run
The value of attribute `fast_dev_run` specifies if mock training is performed. If set to true, no real models are created and training is instant. If not debugging should be set to `false`.

### shuffle_data
The value of attribute `shuffle_data` specifies if data groups in trainig and validation are shuffled. If not debugging or trying reduce indeterminism should be set to `true`.

---

Example of `training_config.json` file:

```json
{
    "models": [
            {
                "model_type": "FeedForward",
                "timeout": 3600,
                "n_trials": 2
            },
            {
                "model_type": "StackedRNN",
                "timeout": 3600,
                "n_trials": 2
            },
            {
                "model_type": "Transformer",
                "timeout": 3600,
                "n_trials": 1
            }
    ],
    "ensemble_size": 5,
    "loss_to_weight": "lambda x, y: 1 / (1 + x)",
    "fast_dev_run": true,
    "shuffle_data": true
}
```

---

### <a name="running-cpu-docker-image-anchor"></a> Running docker image (CPU training version)
After [building](#docker) docker image, run:
```sh
$ docker run --env-file <path-to-enviroment-variables-file> --expose 80:<port-on-which-you-want-to-expose-service>
```

### <a name="running-gpu-docker-image-anchor"></a> Running docker image (GPU training version)
First of all make sure you have [NVIDIA container runtime](https://docs.nvidia.com/deeplearning/frameworks/user-guide/index.html#installdocker) installed.  
After [building](#docker) docker image, run:
```sh
$ docker run --privileged --gpus­ all --env-file <path-to-enviroment-variables-file> --expose 80:<port-on-which-you-want-to-expose-service>
```
more info about [using docker with GPU acceleration](https://docs.nvidia.com/deeplearning/frameworks/user-guide/index.html#nvcontainers)


### Running tests inside docker container
To run tests inside docker container run:
```sh
$ docker exec -it <container-id> bash
```
navigate to `/app` and run:
```sh
$ pytest
```

Additionaly to test if container created from GPU supported image is working properly run:
```sh
$ nvidia-smi
```
should result in a console output shown below:
```
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 450.51.06    Driver Version: 450.51.06    CUDA Version: 11.0     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  Tesla T4            On   | 00000000:00:1E.0 Off |                    0 |
| N/A   34C    P8     9W /  70W |      0MiB / 15109MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+

+-----------------------------------------------------------------------------+
| Processes:                                                                  |
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
|        ID   ID                                                   Usage      |
|=============================================================================|
|  No running processes found                                                 |
+-----------------------------------------------------------------------------+
```

and assert that this script runs without error:
```sh
$ python -c "import torch as t; assert(t.cuda.is_available()); t.cuda.current_device()"
```
more information about testing [GPU supported docker](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#getting-started)

# Room for Improvement
 1. It might be beneficial to choose model weights in ensemble, performing optuna hyperparameter search study istead of using [loss_to_weight](#loss_to_weight) attribute.
    * It would decrease complexity of configuration
    * Reduce risk of malitious code execution
    * Possibly increase performance of generated ensemble
 2. It might be beneficial to set `seq_len` hyperparamter in configuration.
    * It would simplify using whole solver app, by knowing in advance amount of data needed by solvers to make prediction.
    * It could worsen generated model performance
 3. Using app utility function as loss function in model training
    * It could skyrocket performance of generated solvers
    * It would require significant rebuild of pipeline
    * It would require parsing app utility functions to high-performance GPU computing supported form [this library might be helpful](#https://jax.readthedocs.io/en/latest/)
 4. Model training could be distributed on multiple machines. Both celery and optuna (used libraries) support such approach.
 5. Adding more robust architectures of ml models. (eg. NBeats, DeepAR, TemporalFusionTransformer)
 6. Supporting retraing modeles instead of training new ones from scratch.
    * Using for example Transfer Learning

# Contact
Authors:
 1. Szymon Sadkowski, email: szymon.sadkow@gmail.com
 2. Wojciech Szymański, email: wojciech.szymanski@students.mimuw.edu.pl
