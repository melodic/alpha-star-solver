import logging
from environs import Env
from marshmallow.validate import OneOf
from dataclasses import dataclass
from SupervisedGym.utils.data import Train_val_test_perc
from .training_config import get_training_config, TrainingConfig


@dataclass(frozen=False)
class Config:
    """
    Wrapper for configuration data from .env
    and training_config.json
    """

    rabbitmq_username: str
    rabbitmq_password: str
    rabbitmq_host: str
    rabbitmq_port: int
    group_id: str
    prediction_horizon: int
    data_split: Train_val_test_perc
    log_level: int
    training_config: TrainingConfig


def _new_data_split(split: Train_val_test_perc) -> Train_val_test_perc:
    """
    Given global data split returns new data split.
    This function is necessary becouse SupervisedGym service
    is given from modeldb only train + validation data,
    and testing in handled outside of this service
    (by predict endpoint).
    So we have to adjust train and validation size
    in creating local TimeSeriesDataset

    NOTE:
        if prevoius split was train, val, test where
        train + val + test = 100. Then new split is
        round(train / (train + val)), 100 - round((train / (train + val))), 0
    """

    train = round(split.train / (split.train + split.validate))
    return Train_val_test_perc(train, 100 - train, 0)


logging.info("starting to search for config parameters")
env = Env()
env.read_env()

config = Config(
    rabbitmq_username=env.str("RABBITMQ_USERNAME"),
    rabbitmq_host=env.str("RABBITMQ_HOST"),
    rabbitmq_password=env.str("RABBITMQ_PASSWORD"),
    rabbitmq_port=env.int("RABBITMQ_PORT"),
    group_id=env.str("GROUP_ID"),
    prediction_horizon=env.int("PREDICTION_HORIZON"),
    data_split=_new_data_split(
        Train_val_test_perc(*env.list("DATA_SPLIT", subcast=int))
    ),
    log_level=getattr(
        logging,
        env.str(
            "LOG_LEVEL",
            validate=OneOf(
                ["NOTSET", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
                error='"LOG_LEVEL" must be one of: {choices}',
            ),
        ),
    ),
    training_config=get_training_config(env.path("TRAINING_CONFIG_FILE")),
)
logging.info("succesfully searched and parsed config parameters")
