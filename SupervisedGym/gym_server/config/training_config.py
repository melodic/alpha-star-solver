import logging
import json
import sys
import jsonschema
from dataclasses import dataclass
from pathlib import Path
from typing import Dict, Type, Callable, List
from SupervisedGym.models import (
    IBaseTunableModel,
    FeedForwardTunable,
    StackedRNNTunable,
    TransformerTunable,
)
from SupervisedGym.utils import ModelResources


_MODEL_NAME_MAPPING = {
    "FeedForward": FeedForwardTunable,
    "StackedRNN": StackedRNNTunable,
    "Transformer": TransformerTunable,
}


@dataclass(frozen=False)
class TrainingConfig:
    """
    wraper for config data
    from trainin_config.json
    """

    resources: Dict[Type[IBaseTunableModel], ModelResources]
    ensemble_size: int
    loss_to_weight: Callable[[float, List[float]], float]
    fast_dev_run: bool
    shuffle_data: bool


def is_lambda_parsable(parsed: str) -> bool:
    """
    checks whether given string represents
    can be parsed to function

    NOTE:
        call to 'eval' is not safe use this
        function with caution
    """

    code = None
    try:
        code = eval(parsed)
        if not callable(code):
            return False
        # random input check
        rez = code(1.1, [1.1, 2.3, 0, 0.3])
        if not isinstance(rez, float):
            return False
    except Exception:
        return False
    return True


# creates two custom types "avaible_model_type" and "loss_to_weight_parsable"
# avaible_model_type is string which matches one of type names from "avaible_model_types"
# loss_to_weight_parsable is string which can represents python code
type_checker = jsonschema.Draft3Validator.TYPE_CHECKER.redefine_many(
    {
        "avaible_model_type": lambda _, tname: tname in _MODEL_NAME_MAPPING,
        "loss_to_weight_parsable": lambda _, func_str: is_lambda_parsable(func_str),
    }
)

ext_validator = jsonschema.validators.extend(
    jsonschema.Draft3Validator, type_checker=type_checker
)


_SCHEMA = {
    "type": "object",
    "properties": {
        "models": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "model_type": {"type": "avaible_model_type"},
                    "timeout": {"type": "number"},
                    "n_trials": {"type": "number"},
                },
            },
        },
        "ensemble_size": {"type": "number"},
        "fast_dev_run": {"enum": [True, False]},
        "shuffle_data": {"enum": [True, False]},
        "loss_to_weight": {"type": "loss_to_weight_parsable"},
    },
}


def get_training_config(config_file: Path) -> TrainingConfig:
    """
    fiven path to training config file path
    returns TrainingConfig instance
    """

    j_conf = None
    with config_file.open("r") as f:
        try:
            j_conf = json.load(f)
        except json.decoder.JSONDecodeError as e:
            logging.exception(f"error decoding '{config_file.name}': ", e)
            sys.exit(1)

    validator = ext_validator(schema=_SCHEMA)
    validator.validate(j_conf)
    resources: Dict[Type[IBaseTunableModel], ModelResources] = {}
    for res in j_conf["models"]:
        model_t = _MODEL_NAME_MAPPING[res["model_type"]]
        model_res = ModelResources(timeout=res["timeout"], n_trials=res["n_trials"])
        resources[model_t] = model_res

    return TrainingConfig(
        resources=resources,
        ensemble_size=j_conf["ensemble_size"],
        fast_dev_run=j_conf["fast_dev_run"],
        shuffle_data=j_conf["shuffle_data"],
        loss_to_weight=eval(j_conf["loss_to_weight"]),
    )
