from typing import Optional
from fastapi import APIRouter
import fastapi
from fastapi.responses import JSONResponse
import logging
from modeldb import ModelDBClient
from typing import Dict

from pydantic.errors import JsonError

from SupervisedGym import AssembledModel
from .models import ScheduleTrainingInfo, PredictConfigurationInfo
from .worker import tasks

logger = logging.getLogger("gym_server")
router = APIRouter()


@router.post("/scheduleTraining")
def scheduleTraining(schedule_training_info: ScheduleTrainingInfo):
    """Orders the supervised gym to find the correct model and start its training.

    After the training the gym will send notification to the url specified in the request's arguments.

    Args:
        schedule_training_info: information about training - application id and url which listens for a notification
        background_tasks: automatically created background task object, which supports async operations

    Returns:
        response for the POST request describing whether the training has successfully begun

    Raises:
        HTTPException with code and message describing the problem
    """
    logger.info(
        f"Received schedule train request for application id: {schedule_training_info.application_id}"
    )
    tasks.train.delay(schedule_training_info)
    return "OK"


@router.post("/predictConfiguration")
def predictConfiguration(predict_configuration_info: PredictConfigurationInfo):
    """Orders the supervised gym to predict configuration.

    Args:
        predict_configuration_info: information about the prediction - application id and network input

    Returns:
        response for the POST request describing the predicted configuration

    Raises:
        HTTPException with code and message describing the problem
    """
    logger.info(
        f"Received predict configuration request for application id: {predict_configuration_info.application_id}"
    )
    prediction: Optional[Dict[float, int]]
    try:
        dbc = ModelDBClient()
        logging.info("Successfuly connected to modelDB")
        parsed_model = dbc.loadTrainedModel(predict_configuration_info.application_id)
        logging.info("Successfuly fetched parsed model from modelDB")
        model = AssembledModel.load_model(parsed_model)
        logging.info("Successfuly loaded parsed model")
        network_input = predict_configuration_info.network_input
        prediction = model.predict(network_input)
        logging.info("Successfuly generated model prediction")
    except Exception as e:
        logging.exception(f"error while generating prediction: {e}")
        return JSONResponse(
            {}, status_code=fastapi.status.HTTP_500_INTERNAL_SERVER_ERROR
        )

    logging.debug(
        f"Successfully predicted a configuration for application id: {predict_configuration_info.application_id}"
    )
    return prediction
