import logging
import xml.etree.ElementTree as ET
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import Optional

import pandas as pd
import requests
import torch.nn as nn
from SupervisedGym import ModelAssembler, AssembledModel, DataConf
from modeldb import ModelDBClient

from .celery_app import celery_app
from ..config import config
from ..models import ScheduleTrainingInfo

logger = logging.getLogger("gym_server")
data_conf = DataConf(
    group_id=config.group_id,
    pred_horizon=config.prediction_horizon,
    data_split=config.data_split,
    shuffle=config.data_split,
)


def send_fail_req(conf: ScheduleTrainingInfo) -> None:
    """
    sends request to ScheduleTrainingInfo.proc_finished_uri
    with 'failed' status
    """

    try:
        requests.post(
            conf.proc_finished_uri,
            json={"status": "failed", "application_id": conf.application_id},
        )
    except Exception as e:
        logging.exception(
            "Error while sending failed status request "
            f"to {conf.proc_finished_uri}: {e}"
        )


@celery_app.task
def train(training_conf: ScheduleTrainingInfo) -> None:
    """Chooses and trains the network model, then saves is to the Model Database.

    Args:
        training_conf (ScheduleTrainingInfo):
            information about training - application id and url which listens for a notification
    NOTE:
        this taks will only work if env variable PYTHONOPTIMIZE is set to 1
        (So deamon processes could spawn children)
    """

    logging.info("executing train task, starting to fetch data from modelDB")
    # create modeldb client its important to make create it
    # in worker task becouse mongoclient must be opened after fork
    db_cli = ModelDBClient()
    data: Optional[pd.DataFrame] = None
    cp_problem: Optional[ET.ElementTree] = None
    err: Optional[Exception] = None
    with NamedTemporaryFile("r") as f:
        try:
            # put data to tmp file as csv and read it from there
            db_cli.exportTrainingDataToFile(training_conf.application_id, Path(f.name))
            data = pd.read_csv(f.name)
        except Exception as e:
            logging.exception(f"error while fetching training data from modelDB: {e}")
            err = e
        else:
            logging.info("succesfully fetched training data from modelDB")

        try:
            # put cp_problem tp tmp file and parse it from there
            db_cli.exportCP(training_conf.application_id, Path(f.name))
            cp_problem = ET.parse(f.name)
        except Exception as e:
            logging.exception(f"error while fetching cp-problem from modelDB: {e}")
            err = e
        else:
            logging.info("succesfully fetched cp-problem from modelDB")

        if err is not None:
            logging.info("quiting training task")
            send_fail_req(training_conf)
            return

    logging.info("all data fetched succesfully from modelDB")

    # create model assembler
    assembler = ModelAssembler(
        raw_data=data, cp_problem=cp_problem, val_loss=nn.L1Loss(), data_conf=data_conf
    )

    logging.info("starting to train model")

    # train network
    model: Optional[AssembledModel] = None
    try:
        tconf = config.training_config
        model = assembler.assemble_model(
            models=tconf.resources,
            ensemble_size=tconf.ensemble_size,
            loss_to_weight=tconf.loss_to_weight,
            fast_dev_run=tconf.fast_dev_run,
        )
    except Exception as e:
        logger.exception(f"Error while training model: {e}")
        logging.info("quiting training task")
        send_fail_req(training_conf)
        return

    logger.info(f"Successfully trained network")

    with NamedTemporaryFile("wb") as f:
        try:
            f.write(model.parse_model())
            db_cli.importTrainedModelFromFile(
                training_conf.application_id, Path(f.name)
            )

            db_cli.updateNetworkInfo(
                application_id=training_conf.application_id,
                input_columns=model.input_columns,
                input_sequence_len=model.input_sequence_len,
                target_columns=model.target_columns,
            )

        except Exception as e:
            logger.exception(f"error found while saving model to modelDB: {e}")
            logger.info("quiting training task")
            send_fail_req(training_conf)
            return

    logger.info(
        f"Successfully saved a network for application id to modelDB: {training_conf.application_id}"
    )

    try:
        requests.post(
            training_conf.proc_finished_uri,
            json={
                "status": "successful",
                "application_id": training_conf.application_id,
            },
        )
    except Exception as e:
        logging.exception(
            "Error while sending success status request "
            f"to {training_conf.proc_finished_uri}: {e}"
        )
        logger.info("quiting training task")
        return
    logger.info("Successfully finished training task")
