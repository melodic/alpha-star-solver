from celery import Celery

from ..config import config

celery_app = Celery(
    "tasks",
    broker=(
        f"amqp://{config.rabbitmq_username}:{config.rabbitmq_password}@"
        f"{config.rabbitmq_host}:{config.rabbitmq_port}//"
    ),
    include=["gym_server.worker.tasks"],
)

# we pass tasks to celery worker as pydantic model
# which are not serializable by json but are by pickle
# so we have to add "pickle" as task serializer
class CeleryConfig:
    task_serializer = "pickle"
    result_serializer = "pickle"
    event_serializer = "json"
    accept_content = ["application/json", "application/x-python-serialize"]
    result_accept_content = ["application/json", "application/x-python-serialize"]


celery_app.config_from_object(CeleryConfig)
