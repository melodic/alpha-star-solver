from fastapi import FastAPI
import logging

from .config import config


logging.basicConfig(level=config.log_level)
app = FastAPI()


@app.on_event("startup")
def init_app() -> None:
    """ starts routing """

    from .routes import router

    app.include_router(router)
