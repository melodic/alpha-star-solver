from pydantic import BaseModel
from typing import Dict, List, Union


class PredictConfigurationInfo(BaseModel):
    application_id: str
    network_input: Dict[str, List[Union[float, int]]]
