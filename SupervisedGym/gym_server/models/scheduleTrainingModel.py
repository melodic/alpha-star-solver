from pydantic import BaseModel


class ScheduleTrainingInfo(BaseModel):
    application_id: str
    proc_finished_uri: str
