from __future__ import annotations
from typing import Dict, List
import xml.etree.ElementTree as ET
import pandas as pd
from functools import lru_cache
from .domain_rounder import DomainRounder
from .data import TimeSeriesDataStaticConfig
from .data_wrappers import DataConf
import re


class ProblemAnalyzer:
    """
    class that validates combination of
    cp-problem file and padnas dataframe with
    data from datagenerator.
    Additionally based on that generates
    cp-variables domain rounder and
    static config of TimeSeriesDataset
    """

    def __init__(self, data_conf: DataConf) -> ProblemAnalyzer:
        """creates new instance of ProblemAnalyzer

        Args:
            data_conf (DataConf):
                configutation of problem and data

        Returns:
            ProblemAnalyzer:
                new instance of ProblemAnalyzer
        """
        self.group_id = data_conf["group_id"]
        self.pred_horizon = data_conf["pred_horizon"]
        self.data_split = data_conf["data_split"]
        self.shuffle = data_conf["shuffle"]

    def generate_domain_rounder(self, cp_problem: ET.ElementTree) -> DomainRounder:
        return DomainRounder(self._get_domains_from_constraint_problem(cp_problem))

    def generate_tsdata_static_conf(
        self, raw_data: pd.DataFrame, cp_problem: ET.ElementTree
    ) -> TimeSeriesDataStaticConfig:
        target_vals = self._get_domains_from_constraint_problem(cp_problem).keys()
        for var in target_vals:
            if var not in raw_data.columns:
                raise Exception(
                    f"cp-problem variable '{var}' can't be found in given data columns"
                )
        if self.group_id not in raw_data.columns:
            raise Exception(f"given data does not contain '{self.group_id}' column")

        # guess predicted values by 'pred' postfix
        predict_vals = list(
            filter(lambda x: "PREDICTION" in x.upper(), raw_data.columns)
        )
        input_cols = list(
            filter(
                lambda x: x not in target_vals and x != self.group_id, raw_data.columns
            )
        )

        return TimeSeriesDataStaticConfig(
            data=raw_data,
            group_id=self.group_id,
            input_continuous=input_cols,
            input_categorical=[],
            target_continuous=list(target_vals),
            target_categorical=[],
            prediction_horizon=self.pred_horizon,
            input_predictions=predict_vals,
            train_val_test_percentage=self.data_split,
            shuffle_groups=self.shuffle,
        )

    @lru_cache(maxsize=1)
    def _get_domains_from_constraint_problem(
        self,
        cp_problem: ET.ElementTree,
    ) -> Dict[str, List[int]]:
        """
        Returns a dictionary containing domains of variables.
        """

        result = {}
        type_re = re.compile(r"^({.*})?type$")

        for variable_node in cp_problem.findall("cpVariables"):

            variable_name = variable_node.attrib.get("id")

            domain_node = variable_node.find("domain")
            variable_domain = []

            if variable_name is None:
                raise Exception("No 'id' attribute in 'cpVariables' tag.")
            if domain_node is None:
                raise Exception(
                    f"Variable {variable_name} has no 'domain' child in CP."
                )

            domain_type = "No type attribute."

            for key, value in domain_node.attrib.items():
                if not type_re.match(key) is None:
                    domain_type = value
                    break

            if domain_type == "cp:RangeDomain":

                from_node = domain_node.find("from")
                to_node = domain_node.find("to")
                if from_node is None or to_node is None:
                    raise Exception(
                        f"Wrong domain fromat of {variable_name} variable."
                        "Expected 'from' and 'to' nodes"
                    )

                start_value = int(from_node.attrib.get("value", "0"))
                end_value = int(to_node.attrib.get("value", "0")) + 1
                variable_domain = range(start_value, end_value)

            elif domain_type == "cp:NumericListDomain":

                for value in domain_node.findall("values"):
                    variable_domain.append(int(value.attrib.get("value", "0")))

            else:
                raise Exception(
                    f"Unknown type of domain of {variable_name} variable: {domain_type}."
                )

            result[variable_name] = variable_domain

        return result
