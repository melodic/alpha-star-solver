from typing import TypedDict
from .data import Train_val_test_perc


class DataConf(TypedDict):
    """Wrapper for some general data configurations

    Args:
        group_id (str):
            name of column that specifies "group_id", which
            identifies independent segments of data
            in some csv.
        pred_horizon (int):
            the ammount of steps ahead that we are generating
            predictions for. For example, if distance between
            each timestamp is 60 second, that with
            "pred_horizon" = 5, we are generating predictions
            5 minutes ahed of now.
        data_split (Train_val_test_perc):
            How are we splitting data for training, validation, testing
            in accuracy to one percent.
            NOTE: currently test set is not used in whole pipeline
        shuffle (bool):
            specifies if pipeline should shuffle data_groups.
            Should be set to True, if not testing on same
            data outside of this pipeline
    """

    group_id: str
    pred_horizon: int
    data_split: Train_val_test_perc
    shuffle: bool


class ModelResources(TypedDict):
    """Wrapper for model training resources

    Args:
        timeout (int):
            maximal time of training in seconds.
        n_trials (int):
            maximal number of model training
            trials performed in pipeline
    """

    timeout: int
    n_trials: int
