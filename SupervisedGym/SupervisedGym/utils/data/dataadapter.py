from __future__ import annotations
import torch
from typing import List
from sklearn.base import TransformerMixin

from .TimeSeriesDataSet import (
    ColumnTransformer,
    TimeSeriesData,
    BatchedTransformerMinix,
)


class DataAdapter:
    """
    wrapper that contains data transformers for network,
    remembers order in which data is given and returned
    from network. Helps with dict to dict network prediction
    """

    def __init__(
        self,
        input_sequence_len: int,
        input_transformer: TransformerMixin,
        target_transformer: TransformerMixin,
        input_columns: List[str],
        target_columns: List[str],
    ) -> DataAdapter:
        self.__input_sequence_len = input_sequence_len
        self.__input_transformer = BatchedTransformerMinix(input_transformer)
        self.__target_transformer = BatchedTransformerMinix(target_transformer)
        self.__input_columns = input_columns
        self.__target_columns = target_columns

    @staticmethod
    def create_from_timeseries_data(time_series_data: TimeSeriesData) -> DataAdapter:
        return DataAdapter(
            input_sequence_len=time_series_data.input_sequence_len,
            input_transformer=time_series_data.input_transformer,
            target_transformer=time_series_data.target_transformer,
            input_columns=time_series_data.input_columns,
            target_columns=time_series_data.target_columns,
        )

    @property
    def input_sequence_len(self) -> int:
        return self.__input_sequence_len

    @property
    def input_transformer(self) -> ColumnTransformer:
        return self.__input_transformer

    @property
    def target_transformer(self) -> ColumnTransformer:
        return self.__target_transformer

    @property
    def input_columns(self) -> List[str]:
        return self.__input_columns

    @property
    def target_columns(self) -> List[str]:
        return self.__target_columns

    def transform_input(self, x: torch.Tensor, inverse: bool = False) -> torch.Tensor:
        """
        preprocess given input data
        """

        # input tensor requires grad so detach
        with torch.no_grad():
            if inverse:
                return torch.from_numpy(
                    self.__input_transformer.inverse_transform(x.cpu().numpy())
                )
            else:
                return torch.from_numpy(
                    self.__input_transformer.transform(x.cpu().numpy())
                )

    def transform_target(self, y: torch.Tensor, inverse: bool = False) -> torch.Tensor:
        """
        preprocess given target data
        """

        # input tensor requires grad so detach
        with torch.no_grad():
            if inverse:
                return torch.from_numpy(
                    self.__target_transformer.inverse_transform(y.cpu().numpy())
                )
            else:
                return torch.from_numpy(
                    self.__target_transformer.transform(y.cpu().numpy())
                )
