import numpy as np
from sklearn.base import TransformerMixin


class BatchedTransformerMinix(TransformerMixin):
    """
    wrapper for transformer minix that allows
    for operations on data with shape
    (batch_size, n_samples, n_features)
    """

    def __init__(self, transformer: TransformerMixin) -> None:
        self._transformer = transformer

    def fit(self, X: np.ndarray):
        """
        fits data of shape (batch_size, n_samples, n_features)
        if X has 3 dimension or (n_samples, n_features) if
        X has 2 dimension
        """
        if len(X.shape) == 2:
            return self._transformer.fit(X)
        return self._transformer.fit(X.reshape(-1, X.shape[2]))

    def transform(self, X: np.ndarray) -> np.ndarray:
        """
        returns transformed data of shape (batch_size, n_samples, n_features)
        if X has 3 dimension or (n_samples, n_features) if X has 2 dimension
        """
        if len(X.shape) == 2:
            return self._transformer.transform(X)
        org_shape = X.shape
        reshaped = X.reshape(-1, org_shape[2])
        transformed = self._transformer.transform(reshaped)
        return transformed.reshape(*org_shape)

    def inverse_transform(self, X: np.ndarray) -> np.ndarray:
        """
        returns inverse transformed data of shape (batch_size, n_samples, n_features)
        if X has 3 dimension or (n_samples, n_features) if X has 2 dimension
        """
        if len(X.shape) == 2:
            return self._transformer.inverse_transform(X)
        org_shape = X.shape
        transformed = self._transformer.inverse_transform(X.reshape(-1, org_shape[2]))
        return transformed.reshape(*org_shape)
