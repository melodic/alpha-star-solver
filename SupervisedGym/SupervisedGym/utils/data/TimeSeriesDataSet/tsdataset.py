import torch
from torch.utils.data import Dataset
from typing import Callable, Tuple


class TsDataset(Dataset):
    """
    simple implementation of pytorch Dataset,
    for time series predictions
    """

    def __init__(
        self, getitem: Callable[[int], Tuple[torch.Tensor, torch.Tensor]], length: int
    ):
        self.getitem = getitem
        self.length = length

    def __len__(self) -> int:
        return self.length

    def __getitem__(self, idx: int) -> Tuple[torch.Tensor, torch.Tensor]:
        return self.getitem(idx)
