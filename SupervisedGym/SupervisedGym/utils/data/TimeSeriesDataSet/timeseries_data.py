from __future__ import annotations
import pandas as pd
import random
import numpy as np
import torch
from torch.utils.data import WeightedRandomSampler
from typing import (
    List,
    Type,
    Optional,
    Tuple,
    Any,
)
from sklearn.base import TransformerMixin
from collections import namedtuple
from functools import lru_cache, cached_property

from .tsdataset import TsDataset
from .columntransformer import ColumnTransformer


# if set to None, CACHE_SIZE unlimited
CACHE_SIZE = None
Train_val_test_perc = namedtuple("Train_val_test_perc", ["train", "validate", "test"])


class TimeSeriesData:
    """
    Container for TimeSeriesData performes data reshaping,
    preprocessing, data sampling, generate pytorch
    dataset for network training
    """

    data_len: int
    idx_gr_map: np.array
    idx_gr_idx_map: np.array
    data_groups: List[Tuple[pd.DataFrame, pd.DataFrame]]
    __input_transformer: ColumnTransformer
    __target_transformer: ColumnTransformer

    def __init__(
        self,
        data: pd.DataFrame,
        group_id: str,
        input_continuous: List[str],
        input_categorical: List[str],
        target_continuous: List[str],
        target_categorical: List[str],
        prediction_horizon: int,
        input_sequence_len: int,
        input_predictions: List[str] = [],
        input_scaler_type: Optional[Type[TransformerMixin]] = None,
        target_scaler_type: Optional[Type[TransformerMixin]] = None,
        train_val_test_percentage: Train_val_test_perc = Train_val_test_perc(
            70, 15, 15
        ),
        shuffle_groups: bool = True,
    ) -> TimeSeriesData:
        """
        Creates TimeSeriesData instance, which performes
            1. data preprocessing
            2. shuffling
            3. generating samplers

        NOTE:
            1. variable name == name of column in padnas DataFrame
            2. data group is continous set of csv rows with same value of group_id

        Args:
            data (pd.DataFrame):
                csv of training data parsed into pandas DataFrame
            group_id (str):
                name of column in training data that identifies from which experiment data comes from.
            input_continuous (List[str]):
                list of variables names which are inputs for prediction and are continous values. (eg. avg number of users per minute)
            input_categorical (List[str]):
               list of variables names which are inputs for prediction and are categorical values. (eg. current type of application deployment)
            target_continuous (List[str]):
                list of variables names which are target of prediction and are continous values. (eg. number of cores in deployment configuration)
            target_categorical (List[str]):
                list of variables names which are target of prediction and are categorical values. (eg. type of provider for app hosting)
            prediction_horizon (int):
                how much lag we want to apply in prediction.
                (eg. if prediction_horizon=0 generated datasets will return target values in same csv row as last input data csv row
                    if prediction_horizon=10 generated datasets will return target values in 10 csv rows further then last input data csv row)
            input_sequence_len (int):
                length of data series that will be provided by generated datasets to input.
            input_predictions (List[str]):
                list of variables names which values are predictions of some value.
                Defaults to: [].
            input_scaler_type (Optional[Type[TransformerMixin]]):
                Type of scaler that will be used to transform input data. If set to None, no transformation will be applied.
                Defaults to: None.
            target_scaler_type (Optional[Type[TransformerMixin]]):
                Type of scaler that will be used to transform target data. If set to None, no transformation will be applied.
                Defaults to: None.
            train_val_test_percentage (Train_val_test_perc[int, int, int]):
                Train_val_test_perc object encoding train, validate, test data split.
                Defaults to:  Train_val_test_perc(70, 15, 15).
            shuffle_groups (bool):
                if set to True, all data groups will be shuffled (not contents of data group), before data splitting.
                Defaults to: True.

        Returns:
            TimeSeriesData instance

        NOTE:
            1. returned values are cached, if encountered
                memory issues try lowering CACHE_SIZE.

            2. continuous and categorical values are treated the same
               way, but they influence order of retuned values.
               You can implement other optimizations for diffrent
               types of values, ex. encoding of categorical ones.
        """

        self.data = data
        self.group_id = group_id
        self.input_continuous = input_continuous
        self.input_categorical = input_categorical
        self.target_continuous = target_continuous
        self.target_categorical = target_categorical
        self.__prediction_horizon = prediction_horizon
        self.input_predictions = input_predictions
        self.__input_sequence_len = input_sequence_len
        self.input_scaler_type = input_scaler_type
        self.target_scaler_type = target_scaler_type
        self.train_val_test_percentage = train_val_test_percentage
        self.shuffle_groups = shuffle_groups

        # validates dataa
        self.__validate_input()

        # reshape data
        self.__reshape_data()

        # create data transformer
        self.__input_transformer = self.__generate_input_transformator()
        self.__target_transformer = self.__generate_target_transformator()

        for i in range(len(self.data_groups)):
            x_df, y_df = self.data_groups[i]
            x_df[self.input_columns] = self.__input_transformer.transform(
                x_df.to_numpy()
            )
            y_df[self.target_columns] = self.__target_transformer.transform(
                y_df.to_numpy()
            )

    @lru_cache(maxsize=CACHE_SIZE)
    def __getitem__(self, idx: int) -> Tuple[torch.Tensor, torch.Tensor]:
        group = self.idx_gr_map[idx]
        group_idx = self.idx_gr_idx_map[idx]
        x, y = self.data_groups[group]

        # NOTE pretransforming data to torch.Tensors
        # and returning it that way and removeing
        # lru_cache might help with memory usage reduction
        return (
            torch.from_numpy(
                x.iloc[
                    group_idx - self.__input_sequence_len + 1 : group_idx + 1
                ].to_numpy()
            ).float(),
            torch.from_numpy(
                y.iloc[group_idx + self.prediction_horizon].to_numpy()
            ).float(),
        )

    def __generate_input_transformator(self):
        """returns ColumnTransformer already fitted to input data"""

        # if no skaler provided return id skaler
        if self.input_scaler_type is None:
            return ColumnTransformer(None)

        transformers = [self.input_scaler_type() for _ in self.input_columns]

        col_transformer = ColumnTransformer(transformers)
        train_cutoff_size = int(len(self.data) * self.train_val_test_percentage.train)
        col_transformer.fit(
            self.data.loc[:train_cutoff_size, self.input_columns].to_numpy()
        )

        return col_transformer

    def __generate_target_transformator(self):
        """returns ColumnTransformer already fitted to target data"""

        # if no skaler provided return id skaler
        if self.target_scaler_type is None:
            return ColumnTransformer(None)

        transformers = [self.target_scaler_type() for _ in self.target_columns]

        col_transformer = ColumnTransformer(transformers)
        col_transformer.fit(
            self.data.loc[: self.data_len, self.target_columns].to_numpy()
        )

        return col_transformer

    def __reshape_data(self) -> None:
        """
        reshapes data

        Raises:
            ValueError if any of data groups is too small for other params
        """

        # remove unused columns
        unused_cols = list(
            set(self.data.columns).difference(
                set().union(
                    [self.group_id],
                    self.input_continuous,
                    self.input_categorical,
                    self.target_continuous,
                    self.target_categorical,
                )
            )
        )
        self.data.drop(labels=unused_cols, axis=1, inplace=True)

        # split data into groups and shuffle if specified
        data_groups = self.__preprocess_data_groups(
            [g for _, g in self.data.groupby(self.group_id)]
        )

        # for each group we will remove input_sequence_len rows from start and prediction_horizon rows from end
        data_len = len(self.data) - len(data_groups) * (
            self.__input_sequence_len + self.__prediction_horizon - 1
        )

        # create mappings:
        #   idx_gr_map: [0, data_len) -> group of data
        #   idx_gr_idx_map: [0, data_len) -> index in data group
        idx_gr_map = np.zeros(data_len).astype(np.int32)
        idx_gr_idx_map = np.zeros(data_len).astype(np.int32)
        gr, gr_idx, idx = 0, 0, 0
        while idx < data_len:
            # if iterated over each elem in group, start iterating in next group
            if gr_idx == len(data_groups[gr][1]):
                gr_idx = 0
                gr += 1

            # if index inside group is too close to each end, we can't use that idx
            if (
                gr_idx < self.__input_sequence_len - 1
                or gr_idx + self.prediction_horizon >= len(data_groups[gr][1])
            ):
                gr_idx += 1
                continue

            idx_gr_map[idx] = gr
            idx_gr_idx_map[idx] = gr_idx
            idx += 1
            gr_idx += 1

        # assign class variables
        self.data_groups = data_groups
        self.idx_gr_map = idx_gr_map
        self.idx_gr_idx_map = idx_gr_idx_map
        self.data_len = data_len

    def __preprocess_data_groups(
        self, groups: List[pd.DataFrame]
    ) -> List[Tuple[pd.DataFrame, pd.DataFrame]]:
        """
        1. shuffles groups
        2. checks if all groups are big enough
        3. splits data groups to list of tuples (x, y)
            where each data segment looks like:
                input = x[idx - input_sequence_len: input_sequence_len]
                target = y[idx + __prediction_horizon]

        Arguments:
            groups (List[pd.DataFrame]):
                list of raw data groups
        Returns:
            List[Tuple[pd.DataFrame, pd.DataFrame]]

        Raises:
            ValueError - if any of the groups is too small
        """

        if self.shuffle_groups:
            random.shuffle(groups)

        # check if all data groups are big enough
        min_group_size = self.__prediction_horizon + self.__input_sequence_len
        for gr in groups:
            if len(gr) < min_group_size:
                raise ValueError(
                    "at least one data group is too small, "
                    f"each must have at least {min_group_size} rows"
                )

        # in each group create tuple with input data, and target data
        # shift predicted input values UP in each group by self.input_predictions
        new_groups: List[Tuple[pd.DataFrame, pd.DataFrame]] = []
        for gr in groups:
            x = gr.loc[:, self.input_columns]
            y = gr.loc[:, self.target_columns]

            # shift UP predictions in input and drop nans
            x.loc[:, self.input_predictions] = (
                x[self.input_predictions].shift(-self.__prediction_horizon).dropna()
            )
            new_groups.append((x, y))

        return new_groups

    def __len__(self) -> int:
        """returns length of all data combined"""
        return self.data_len

    @staticmethod
    def __is_contained_in(l1: List[Any], l2: List[Any]) -> bool:
        """checks if li is contained in l2"""
        return set(l1).issubset(set(l2))

    def __validate_input(self):
        """
        validates input

        Throws:
            ValueError if some input arg is invalid

        Returns:
            None
        """

        # validate subcolumns
        cols = self.data.columns
        contained_in_columns_argname: List[str] = [
            "input_continuous",
            "input_categorical",
            "target_continuous",
            "target_categorical",
            "input_predictions",
        ]
        for sub_col_names in contained_in_columns_argname:
            sub_cols = getattr(self, sub_col_names)
            if not self.__is_contained_in(sub_cols, cols):
                raise ValueError(
                    f"{sub_cols} headers are not contained in data headers"
                )

        # validate that continuous data and categorical data
        # is consistent (only marked as continuous or categorical)
        cols_name_with_empty_intersection: List[Tuple[str, str]] = [
            ("input_continuous", "input_categorical"),
            ("input_continuous", "target_categorical"),
            ("input_categorical", "target_continuous"),
        ]

        for col_names1, col_names2 in cols_name_with_empty_intersection:
            cols1 = set(getattr(self, col_names1))
            cols2 = set(getattr(self, col_names2))
            intersection = cols1.intersection(cols2)
            if len(intersection) != 0:
                raise ValueError(
                    f"intersection of arguments '{col_names1}' and '{col_names2}' "
                    f"must be empty, args '{intersection}' are not consistent"
                )

        if self.group_id not in cols:
            raise ValueError(f"{self.group_id} must be one of data headers")

        # validate input and target not empty
        if len(self.input_columns) == 0:
            raise ValueError("input columns not specified")

        if len(self.target_columns) == 0:
            raise ValueError("target columns not specified")

        # validate integer args
        if self.prediction_horizon < 0:
            raise ValueError("prediction_horizon cant be negative")

        if self.__input_sequence_len <= 0:
            raise ValueError("input_sequence_len must be positive integer")

        # validate train_val_test_percentage
        if sum(self.train_val_test_percentage) != 100:
            raise ValueError(
                "all 3 elements of train_val_test_percentage must sum to 100"
            )

    def __train_cutoff(self) -> int:
        """
        returns last row index + 1 in train data section
        assuming that we split data into segments like
        [train, validate, test]
        """
        return (self.data_len * self.train_val_test_percentage.train) // 100

    def __val_cutoff(self) -> int:
        """
        returns last row index + 1 in validation data section
        assuming that we split data into segments like
        [train, validate, test]
        """
        val_len = (self.data_len * self.train_val_test_percentage.validate) // 100
        return self.__train_cutoff() + val_len

    def __test_cutoff(self) -> int:
        """
        returns last row index + 1 in test data section
        assuming that we split data into segments like
        [train, validate, test]
        """
        return self.data_len

    @cached_property
    def columns(self) -> List[str]:
        """returns list of all column names without group_id column"""
        return self.input_columns + self.target_columns

    @cached_property
    def input_columns(self) -> List[str]:
        """
        returns list of all network input column names in order the generated
        datasets will provide them in tensor
        """
        return self.input_continuous + self.input_categorical

    @property
    def input_continuous_columns(self) -> List[str]:
        """returns list of all network input column names that contain continuous values"""
        return self.input_continuous

    @property
    def input_categorical_columns(self) -> List[str]:
        """returns list of all network input column names that contain categorical values"""
        return self.input_categorical

    @cached_property
    def target_columns(self) -> List[str]:
        """
        returns list of all network target column names in order the generated
        datasets will provide them in tensor
        """
        return self.target_continuous + self.target_categorical

    @property
    def target_continuous_columns(self) -> List[str]:
        """returns list of all network target column names that contain continuous values"""
        return self.target_continuous

    @property
    def target_categorical_columns(self) -> List[str]:
        """returns list of all network target column names that contain categorical values"""
        return self.target_categorical

    @property
    def prediction_horizon(self) -> int:
        """returns prediction horizont"""
        return self.__prediction_horizon

    @property
    def input_sequence_len(self) -> int:
        """returns input_sequence_len"""
        return self.__input_sequence_len

    def generate_train_dataset(self) -> TsDataset:
        """
        returns instance of TsDataset that
        returns train data
        """

        return TsDataset(self.__getitem__, self.__train_cutoff())

    def generate_validation_dataset(self) -> TsDataset:
        """
        returns instance of TsDataset that
        returns validation data
        """

        offset = self.__train_cutoff()
        return TsDataset(
            lambda idx: self.__getitem__(offset + idx), self.__val_cutoff() - offset
        )

    def generate_test_dataset(self) -> TsDataset:
        """
        returns instance of TsDataset that
        returns test data
        """

        offset = self.__val_cutoff()
        return TsDataset(
            lambda idx: self.__getitem__(offset + idx), self.__test_cutoff() - offset
        )

    def generate_uniform_target_sampler(self) -> WeightedRandomSampler:
        """
        returns pytorch WeightedRandomSampler that samples
        so that returned targets have uniform distribution.
        Sampler is build based on training data

        NOTE:
            sampler should be only passed to train dataloader
        """

        train_targets = self.data.loc[
            : self.__train_cutoff(), self.target_columns
        ].to_numpy()
        unique, indexes, occurs = np.unique(
            train_targets, return_counts=True, return_inverse=True, axis=0
        )

        weights = 1 / (occurs[indexes] * unique.shape[0])
        return WeightedRandomSampler(weights, train_targets.shape[0], True)

    @property
    def input_transformer(self) -> ColumnTransformer:
        return self.__input_transformer

    @property
    def target_transformer(self) -> ColumnTransformer:
        return self.__target_transformer
