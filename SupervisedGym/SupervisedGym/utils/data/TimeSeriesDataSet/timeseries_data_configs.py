import pandas as pd
from typing import List, Optional, TypedDict
from sklearn.base import TransformerMixin

from . import Train_val_test_perc, TimeSeriesData


class TimeSeriesDataStaticConfig(TypedDict):
    """
    time series init aruments that are not
    subject to change in model fitting
    process
    """

    data: pd.DataFrame
    group_id: str
    input_continuous: List[str]
    input_categorical: List[str]
    target_continuous: List[str]
    target_categorical: List[str]
    prediction_horizon: int
    input_predictions: List[str]
    train_val_test_percentage: Train_val_test_perc = Train_val_test_perc(70, 15, 15)
    shuffle_groups: bool


class TimeSeriesDataFitableConfig(TypedDict):
    """
    time series init aruments that are
    subject to change in model fitting
    process
    """

    input_sequence_len: int
    input_scaler_type: Optional[type(TransformerMixin)]
    target_scaler_type: Optional[type(TransformerMixin)]


def make_time_series_data(
    static_conf: TimeSeriesDataStaticConfig, fitable_conf: TimeSeriesDataFitableConfig
) -> TimeSeriesData:
    """
    creates TimeSeriesData instance based
    on TimeSeriesDataStaticConfig and
    TimeSeriesDataFitableConfig
    """
    return TimeSeriesData(**{**static_conf, **fitable_conf})
