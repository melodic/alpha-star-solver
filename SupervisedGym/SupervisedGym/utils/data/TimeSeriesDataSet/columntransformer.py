import numpy as np
from sklearn.base import TransformerMixin
from typing import List, Optional


class ColumnTransformer(TransformerMixin):
    """
    implements sklearn ColumnTransformer with inverse
    transformation using sklearn preprocessing scalers
    """

    def __init__(self, scalers: Optional[List[TransformerMixin]] = None):
        """
        creates instance of ColumnTransformer

        Arguments:
            skalers (Optional[List[TransformerMixin]]):
                list of sklearn transformers which will be applied to data.
                Each transformer will be applied to column corresponding
                to position of transformer in skalers list.
                If None ColumnTransformer will not perform transformation
                Defaults to None
        """
        self.scalers = scalers

    def fit(self, X: np.ndarray):
        """
        fit all transforers to data in their columns

        Arguments:
            X (nd.ndarray):
                2-D np.ndarray of shape (n_samples, n_features)
        """

        if self.scalers is None:
            return

        columns = X.T
        for col, transformer in zip(columns, self.scalers):
            transformer.fit(col.reshape(-1, 1))

    def transform(self, X: np.ndarray) -> np.ndarray:
        """
        transform given data

        Arguments:
            X (nd.ndarray):
                2-D np.ndarray of shape (n_samples, n_features)

        Returns:
            2-D np.ndarray of input shape but with applied transformation
        """

        if self.scalers is None:
            return X

        return np.column_stack(
            list(
                map(
                    lambda column, transformer: transformer.transform(
                        column.reshape(column.shape[0], 1)
                    ),
                    X.T,
                    self.scalers,
                )
            )
        )

    def inverse_transform(self, X: np.ndarray) -> np.ndarray:
        """
        inverse transform given data

        Arguments:
            X (nd.ndarray):
                2-D np.ndarray of shape (n_samples, n_features)

        Returns:
            2-D np.ndarray of input shape but with applied inverse transformation
        """

        if self.scalers is None:
            return X

        return np.column_stack(
            list(
                map(
                    lambda column, transformer: transformer.inverse_transform(
                        column.reshape(column.shape[0], 1)
                    ),
                    X.T,
                    self.scalers,
                )
            )
        )
