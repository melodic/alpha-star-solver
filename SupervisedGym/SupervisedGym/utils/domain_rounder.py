import numpy as np
from typing import Dict, List


class DomainRounder:
    """
    Rounds output to nearest value in domain.
    Domain format: list of domains of variables.
    Domain of each variable: nonempty sorted list of possible values.
    Order of variable domains in domain must match order of variables in model's output.

    Example:
    >>> r = DomainRounder({'v1': [0, 1, 2], 'v2': [0, 1, 2]})
    >>> x = {'v1': 1.111, 'v2': 0.2137}
    >>> r.round(x)
    >>> {'v1': 1, 'v2': 0}
    """

    domain: Dict[str, np.array]

    def __init__(self, domain: Dict[str, List[int]]):
        """creates new instance of DomainRounder

        Args:
            domain (Dict[str, List[int]]):
                mapping from name of the variable
                (pandas column and cp-problem vairable)
                to List of ints which is correct domain
                of this varible in production enviroment
        """
        self.domain = {
            col: np.sort(np.array(var_dom)) for col, var_dom in domain.items()
        }

    def round(self, x: Dict[str, float]) -> Dict[str, int]:
        """returns variables mapped to their correct domain

        Args:
            x (Dict[str, float]):
                mapping from name of variable
                (pandas column and cp-problem vairable)
                to its raw value returned by network
                or other model.

        Returns:
            Dict[str, int]:
                returns same mapping but from
                name of variables to value of
                variables rounded to their
                correct business domain
        """
        rez = {}
        for col, y in x.items():
            closest_idx = 0
            dom = self.domain[col]
            idx = np.searchsorted(dom, [y])
            if idx == 0:
                closest_idx = idx
            elif idx == len(dom):
                closest_idx = idx - 1
            elif abs(dom[idx - 1] - y) < abs(dom[idx] - y):
                closest_idx = idx - 1
            else:
                closest_idx = idx
            rez[col] = dom[closest_idx].item()

        return rez
