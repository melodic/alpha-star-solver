import optuna
from typing import Callable


class NewBestTrialCallbackTrigger:
    """
    optuna callback that triggers
    given callback when new trial
    just become new best trial
    """

    def __init__(self, callback: Callable[[], None]):
        self.callback = callback

    def __call__(self, study: optuna.Study, trial: optuna.trial.FrozenTrial) -> None:
        """
        Executes after each trial
        """
        try:
            # if there are no trials then Value error is thrown
            best_trial = study.best_trial
            if best_trial != trial:
                return
        except ValueError:
            pass

        self.callback()
