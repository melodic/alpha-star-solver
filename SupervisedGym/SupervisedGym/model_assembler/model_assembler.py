from __future__ import annotations
import logging
import pandas as pd
import torch
import xml.etree.ElementTree as ET
from typing import Dict, List, Tuple, Type, Callable

from ..utils import ModelResources, DataConf
from ..models.base import IBaseTunableModel
from ..models.assembled_model import AssembledModel
from ..utils.problem_analyzer import ProblemAnalyzer
from ..modeltuner import ModelTuner, TrainedModelStats


logger = logging.getLogger("ModelAssebler")


class ModelAssembler:
    """Top class in SupervisedGym module
    responsible understanding cp-problem for training models,
    training models and building ensembly model used as solver
    """

    def __init__(
        self,
        raw_data: pd.DataFrame,
        cp_problem: ET.ElementTree,
        val_loss: torch.nn.Module,
        data_conf: DataConf,
    ) -> ModelAssembler:
        """Creates new Instance of ModelAssembler

        Args:
            raw_data (pd.DataFrame):
                Data preprocessed by datagenerator.
                    1. Timestamps should be equally spaced.
                    2. Dataframe should contain group_id column
                    3. Dataframe should contain all columns that
                        are in constrain problem as cp-variables
                    4. Prediction columns in dataframe should
                        contain "prediction" phraze
                    5. All inputs in dataframe should be numeric.
            cp_problem (ET.ElementTree):
               Parsed Constrain problem file, should contain
               cp-variables, with their domains
            val_loss (torch.nn.Module):
                validation function for calculating validation loss
                during training. Generaly the better the prediction of the target
                the closer to 0 value of validation loss should be.
                NOTE: In fact it doesnt have to be of type torch.nn.Module
                    but it should have the same interface
            data_conf (DataConf):
                Some additional informations about the data

        Returns:
            ModelAssebler: [description]
        """
        self.val_loss = val_loss
        self.raw_data = raw_data
        analyzer = ProblemAnalyzer(data_conf)
        self.dom_rounder = analyzer.generate_domain_rounder(cp_problem)
        self.data_static_conf = analyzer.generate_tsdata_static_conf(
            raw_data, cp_problem
        )

    def assemble_model(
        self,
        models: Dict[Type[IBaseTunableModel], ModelResources],
        ensemble_size: int = 5,
        loss_to_weight: Callable[[float, List[float]], float] = lambda v, _: 1
        / (v + 1),
        fast_dev_run: bool = False,
    ) -> AssembledModel:
        """Runs whole pipeline training process in the end returning
        trained AssembledModel instance that can be used as solver

        Args:
            models (Dict[Type[IBaseTunableModel], ModelResources]):
                Specifies resources in training for each model type.
                Created AssembledModel with ensemble_size big enough
                can contain each of trained model given in this argument.
            ensemble_size (int, optional):
                Size of enseble that will be created.
                Defaults to 5.
            loss_to_weight (Callable[[float, List[float]], float], optional):
                Function that given, validation_loss of model, and List of
                size not greater then ensemble_size containing the smallest
                validation losses. Returns weight of this model in ensemble.
                Generated AssembledModel predictions are weighted average
                of all models predicitons with their weights.
                Defaults to f(loss, ensemble_losses) = 1 / (loss + 1)
                NOTE:
                    models with negative or equal to 0 weight are not included in ensemble.
            fast_dev_run (bool, optional):
                If true pipeline executes all of code (very shortly) to check for eny erros.
                Defaults to False.

        Returns:
            AssembledModel: Instance of trained AssembledModel
        """

        logger.info(
            f"Started model assembing, with config: {dict(models=models, ensemble_size=ensemble_size)}"
        )

        model_stats: List[TrainedModelStats] = []

        def __receive_stat(new_stat: TrainedModelStats):
            """updates model stats with new model if has val_loss
            small enough"""
            # nonlocal to include model_stats variable
            # from outside of inner function scope
            nonlocal model_stats

            if len(model_stats) < ensemble_size:
                model_stats.append(new_stat)
                logger.info(
                    f"new model with val_loss: {new_stat.val_loss} added to current ensemble set"
                )

            max_idx = 0
            max_loss = float("-inf")
            for i, stat in enumerate(model_stats):
                if stat.val_loss > max_loss:
                    max_loss = stat.val_loss
                    max_idx = i

            if new_stat.val_loss < max_loss:
                model_stats[max_idx] = new_stat
                logger.info(
                    "new current best model added to ensemble set val_loss: {new_stat.val_loss}"
                )

        tuner = ModelTuner(
            val_loss=self.val_loss,
            tsdata_static_config=self.data_static_conf,
            fast_dev_run=fast_dev_run,
        )

        for modelT, resources in models.items():
            logging.info(
                f"starting to train model of type: {modelT}, "
                f"with: timeout={resources['timeout']}, "
                f"n_trials={resources['n_trials']}"
            )
            tuner.tune(
                timeout=resources["timeout"],
                tunable_model_type=modelT,
                n_trials=resources["n_trials"],
                model_callback=__receive_stat,
            )
            logger.info(f"finished training model of type: {modelT}")

        logger.info("finished training all models, building ensemble...")

        # calculate model weights in ensemble
        losses = [stat.val_loss for stat in model_stats]
        logger.info(
            "building ensemble with models haveing validation losses: "
            f"{losses}, which corresponds to weights: "
            f"{[loss_to_weight(l, losses) for l in losses]}"
        )
        models: List[Tuple[IBaseTunableModel, float]] = []

        max_w = max([loss_to_weight(x, losses) for x in losses])
        min_w = 0

        if max_w == min_w:
            logger.warn(
                "assembly maximum weight equals "
                f"assembly minimum weight ({max_w}). "
                "Swiching to same weights average assembly strategy"
            )

        for stat in model_stats:
            weigth = loss_to_weight(stat.val_loss, losses)
            # normalize weight to [0, 1]
            weigth = (weigth - min_w) / (max_w - min_w) if max_w != min_w else 1

            if weigth > 0:
                models.append((stat.model, weigth))

        return AssembledModel(
            models=models,
            domain_rounder=self.dom_rounder,
        )
