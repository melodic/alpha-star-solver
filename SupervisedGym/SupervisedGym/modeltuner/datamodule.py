import torch
from pytorch_lightning import LightningDataModule
from torch.utils.data import DataLoader

from SupervisedGym.utils.data import TimeSeriesData


class DataModule(LightningDataModule):
    """
    Implementation of https://pytorch-lightning.readthedocs.io/en/stable/extensions/datamodules.html?highlight=LightningDataModule#lightningdatamodule-api
    """

    def __init__(self, time_series_data: TimeSeriesData, batch_size: int):
        """Creates new DataModule instance

        Args:
            time_series_data (TimeSeriesData): instance of TimeSeriesData from which datasets will be generated
            batch_size (int): batch size for dataloaders
        """
        super().__init__()
        self.tsdata = time_series_data
        self.batch_size = batch_size

    def train_dataloader(self) -> DataLoader:
        """Generates train dataloader

        Returns:
            DataLoader: instance of new train DataLoader

        NOTE:
            smarter way of chosing num_workers might speed up training. ex. based on batch_size
        """
        return DataLoader(
            dataset=self.tsdata.generate_train_dataset(),
            batch_size=self.batch_size,
            sampler=self.tsdata.generate_uniform_target_sampler(),
            num_workers=4,
            pin_memory=torch.cuda.is_available(),
        )

    def val_dataloader(self) -> DataLoader:
        """Generates validation dataloader

        Returns:
            DataLoader: instance of new validation DataLoader

        NOTE:
            smarter way of chosing num_workers might speed up training. ex. based on batch_size
        """
        return DataLoader(
            dataset=self.tsdata.generate_validation_dataset(),
            batch_size=self.batch_size,
            num_workers=4,
            pin_memory=torch.cuda.is_available(),
        )

    def test_dataloader(self) -> DataLoader:
        """Generates test dataloader

        Returns:
            DataLoader: instance of new test DataLoader
        """
        return DataLoader(
            dataset=self.tsdata.generate_test_dataset(),
            batch_size=self.batch_size,
            num_workers=0,
            pin_memory=False,
        )
