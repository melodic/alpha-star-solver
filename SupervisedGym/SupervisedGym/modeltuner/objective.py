import pytorch_lightning as pl
import torch
import optuna
import logging
from typing import Dict, Any, Optional, Type, TypedDict
from sklearn.base import TransformerMixin
from sklearn import preprocessing
from optuna.integration import PyTorchLightningPruningCallback
from pytorch_lightning.callbacks import EarlyStopping

from ..utils.data import (
    TimeSeriesDataStaticConfig,
    TimeSeriesDataFitableConfig,
    DataAdapter,
    make_time_series_data,
)
from ..models.base import IBaseTunableModel
from .datamodule import DataModule


logger = logging.getLogger("Objective")


class ObjectiveHParams(TypedDict):
    """
    dict containing Objective hyper
    parameters which are hyperparameters
    but such that concerns piepline
    as whole and not single model
    """

    max_epochs: int
    min_epochs: int
    es_patience: int
    es_divergence_threshold: float
    val_check_interval: float
    max_batch_size_pow: int
    min_batch_size_pow: int
    inc_batch_every_n_oks: int


class Objective:
    """callable class which is objective
    for modeltuner to minimize
    """

    def __init__(
        self,
        tsdata_static_config: TimeSeriesDataStaticConfig,
        tunable_model_type: Type[IBaseTunableModel],
        val_func: torch.nn.Module,
        objective_hyper_params: ObjectiveHParams,
        fast_dev_run: bool = False,
    ):
        """
        constructor of new Objective instance

        Args:
            tsdata_static_config (TimeSeriesDataStaticConfig):
                static config of TimeSeriesDataset used
                when creating TimeSeriesDataset for model training.
            tunable_model_type (Type[IBaseTunableModel]):
                subtype of BaseTunableModel which instance will be trained
            val_func (torch.nn.Module):
                loss function used to evaluate validation error, the better model should have
                lower validation loss value. This might not be the same as loss function used
                in training. But val_loss is used in model pruning.
                (Can be of type Callable[[output_size], float])
            objective_hyper_params (ObjectiveHParams):
                objective hyperparams for pipeline to use.
            fast_dev_run (bool):
                if true the objective training will perform short diagnostic run.
                Defaults to True
            max_batch_size_pow (int):
                max batch size in training
            min_batch_size_pow (int):
                min batch size in training

        Returns:
            new Objective instance
        """

        self.tsdata_static_config = tsdata_static_config
        self.tunable_model_type = tunable_model_type
        self.val_func = val_func
        self.model: Optional[IBaseTunableModel] = None
        self.fast_dev_run = fast_dev_run
        self.ohparams = objective_hyper_params
        self.max_batch_size_pow = objective_hyper_params["max_batch_size_pow"]
        self.min_batch_size_pow = objective_hyper_params["min_batch_size_pow"]
        self.inc_batch_every_n_oks = objective_hyper_params["inc_batch_every_n_oks"]

    def current_model(self) -> IBaseTunableModel:
        """
        returns currently trained model with its validation loss
        """
        if self.model is None:
            raise Exception("There is no trained model in objective")
        return self.model

    def suggested_params(self) -> Dict[str, Any]:
        """returns all default params for trial based on model suggested params"""

        return {
            "batch_size_pow": 6,
            "x_scaler_name": "StandardScaler",
            "y_scaler_name": "MaxAbsScaler",
            "gradient_clip_val": 1,
            **self.tunable_model_type.suggested_params(),
        }

    def __call__(self, trial: optuna.trial.Trial) -> float:
        # generate batch size pow
        batch_size_pow = trial.suggest_int(
            "batch_size_pow", self.min_batch_size_pow, self.max_batch_size_pow
        )
        batch_size = 2 ** batch_size_pow

        input_scaler_name = trial.suggest_categorical(
            "x_scaler_name",
            ["MinMaxScaler", "MaxAbsScaler", "StandardScaler", "RobustScaler", "None"],
        )
        input_scaler_type: Optional[Type[TransformerMixin]] = (
            getattr(preprocessing, input_scaler_name)
            if input_scaler_name != "None"
            else None
        )

        target_scaler_name = trial.suggest_categorical(
            "y_scaler_name", ["MinMaxScaler", "MaxAbsScaler", "RobustScaler", "None"]
        )
        target_scaler_type: Optional[Type[TransformerMixin]] = (
            getattr(preprocessing, target_scaler_name)
            if target_scaler_name != "None"
            else None
        )

        # generate gradient_clip_val
        gradient_clip_val = trial.suggest_float("gradient_clip_val", 1e-1, 10)

        seq_len = trial.suggest_int("seq_len", 5, 60, step=5)

        fitable_config = TimeSeriesDataFitableConfig(
            input_sequence_len=seq_len,
            input_scaler_type=input_scaler_type,
            target_scaler_type=target_scaler_type,
        )

        tsdata = make_time_series_data(self.tsdata_static_config, fitable_config)
        datamodule = DataModule(tsdata, batch_size)

        adapter = DataAdapter.create_from_timeseries_data(tsdata)

        # generate tunable model
        self.model = self.tunable_model_type.get_trialed_model(
            trial=trial, data_adapter=adapter, val_loss=self.val_func
        )

        callbacks = [
            PyTorchLightningPruningCallback(trial, monitor="val_loss"),
            EarlyStopping(
                monitor="val_loss",
                mode="min",
                patience=self.ohparams["es_patience"],
                divergence_threshold=self.ohparams["es_divergence_threshold"],
            ),
        ]

        trainer = pl.Trainer(
            logger=False,
            gpus=-1 if torch.cuda.is_available() else None,
            val_check_interval=self.ohparams["val_check_interval"],
            fast_dev_run=self.fast_dev_run,
            checkpoint_callback=False,
            max_epochs=self.ohparams["max_epochs"],
            min_epochs=self.ohparams["min_epochs"],
            gradient_clip_val=gradient_clip_val,
            callbacks=callbacks,
            progress_bar_refresh_rate=0,
        )

        logger.info(
            f"starting trial: {trial._trial_id} with hparams "
            f"val_check_interval={self.ohparams['val_check_interval']} "
            f"max_epochs={self.ohparams['max_epochs']} "
            f"min_epochs={self.ohparams['min_epochs']}"
        )

        ok_fits = 0
        try:
            trainer.fit(model=self.model, datamodule=datamodule)
            ok_fits += 1
        except RuntimeError as e:
            ok_fits = 0
            if str(e).startswith("CUDA out of memory."):
                logger.error(
                    "'CUDA out of memory' execption happened "
                    f"setting max_batch_size to {2 ** (batch_size_pow - 1)}"
                )
                self.max_batch_size_pow = batch_size_pow - 1
                if self.max_batch_size_pow < self.min_batch_size_pow:
                    logger.error("Can't decrease batch size more")
                    raise e
            else:
                logger.error(f"ANOTHER EXCEPTION HAPPENDED {e}")
                raise e
        else:
            # if last self.inc_batch_every_n_oks went without CUDA error
            # increase batch size by one up to orginal max
            if (
                ok_fits % self.inc_batch_every_n_oks == 0
                and self.max_batch_size_pow < self.ohparams["max_batch_size_pow"]
            ):
                self.max_batch_size_pow += 1

        val_loss = trainer.callback_metrics["val_loss"].item()

        logger.info(f"finished trial: {trial._trial_id} " f"with val_loss: {val_loss}")
        return val_loss
