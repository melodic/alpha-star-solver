import optuna
import torch
import gc
import logging
from typing import Callable, Optional, Type, NamedTuple
from ..models.base import IBaseTunableModel
from ..utils.data import TimeSeriesDataStaticConfig
from .objective import Objective
from .utils import generate_objective_hparams

logger = logging.getLogger("ModelTuner")


class TrainedModelStats(NamedTuple):
    """model and its training results

    Args:
        model (IBaseTunableModel):
            instance of trained model
        val_loss (float):
            validation loss of trained model
    """

    model: IBaseTunableModel
    val_loss: float


def collect_garbage() -> None:
    """
    runs python garbage collection and
    clears cuda cache
    """
    gc.collect()
    torch.cuda.empty_cache()


class ModelTuner:
    """class responsible for training single
    network
    """

    def __init__(
        self,
        val_loss: torch.nn.Module,
        tsdata_static_config: TimeSeriesDataStaticConfig,
        fast_dev_run: bool = False,
    ):
        self.tsdata_static_config = tsdata_static_config
        self.val_loss = val_loss
        self._best_model: Optional[IBaseTunableModel] = None
        self.fast_dev_run = fast_dev_run

    def tune(
        self,
        timeout: int,
        tunable_model_type: Type[IBaseTunableModel],
        model_callback: Callable[[TrainedModelStats], None],
        n_trials: Optional[int] = None,
    ) -> None:
        """creates new instance of ModelTuner

        Args:
            timeout (int):
                maximal time of training in seconds
            tunable_model_type (Type[IBaseTunableModel]):
                type of network to be trained
            model_callback (Callable[[TrainedModelStats], None]):
                callback with each trained model and its training results.
                called after each trial
            n_trials (Optional[int], optional): [description].
                maximal number of training trials in training session.
                If None no such limit is set. Defaults to None.
        """

        pruner = optuna.pruners.HyperbandPruner()
        study = optuna.create_study(direction="minimize", pruner=pruner)
        objective = Objective(
            tsdata_static_config=self.tsdata_static_config,
            tunable_model_type=tunable_model_type,
            val_func=self.val_loss,
            fast_dev_run=self.fast_dev_run,
            objective_hyper_params=generate_objective_hparams(
                conf=self.tsdata_static_config, val_loss=self.val_loss
            ),
        )

        # enqueue trial with suggested params
        study.enqueue_trial(objective.suggested_params())

        logger.info(
            "starting training session"
            f"used pruner: {type(pruner).__name__}"
            f"first trial config: {objective.suggested_params()}"
        )

        study.optimize(
            objective,
            n_trials=n_trials,
            timeout=timeout,
            callbacks=[
                lambda _, ft: model_callback(
                    TrainedModelStats(
                        model=objective.current_model(),
                        val_loss=ft.value if ft.value else float("inf"),
                    )
                ),
                lambda study, trial: collect_garbage(),
            ],
        )

        logger.info("training session ended")
