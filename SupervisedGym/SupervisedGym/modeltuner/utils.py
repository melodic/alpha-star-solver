import torch
from math import log
from torch.utils.data import Dataset, DataLoader, Sampler
from typing import Literal, Optional
from SupervisedGym.utils.data import TimeSeriesDataStaticConfig
from .objective import ObjectiveHParams


def naive_model_loss(
    val_func: torch.nn.Module,
    dataset: Dataset,
    mode_type: Literal["mean"] = "mean",
    sampler: Optional[Sampler] = None,
) -> float:
    """
    calculates naive prediction loss on train dataset
    """
    loader = DataLoader(dataset=dataset, sampler=sampler, batch_size=1)
    pred: torch.Tensor
    if mode_type == "mean":
        # calculate average values of targets
        # using moving average
        cma = None
        for i, (_, y) in enumerate(loader, start=1):
            if i == 1:
                cma = torch.Tensor(y)
                continue
            cma = (y + i * cma) / (i + 1)
        pred = cma

        # calculate avg loss of average prediction
        # using moving average
        loss = 0
        for i, (_, y) in enumerate(loader, start=1):
            loss = (val_func(pred, y) + i * loss) / (i + 1)
        return loss

    else:
        raise Exception(f"type {mode_type} is not handled")


def generate_objective_hparams(
    conf: TimeSeriesDataStaticConfig, val_loss: torch.nn.Module
) -> ObjectiveHParams:
    """
    based on TimeSeriesDataStaticConfig and problem validation
    function generates ObjectiveHParams
    """
    train_items = int(
        (conf["data"].size / 100) * conf["train_val_test_percentage"].train
    )
    min_epochs = max(1, int(log(train_items, 100)))
    return ObjectiveHParams(
        val_check_interval=1.0,
        max_epochs=min_epochs * 50,
        min_epochs=min_epochs,
        es_divergence_threshold=None,
        es_patience=min_epochs * 3,
        max_batch_size_pow=12,
        min_batch_size_pow=3,
        inc_batch_every_n_oks=10,
    )
