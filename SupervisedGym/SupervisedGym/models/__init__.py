from SupervisedGym.models.base.IBaseTunableModel import IBaseTunableModel
from typing import List, Type
from .FeedForward.FeedForwardTunable import FeedForwardTunable
from .StackedRNN.StackedRNNTunable import StackedRNNTunable
from .Transformer.TransformerTunable import TransformerTunable
from .assembled_model import AssembledModel


avaible_model_types: List[Type[IBaseTunableModel]] = [
    FeedForwardTunable,
    StackedRNNTunable,
    TransformerTunable,
]

__all__ = [
    FeedForwardTunable,
    StackedRNNTunable,
    TransformerTunable,
    AssembledModel,
    avaible_model_types,
]
