from __future__ import annotations
from torch.optim.lr_scheduler import ReduceLROnPlateau
from typing import Dict, Any, Tuple
import torch
import pickle
import optuna
import torch.nn as nn

from ..base import IBaseTunableModel
from .StackedRNN import StackedRNN
from ...utils.data import DataAdapter


class StackedRNNTunable(IBaseTunableModel):
    """
    LightningNet module
    """

    def __init__(
        self,
        data_adapter: DataAdapter,
        cell_type: str,
        num_recurrent_layers: int,
        hidden_size: int,
        bidirectional: bool,
        dropouts: Tuple[float, float],
        activation: nn.Module,
        layer_norm: bool,
        loss: nn.Module,
        optimizer_type: torch.optim.Optimizer,
        lr: float,
        val_loss: nn.Module,
    ):
        super().__init__()
        self.save_hyperparameters()

        # stackedRNN args
        self.cell_type = cell_type
        self.num_recurrent_layers = num_recurrent_layers
        self.hidden_size = hidden_size
        self.bidirectional = bidirectional
        self.dropouts = dropouts
        self.activation = activation
        self.layer_norm = layer_norm

        # other extra args
        self.dataadapter = data_adapter
        self.loss = loss
        self.optimizer_type = optimizer_type
        self.lr = lr
        self.val_loss = val_loss

        self.net = StackedRNN(
            input_size=len(data_adapter.input_columns),
            output_size=len(data_adapter.target_columns),
            seq_len=data_adapter.input_sequence_len,
            cell_type=cell_type,
            num_recurrent_layers=num_recurrent_layers,
            hidden_size=hidden_size,
            bidirectional=bidirectional,
            dropouts=dropouts,
            activation=activation,
            layer_norm=layer_norm,
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """
        performs forward step
        """
        return self.net.forward(x)

    def training_step(self, batch, batch_idx: int) -> float:
        """
        Performs training step on input and
        output data already processed by dataadpater
        """
        x, y = batch
        prediction = self(x)
        loss = self.loss(prediction, y)
        self.log("train_loss", loss)
        return loss

    def validation_step(self, batch, batch_idx: int) -> None:
        """
        Performs validation step
        """
        x, y = batch
        output = self(x)
        prediction = self.dataadapter.transform_target(output, inverse=True)
        y = self.dataadapter.transform_target(y, inverse=True)
        val_loss = self.val_loss(prediction, y)
        self.log("val_loss", val_loss, prog_bar=True)

    def configure_optimizers(self) -> Dict[str, Any]:
        """
        returns optimizer
        """
        optimizer = self.optimizer_type(self.parameters(), lr=self.lr)
        scheduler = ReduceLROnPlateau(optimizer, "min", patience=5)
        return {
            "optimizer": optimizer,
            "lr_scheduler": scheduler,
            "monitor": "train_loss",
        }

    @property
    def data_adapter(self) -> DataAdapter:
        return self.dataadapter

    def get_trialed_model(
        trial: optuna.Trial, data_adapter: DataAdapter, val_loss: torch.nn.Module
    ) -> StackedRNNTunable:

        # generate loss function
        loss_name = trial.suggest_categorical(
            "loss_name", ["L1Loss", "MSELoss", "SmoothL1Loss"]
        )
        loss = getattr(torch.nn, loss_name)()

        # generate activation unit
        activation_name = trial.suggest_categorical(
            "activation_name", ["ReLU", "LeakyReLU"]
        )
        activation = getattr(torch.nn, activation_name)()

        # generate the optimizer
        optimizer_name = trial.suggest_categorical(
            "optimizer_name", ["Adam", "RMSprop"]
        )
        optimizer_type = getattr(torch.optim, optimizer_name)

        return StackedRNNTunable(
            data_adapter=data_adapter,
            cell_type=trial.suggest_categorical("cell_type", ["LSTM", "GRU"]),
            num_recurrent_layers=trial.suggest_int("num_hidden_layers", 1, 5),
            hidden_size=trial.suggest_int("hidden_size", 4, 50),
            bidirectional=trial.suggest_categorical("bidirectional", [True, False]),
            dropouts=(
                trial.suggest_float("recurrent_layer_dropout", 0, 0.5),
                trial.suggest_float("linear_layer_dropout", 0, 0.5),
            ),
            activation=activation,
            layer_norm=trial.suggest_categorical("layer_norm", [True, False]),
            loss=loss,
            lr=trial.suggest_float("lr", 1e-5, 1, log=True),
            optimizer_type=optimizer_type,
            val_loss=val_loss,
        )

    @staticmethod
    def suggested_params() -> Dict[str, Any]:
        return {
            "loss_name": "SmoothL1Loss",
            "activation_name": "LeakyReLU",
            "optimizer_name": "Adam",
            "cell_type": "LSTM",
            "num_hidden_layers": 2,
            "hidden_size": 15,
            "bidirectional": False,
            "recurrent_layer_dropout": 0.2,
            "linear_layer_dropout": 0.2,
            "layer_norm": False,
        }

    @classmethod
    def load_model(cls, encoding: bytes) -> StackedRNNTunable:
        data = pickle.loads(encoding)
        model = StackedRNNTunable(**data["hparams"])
        model.net.load_state_dict(data["state"])
        return model

    def parse_model(self) -> bytes:
        data = {"state": self.net.state_dict(), "hparams": self.hparams}
        return pickle.dumps(data)
