from typing import List, Dict, Any, Callable, Optional, Tuple
from itertools import chain
import pytorch_lightning as pl
import torch.nn as nn
import torch


class StackedRNN(nn.Module):
    """
    Simple network consisting of multiple
    stacked LSTM or GRU layer with single
    linear layer at the end

    Args:
        input_size (int):
            number of features in input sequence
        output_size (int):
            size of 1d vector returned from network
        seq_len (int):
            length of data sequenct
        cell_type (str):
            recurrent cell type ["LSTM", "GRU"]
        num_recurrent_layers (int):
            number of recurrent layers,
            positive integer
        hidden_size (int): number of features in each
            lstm / gru hidden state,
            positive integer
        bidirectional (bool): if True becomes bidirectional
            rnn network.
        dropouts (Tuple[float, float]): 2 element tuple of
            dropout probabilities for each reccurent layer and
            last linear layer
        activation (nn.Module):
            pytorch activation function used before each
            hidden layer, defaults to Relu
        layer_norm (bool):
            if set to True, after last rnn layer
            layer normalization is applied

    Returns:
        New instance of StackedRNN class

    """

    def __init__(
        self,
        input_size: int,
        output_size: int,
        seq_len: int,
        cell_type: str,
        num_recurrent_layers: int,
        hidden_size: int,
        bidirectional: bool,
        dropouts: Tuple[float, float],
        activation: nn.Module,
        layer_norm: bool,
    ):
        super().__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.cell_type = cell_type
        self.num_recurrent_layers = num_recurrent_layers
        self.hidden_size = hidden_size
        self.bidirectional = bidirectional
        self.dropouts = dropouts
        self.activation = activation
        self.layer_norm = layer_norm

        if cell_type not in ["LSTM", "GRU"]:
            raise ValueError("cell_type must equal 'LSTM' or 'GRU'")

        rnn_type: type = None
        if cell_type == "LSTM":
            rnn_type = nn.LSTM
        elif cell_type == "GRU":
            rnn_type = nn.GRU

        # create stacked rnn segment
        self.rnn = rnn_type(
            input_size=input_size,
            hidden_size=hidden_size,
            num_layers=num_recurrent_layers,
            batch_first=True,
            dropout=dropouts[0],
            bidirectional=bidirectional,
        )

        # create layer norm is specified
        if self.layer_norm:
            self.norm = nn.LayerNorm(self.hidden_size * (2 if bidirectional else 1))
        else:
            # if not specified just use identity transformation
            self.norm = lambda x: x

        # create dropout before output layer
        self.fc_drop = nn.Dropout(dropouts[1])

        # create output layer
        self.fc = nn.Linear(self.hidden_size * (2 if bidirectional else 1), output_size)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """
        Performs pass throught network

        Args:
            x: vector of inputs to network of shape (batch_size, seq_len, input_size)

        Returns:
            vector of outputs of shape (batch_size, output_size)
        """
        out, _ = self.rnn(x)
        out = self.norm(out[:, -1, :])  # take just last hidden layer
        out = self.activation(out)
        out = self.fc_drop(out)
        out = self.fc(out)
        return out
