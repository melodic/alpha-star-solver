import torch.nn as nn
import torch
import math


class PositionalEncoding(nn.Module):
    """
    Inject some information about the relative or absolute position of the tokens
    in the sequence. The positional encodings have the same dimension as
    the embeddings, so that the two can be summed. Here, sine and cosine
    functions of different frequencies are used.

    Args:
        d_model (int):
            the embed dim, size of embedding dimension(required).
        max_len (int):
            the max. length of the incoming sequence (default=5000).
    Examples:
        >>> pos_encoder = PositionalEncoding(d_model)
    """

    def __init__(self, d_model: int, max_len: int = 5000):
        super(PositionalEncoding, self).__init__()

        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(
            torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model)
        )
        pe[:, 0::2] = torch.sin(position * div_term)
        if d_model % 2 != 0:
            pe[:, 1::2] = torch.cos(position * div_term)[:, 0:-1]
        else:
            pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer("pe", pe)

    def forward(self, x):
        """
        Inputs of forward function
        Args:
            x (torch.Tensor):
                the sequence fed to the positional encoder model (required).
        Shape:
            x: [sequence length, batch size, embed dim]
            output: [sequence length, batch size, embed dim]
        Examples:
            >>> output = pos_encoder(x)
        """

        x = x + self.pe[: x.size(0), :]
        return x


class Transformer(nn.Module):
    """
    TransformerEncoder based model.

    Args:
        input_size (int):
            number of features in input sequence
        output_size (int):
            size of 1d vector returned from network
        seq_len (int):
            length of data sequenct
        features (int):
            dimensions of vectors in TransformerEncoder
        heads_number (int):
            number of attention heads in TransformerEncoder
        layers_number (int):
            number of layers in TransformerEncoder
        feed_forward_size (int):
            size of linear layer inside each TransformerEncoder layer
        pre_output (int):
            size of embedding before deducing answer vector
        dropout (float):
            dropout of transformer and primary encoding
        activation (nn.Module):
            pytorch activation function used before
            last linear layer

    Returns:
        New instance of Transformer class
    """

    def __init__(
        self,
        input_size: int,
        output_size: int,
        seq_len: int,
        features: int,
        heads_number: int,
        layers_number: int,
        feed_forward_size: int,
        pre_output: int,
        dropout: float,
        activation: nn.Module,
    ):
        super().__init__()

        # features must be divisible by heads_number, so it is rounded up
        features = ((features + heads_number - 1) // heads_number) * heads_number

        self.input_size = input_size
        self.output_size = output_size
        self.seq_len = seq_len
        self.num_layers = layers_number
        self.input_features = input_size
        self.pre_output = pre_output
        self.dropout = dropout
        self.feed_forward_size = feed_forward_size
        self.activation = activation

        self.primary_embedding = nn.Conv1d(input_size, features, 1)
        self.positionalEncoder = PositionalEncoding(features)
        transformerEncoderLayers = nn.TransformerEncoderLayer(
            features, heads_number, dim_feedforward=feed_forward_size, dropout=dropout
        )
        self.transformerEncoder = nn.TransformerEncoder(
            transformerEncoderLayers, self.num_layers
        )

        self.fc1 = nn.Linear(features * seq_len, pre_output)
        self.fc2 = nn.Linear(pre_output, output_size)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """
        Performs pass throught network

        Args:
            x: vector of inputs to network of shape (batch_size, seq_len, input_size)

        Returns:
            vector of outputs of shape (batch_size, output_size)
        """
        out = x

        # [batch_size, sequence_length, input_size]
        out = torch.transpose(out, 1, 2)

        # [batch_size, input_size, sequence_length]
        out = self.primary_embedding(out)

        # [batch_size, features, sequence_length]
        out = torch.transpose(out, 1, 2)

        # [batch_size, sequence_length, features]
        out = torch.transpose(out, 0, 1)

        # [sequence_length, batch_size, features]
        out = self.positionalEncoder(out)
        out = self.transformerEncoder(out)
        out = torch.transpose(out, 0, 1)

        # [batch_size, sequence_length, features]
        out = torch.flatten(out, 1, 2)

        # [batch_size, sequence_length * features]
        out = self.fc1(out)
        out = self.activation(out)
        out = self.fc2(out)

        return out
