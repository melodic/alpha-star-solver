from __future__ import annotations
from torch.optim.lr_scheduler import ReduceLROnPlateau
from typing import Dict, Any
import torch
import pickle
import optuna
import torch.nn as nn

from ..base import IBaseTunableModel
from .Transformer import Transformer
from ...utils.data import DataAdapter


class TransformerTunable(IBaseTunableModel):
    """
    LightningNet module of Transformer network.
    Args:
        data_adapter (DataAdapter):
        features (int):
            dimensions of vectors in TransformerEncoder
        heads_number (int):
            number of attention heads in TransformerEncoder
        layers_number (int):
            number of layers in TransformerEncoder
        feed_forward_size (int):
            size of linear layer inside each TransformerEncoder layer
        pre_output (int):
            size of embedding before deducing answer vector
        dropout (float):
            dropout of transformer and primary encoding
        activation (nn.Module):
            pytorch activation function used before
            last linear layer
        loss (nn.Module):
            loss function
        optimizer_type (torch.optim.Optimizer):
            optimizer
        lr (float):
            initial learning rate
        val_loss (nn.Module):
            loss function for validation
        patience (int):
            patience for lr scheduler
    """

    def __init__(
        self,
        data_adapter: DataAdapter,
        features: int,
        heads_number: int,
        layers_number: int,
        feed_forward_size: int,
        pre_output: int,
        dropout: float,
        activation: nn.Module,
        loss: nn.Module,
        optimizer_type: torch.optim.Optimizer,
        lr: float,
        val_loss: nn.Module,
        patience: int,
    ):
        super().__init__()
        self.save_hyperparameters()

        # Transformer args
        self.features = features
        self.heads_number = heads_number
        self.layers_number = layers_number
        self.feed_forward_size = feed_forward_size
        self.pre_output = pre_output
        self.dropout = dropout
        self.activation = activation

        # other extra args
        self.dataadapter = data_adapter
        self.loss = loss
        self.optimizer_type = optimizer_type
        self.lr = lr
        self.val_loss = val_loss
        self.patience = patience

        self.net = Transformer(
            input_size=len(data_adapter.input_columns),
            output_size=len(data_adapter.target_columns),
            seq_len=data_adapter.input_sequence_len,
            features=features,
            heads_number=heads_number,
            layers_number=layers_number,
            feed_forward_size=feed_forward_size,
            pre_output=pre_output,
            dropout=dropout,
            activation=activation,
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """
        performs forward step
        """
        return self.net.forward(x)

    def training_step(self, batch: torch.Tensor, batch_idx: int) -> float:
        """
        Performs training step on input and
        output data already processed by dataadapater
        """
        x, y = batch
        prediction = self(x)
        loss = self.loss(prediction, y)
        self.log("train_loss", loss)
        return loss

    def validation_step(self, batch: torch.Tensor, batch_idx: int) -> None:
        """
        Performs validation step
        """
        x, y = batch
        output = self(x)
        prediction = self.dataadapter.transform_target(output, inverse=True)
        y = self.dataadapter.transform_target(y, inverse=True)
        val_loss = self.val_loss(prediction, y)
        self.log("val_loss", val_loss, prog_bar=True)

    def configure_optimizers(self) -> Dict[str, Any]:
        """
        returns optimizer
        """
        optimizer = self.optimizer_type(self.parameters(), lr=self.lr)
        scheduler = ReduceLROnPlateau(optimizer, "min", patience=self.patience)
        return {
            "optimizer": optimizer,
            "lr_scheduler": scheduler,
            "monitor": "train_loss",
        }

    @property
    def data_adapter(self) -> DataAdapter:
        return self.dataadapter

    def get_trialed_model(
        trial: optuna.Trial, data_adapter: DataAdapter, val_loss: torch.nn.Module
    ) -> TransformerTunable:

        # generate loss function
        loss_name = trial.suggest_categorical(
            "loss_name", ["L1Loss", "MSELoss", "SmoothL1Loss"]
        )
        loss = getattr(torch.nn, loss_name)()

        # generate activation unit
        activation_name = trial.suggest_categorical(
            "activation_name", ["ReLU", "LeakyReLU"]
        )
        activation = getattr(torch.nn, activation_name)()

        # generate the optimizer
        optimizer_name = trial.suggest_categorical(
            "optimizer_name", ["Adam", "RMSprop"]
        )
        optimizer_type = getattr(torch.optim, optimizer_name)

        # generate patience
        patience = trial.suggest_categorical("lr_patience", [5, 20, 1000])

        return TransformerTunable(
            data_adapter=data_adapter,
            features=trial.suggest_int("features", 10, 100),
            heads_number=trial.suggest_int("heads_number", 1, 10),
            layers_number=trial.suggest_int("layers_number", 1, 10),
            feed_forward_size=trial.suggest_int("feed_forward_size", 10, 500),
            pre_output=trial.suggest_int("pre_output", 50, 500),
            dropout=trial.suggest_float("dropout", 0, 0.5),
            activation=activation,
            loss=loss,
            optimizer_type=optimizer_type,
            lr=trial.suggest_float("lr", 1e-5, 5e-2, log=True),
            val_loss=val_loss,
            patience=patience,
        )

    @staticmethod
    def suggested_params() -> Dict[str, Any]:
        return {
            "loss_name": "MSELoss",
            "activation_name": "ReLU",
            "optimizer_name": "Adam",
            "lr_patience": 5,
            "features": 50,
            "heads_number": 2,
            "layers_number": 7,
            "feed_forward_size": 100,
            "pre_output": 100,
            "dropout": 0.2,
            "lr": 0.0005,
        }

    @classmethod
    def load_model(cls, encoding: bytes) -> TransformerTunable:
        data = pickle.loads(encoding)
        model = TransformerTunable(**data["hparams"])
        model.net.load_state_dict(data["state"])
        return model

    def parse_model(self) -> bytes:
        data = {"state": self.net.state_dict(), "hparams": self.hparams}
        return pickle.dumps(data)
