from __future__ import annotations
from typing import Dict, Any, Tuple
import pytorch_lightning as pl
import abc
import torch
import optuna

import abc
from ...utils.data import DataAdapter
from .parsable_model import IParsableModel


class IBaseTunableModel(pl.LightningModule, IParsableModel):
    """
    Abstract base tunable model that can be
    trained, and can on its own generate
    predictions from raw values
    """

    @abc.abstractmethod
    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """
        forward pass with data straight
        from dataloader
        """
        pass

    @abc.abstractmethod
    def training_step(self, batch: Tuple[torch.Tensor], batch_idx: int):
        """Implementation of https://pytorch-lightning.readthedocs.io/en/latest/common/lightning_module.html?highlight=training_step#training-step

        Args:
            batch (torch.Tensor): The output of your DataLoader.
            batch_idx (int): Integer displaying index of this batch
        """
        pass

    @abc.abstractmethod
    def validation_step(self, batch: torch.Tensor, batch_idx: int):
        """Implementation of https://pytorch-lightning.readthedocs.io/en/latest/common/lightning_module.html#validation-step

        Args:
            batch (torch.Tensor):  The output of your DataLoader.
            batch_idx (int): Integer displaying index of this batch
        """
        pass

    @abc.abstractmethod
    def configure_optimizers(self) -> Dict[str, Any]:
        """Implementation of https://pytorch-lightning.readthedocs.io/en/latest/common/lightning_module.html#configure-optimizers

        Returns:
            Dict[str, Any]:
            Any of these 6 options:
                1. Single optimizer.
                2. List or Tuple of optimizers.
                3. Two lists - The first list has multiple optimizers, and the second has multiple LR schedulers
                    (or multiple lr_dict).
                4. Dictionary, with an "optimizer" key, and (optionally) a "lr_scheduler"
                    key whose value is a single LR scheduler or lr_dict.
                5. Tuple of dictionaries as described above, with an optional "frequency" key.
                6. None - Fit will run without any optimizer.
        """
        pass

    @abc.abstractproperty
    def data_adapter(self) -> DataAdapter:
        """DataAdapter getter

        Returns:
            DataAdapter: instance of model data_adapter
        """
        pass

    @abc.abstractstaticmethod
    def get_trialed_model(
        trial: optuna.Trial, data_adapter: DataAdapter, val_loss: torch.nn.Module
    ) -> IBaseTunableModel:
        """Encodes hyperparameter search space in trial argument,
        retrieves single hyperparameter configuration and based on that
        creates and returns instance of class implementating IBaseTunableModel interface.

        Args:
            trial (optuna.Trial): optuna Trial object
            data_adapter (DataAdapter):
                data apapter which knows transforation of data for this model
            val_loss (torch.nn.Module): validation loss function for this model

        Returns:
            IBaseTunableModel: class implementating IBaseTunableModel interface
        """
        pass

    @abc.abstractstaticmethod
    def suggested_params() -> Dict[str, Any]:
        """Returns arbitrary set of hyperparameters values that are being choosen in
        self.get_trialed_model function. Those values are will be tried in first
        training trial so they should seem as "good" as possible.

        NOTE:
            it might be usefull to change interface of this function in such way
            that it will take DataAdapter instance as argument.
            It could improve heuristics in for eg.
                choosing number of hidden layers and their sizes based
                on input and output layer sizes.

        Returns:
            Dict[str, Any]:
                Dictionary where keys are names of hyperparameters encoded in optuna Trial
                object while executing self.get_trialed_model function. Values are
                chosen values of those hyperparameters in proper types.
        """
        pass
