from __future__ import annotations
import abc


class IParsableModel(abc.ABC):
    @abc.abstractclassmethod
    def load_model(cls, encoding: bytes) -> IParsableModel:
        raise NotImplementedError

    @abc.abstractmethod
    def parse_model(self) -> bytes:
        raise NotImplementedError
