from __future__ import annotations
from SupervisedGym.models.base.IBaseTunableModel import IBaseTunableModel
import torch
import pickle
from typing import Dict, Any, Union, List, Type, Tuple
from itertools import count

from ..utils import DomainRounder
from .base.parsable_model import IParsableModel


class AssembledModel(IParsableModel):

    input_idx: Dict[str, int]

    def __init__(
        self,
        models: List[Tuple[IBaseTunableModel, float]],
        domain_rounder: DomainRounder,
    ) -> AssembledModel:

        self.models = models
        self.domain_rounder = domain_rounder
        self._input_columns = models[0][0].data_adapter.input_columns
        self._target_columns = models[0][0].data_adapter.target_columns
        self._input_sequence_len = max(
            map(lambda s: s[0].data_adapter.input_sequence_len, models)
        )
        self.input_idx = dict(zip(self._input_columns, count(start=0, step=1)))
        # enter eval mode for all models, a bit sloppy :(
        for m in self.models:
            m[0].eval()

    def __validate_input(self, x: Dict[str, List[Union[float, str, int]]]):
        """Checks if input data for prediction is correct
            raises ValueError with error description if not

        Args:
            x (Dict[str, List[Union[float, str, int]]]): [description]

        Raises:
            ValueError: [description]
        """
        for var_name in self._input_columns:
            if var_name not in x:
                raise ValueError(
                    f"sequence of {var_name} is required to make"
                    " prediction but not provided"
                )

        for var_name, seq in x.items():
            if len(seq) != self._input_sequence_len:
                raise ValueError(
                    f"sequence of {var_name} data has length {len(seq)},"
                    f" expected {self._input_sequence_len}."
                )

    def __assembly_forward(self, x: torch.Tensor) -> torch.Tensor:
        """
        performes forward pass using all assemby models
        and then morging their preiction using weighted
        average based on their val_loss
        """
        # currently all weights are equal
        w_sum = 0
        out = torch.zeros(len(self._target_columns))
        for model, w in self.models:
            seq_len = model.data_adapter.input_sequence_len
            xx = x[:, -seq_len:, :]
            xx = model.data_adapter.transform_input(xx)
            raw_out = model.forward(xx)
            transformed_out = model.data_adapter.transform_target(
                raw_out, inverse=True
            ).squeeze()
            out += transformed_out * w
            w_sum += w
        return out / w_sum

    @property
    def input_columns(self) -> List[str]:
        return self._input_columns

    @property
    def target_columns(self) -> List[str]:
        return self._target_columns

    @property
    def input_sequence_len(self) -> int:
        return self._input_sequence_len

    def predict(self, x: Dict[str, List[Union[float, str, int]]]) -> Dict[float, int]:
        """
        generates model prediction based
        on input data

        Params:
            x (Dict[str, List[Union[float, str]]]):
                input data for network. It has to be
                dict mapping all input column names to
                List of input values from earliest to
                latest. Each value list must have lenght
                equal to input_sequence_length
        """
        self.__validate_input(x)
        # we need to pass 3d tensor with first dimention
        # equal to batch_size which we set to 1
        tensor_in = torch.cat(
            tuple(
                torch.Tensor(x[col]).view(self._input_sequence_len, 1)
                for col in self._input_columns
            ),
            dim=1,
        ).unsqueeze(dim=0)

        prediction = self.__assembly_forward(tensor_in)
        pred_dict = {col: v.item() for col, v in zip(self._target_columns, prediction)}
        return self.domain_rounder.round(pred_dict)

    @classmethod
    def load_model(cls, encoding: bytes) -> AssembledModel:
        data = pickle.loads(encoding)
        models: List[Tuple[IBaseTunableModel, float]] = []
        for state_dict in data["model_data"]:
            model_type: Type[IBaseTunableModel] = state_dict["type"]
            models.append(
                (model_type.load_model(state_dict["encoding"]), state_dict["weight"])
            )
        return AssembledModel(models=models, domain_rounder=data["domain_rounder"])

    def parse_model(self) -> bytes:
        model_data: List[Dict[str, Any]] = []
        for model, weight in self.models:
            model_data.append(
                {"encoding": model.parse_model(), "weight": weight, "type": type(model)}
            )

        data = {"domain_rounder": self.domain_rounder, "model_data": model_data}

        return pickle.dumps(data)
