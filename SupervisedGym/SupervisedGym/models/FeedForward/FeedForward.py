from typing import List, Optional
import torch.nn as nn
import torch


class FeedForward(nn.Module):
    """
    Simple feed forward network implementation.
    Consists of multiple linear layers with
    activation, dropout and layer normalization

    Args:
        input_size (int):
            size of 1d vector given to network input
        output_size (int):
            size of 1d vector returned from network
        hidden_layer_sizes (List[int]): input sizes of hidden layers,
            length of this list defines number of hidden layers
        dropouts (List[float]): list of dropout probabilities for
            input layer (head of list) and after each hidden layer.
            NOTE:
                len(dropouts) - 1 must equal len(hidden_layer_sizes)
        activation (nn.Module):
            pytorch activation function used before each
            hidden layer, defaults to Relu
        layer_norm (bool):
            if set to True, after every layer except
            last layer normalization is applied,
            defaults to True
        output_activation (Optional[nn.Module]):
            if not None, given activation is applied
            after output layer, defaults to None

    Returns:
        New instance of FeedForward class

    """

    def __init__(
        self,
        input_size: int,
        output_size: int,
        hidden_layer_sizes: List[int],
        dropouts: List[float],
        activation=nn.ReLU,
        layer_norm: bool = True,
        output_activation: Optional[nn.Module] = None,
    ):
        super().__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.hidden_layer_sizes = hidden_layer_sizes
        self.dropouts = dropouts
        self.activation = activation
        self.layer_norm = layer_norm
        self.output_activation = output_activation

        # input data validation
        if len(dropouts) != len(hidden_layer_sizes):
            raise ValueError("len(dropouts) is not equal to len(hidden_layer_sizes)")

        if any(map(lambda x: x < 0 or x > 1, dropouts)):
            raise ValueError("all value of droputs must be in range [0, 1]")

        layer_sizes = [self.input_size] + self.hidden_layer_sizes
        network_layers = []

        for i in range(len(layer_sizes) - 1):
            network_layers.append(nn.Linear(layer_sizes[i], layer_sizes[i + 1]))
            if layer_norm == True:
                network_layers.append(nn.LayerNorm(layer_sizes[i + 1]))

            network_layers.append(activation)
            if dropouts[i] > 0:
                network_layers.append(nn.Dropout(dropouts[i]))

        # add output layer
        network_layers.append(nn.Linear(layer_sizes[-1], self.output_size))
        if output_activation is not None:
            network_layers.append(output_activation)

        # sequence model
        self.sequence = nn.Sequential(*network_layers)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """
        Performs pass throught network

        Args:
            x: vector of inputs to network of shape (batch_size, input_size, 1)

        Returns:
            vector of outputs of shape (batch_size, output_size, 1)
        """
        return self.sequence.forward(x)
