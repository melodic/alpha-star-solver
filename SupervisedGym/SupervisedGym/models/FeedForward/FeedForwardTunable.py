from __future__ import annotations
from torch.optim.lr_scheduler import ReduceLROnPlateau
from typing import Optional, Dict, Any, List
import pickle
import torch
import optuna
import torch.nn as nn

from ..base import IBaseTunableModel
from ...utils.data import DataAdapter
from .FeedForward import FeedForward


class FeedForwardTunable(IBaseTunableModel):
    """
    LightningNet module
    """

    def __init__(
        self,
        data_adapter: DataAdapter,
        hidden_layer_sizes: List[int],
        dropouts: List[float],
        activation: nn.Module,
        layer_norm: bool,
        output_activation: Optional[nn.Module],
        loss: nn.Module,
        optimizer_type: torch.optim.Optimizer,
        lr: float,
        val_loss: nn.Module,
    ):
        super().__init__()
        self.save_hyperparameters()

        self.__data_adapter = data_adapter
        self.new_input_size = (
            len(data_adapter.input_columns) * data_adapter.input_sequence_len
        )
        self.optimizer_type = optimizer_type
        self.lr = lr
        self.loss = loss
        self.val_loss = val_loss

        self.net = FeedForward(
            input_size=self.new_input_size,
            output_size=len(data_adapter.target_columns),
            hidden_layer_sizes=hidden_layer_sizes,
            dropouts=dropouts,
            activation=activation,
            layer_norm=layer_norm,
            output_activation=output_activation,
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """
        performs forward step
        """
        # changes data shape from (batch_size, seq_len, n_features) into
        # (new_batch_size, seq_len * n_features)
        x = x.view(-1, self.new_input_size)
        return self.net.forward(x)

    def training_step(self, batch, batch_idx: int) -> float:
        """
        Performs training step on input and target data already
        processed by data_adapter
        """
        x, y = batch
        prediction = self(x)
        loss = self.loss(prediction, y)
        self.log("train_loss", loss)
        return loss

    def validation_step(self, batch, batch_idx: int) -> None:
        """
        Performs validation step on training data
        already preocessed by data_adapter but
        val_loss must be calculated based on
        rescaled data
        """
        x, y = batch
        output = self(x)
        prediction = self.__data_adapter.transform_target(output, inverse=True)
        y = self.__data_adapter.transform_target(y, inverse=True)
        val_loss = self.val_loss(prediction, y)
        self.log("val_loss", val_loss, prog_bar=True)

    def configure_optimizers(self) -> Dict[str, Any]:
        """
        returns optimizer
        """
        optimizer = self.optimizer_type(self.parameters(), lr=self.lr)
        scheduler = ReduceLROnPlateau(optimizer, "min", patience=5)
        return {
            "optimizer": optimizer,
            "lr_scheduler": scheduler,
            "monitor": "train_loss",
        }

    def get_trialed_model(
        trial: optuna.Trial, data_adapter: DataAdapter, val_loss: torch.nn.Module
    ) -> FeedForwardTunable:

        # generate loss function
        loss_name = trial.suggest_categorical(
            "loss_name", ["L1Loss", "MSELoss", "SmoothL1Loss"]
        )
        loss = getattr(torch.nn, loss_name)()

        # generate activation unit
        activation_name = trial.suggest_categorical(
            "activation_name", ["ReLU", "LeakyReLU"]
        )
        activation = getattr(torch.nn, activation_name)()

        # generate the optimizer
        optimizer_name = trial.suggest_categorical(
            "optimizer_name", ["Adam", "RMSprop"]
        )
        optimizer_type = getattr(torch.optim, optimizer_name)

        # generate learning rate
        lr = trial.suggest_float("lr", 1e-5, 1, log=True)

        # generate num hidden layers
        num_hidden_layers = trial.suggest_int("num_hidden_layers", 0, 7)

        # generate hidden layer sizes
        input_size = len(data_adapter.input_columns)
        output_size = len(data_adapter.target_columns)
        min_size = min(input_size * data_adapter.input_sequence_len, output_size)
        max_size = max(input_size * data_adapter.input_sequence_len, output_size)
        hidden_layer_sizes: List[int] = []
        for i in range(num_hidden_layers):
            hidden_layer_sizes.append(
                trial.suggest_int(f"hidden_layer_{i}_size", min_size, max_size)
            )

        # generate dropout probs
        dropout_pros: List[float] = []
        for i in range(num_hidden_layers):
            dropout_pros.append(trial.suggest_float(f"layer_{i}_dropout", 0, 0.5))

        return FeedForwardTunable(
            hidden_layer_sizes=hidden_layer_sizes,
            dropouts=dropout_pros,
            activation=activation,
            layer_norm=trial.suggest_categorical("layer_norm", [True, False]),
            output_activation=None,
            loss=loss,
            lr=lr,
            optimizer_type=optimizer_type,
            data_adapter=data_adapter,
            val_loss=val_loss,
        )

    @property
    def data_adapter(self) -> DataAdapter:
        return self.__data_adapter

    @staticmethod
    def suggested_params() -> Dict[str, Any]:
        return {
            "loss_name": "SmoothL1Loss",
            "optimizer_name": "Adam",
            "num_hidden_layers": 3,
            "hidden_layer_0_size": 40,
            "hidden_layer_1_size": 40,
            "hidden_layer_2_size": 20,
            "layer_0_dropout": 0.2,
            "layer_1_dropout": 0.2,
            "layer_2_dropout": 0.2,
            "layer_3_dropout": 0.2,
            "layer_norm": True,
        }

    @classmethod
    def load_model(cls, encoding: bytes) -> FeedForwardTunable:
        data = pickle.loads(encoding)
        hparams = data["hparams"]
        model = FeedForwardTunable(**hparams)
        model.net.load_state_dict(data["state"])
        return model

    def parse_model(self) -> bytes:
        data = {"state": self.net.state_dict(), "hparams": self.hparams}
        return pickle.dumps(data)
