genomCP = r"""<?xml version="1.0" encoding="ASCII"?>
<cp:ConstraintProblem xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:cp="http://www.paasage.eu/eu/paasage/upperware/metamodel/cp" xmlns:types="http://www.paasage.eu/eu/paasage/upperware/metamodel/types" id="GenomZPPSolverAppBig1618222719218">
  <constants id="0">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="1">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </constants>
  <constants id="cardinality_ComponentSparkWorker_min">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </constants>
  <constants id="cardinality_ComponentSparkWorker_max">
    <value xsi:type="types:IntegerValueUpperware" value="30"/>
  </constants>
  <constants id="provider_ComponentSparkWorker_min">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="provider_ComponentSparkWorker_max">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="cores_ComponentSparkWorker_min">
    <value xsi:type="types:IntegerValueUpperware" value="2"/>
  </constants>
  <constants id="cores_ComponentSparkWorker_max">
    <value xsi:type="types:IntegerValueUpperware" value="16"/>
  </constants>
  <constants id="ram_ComponentSparkWorker_min">
    <value xsi:type="types:IntegerValueUpperware" value="16384"/>
  </constants>
  <constants id="ram_ComponentSparkWorker_max">
    <value xsi:type="types:IntegerValueUpperware" value="70041"/>
  </constants>
  <constants id="provider_ComponentSparkWorker_0">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="cores_ComponentSparkWorker_min_p_0">
    <value xsi:type="types:IntegerValueUpperware" value="2"/>
  </constants>
  <constants id="cores_ComponentSparkWorker_max_p_0">
    <value xsi:type="types:IntegerValueUpperware" value="16"/>
  </constants>
  <constants id="ram_ComponentSparkWorker_min_p_0" type="Long">
    <value xsi:type="types:LongValueUpperware" value="16384"/>
  </constants>
  <constants id="ram_ComponentSparkWorker_max_p_0" type="Long">
    <value xsi:type="types:LongValueUpperware" value="70041"/>
  </constants>
  <constants id="constant_0" type="Double">
    <value xsi:type="types:DoubleValueUpperware" value="0.85"/>
  </constants>
  <constants id="constant_1">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <cpVariables id="WorkerCardinality" variableType="cardinality" componentId="ComponentSparkWorker">
    <domain xsi:type="cp:RangeDomain">
      <from xsi:type="types:IntegerValueUpperware" value="1"/>
      <to xsi:type="types:IntegerValueUpperware" value="30"/>
    </domain>
  </cpVariables>
  <cpVariables id="provider_ComponentSparkWorker" variableType="provider" componentId="ComponentSparkWorker">
    <domain xsi:type="cp:NumericListDomain">
      <values xsi:type="types:IntegerValueUpperware"/>
    </domain>
  </cpVariables>
  <cpVariables id="WorkerCores" variableType="cores" componentId="ComponentSparkWorker">
    <domain xsi:type="cp:NumericListDomain">
      <values xsi:type="types:IntegerValueUpperware" value="2"/>
      <values xsi:type="types:IntegerValueUpperware" value="4"/>
      <values xsi:type="types:IntegerValueUpperware" value="8"/>
      <values xsi:type="types:IntegerValueUpperware" value="16"/>
    </domain>
  </cpVariables>
  <cpVariables id="WorkerRam" variableType="ram" componentId="ComponentSparkWorker">
    <domain xsi:type="cp:NumericListDomain">
      <values xsi:type="types:IntegerValueUpperware" value="16384"/>
      <values xsi:type="types:IntegerValueUpperware" value="17510"/>
      <values xsi:type="types:IntegerValueUpperware" value="22528"/>
      <values xsi:type="types:IntegerValueUpperware" value="23552"/>
      <values xsi:type="types:IntegerValueUpperware" value="30720"/>
      <values xsi:type="types:IntegerValueUpperware" value="31232"/>
      <values xsi:type="types:IntegerValueUpperware" value="32768"/>
      <values xsi:type="types:IntegerValueUpperware" value="35020"/>
      <values xsi:type="types:IntegerValueUpperware" value="61952"/>
      <values xsi:type="types:IntegerValueUpperware" value="62464"/>
      <values xsi:type="types:IntegerValueUpperware" value="65536"/>
      <values xsi:type="types:IntegerValueUpperware" value="70041"/>
    </domain>
  </cpVariables>
  <constraints id="c_0" exp1="//@cpVariables.0" exp2="//@constants.2" comparator="greaterOrEqualTo"/>
  <constraints id="c_1" exp1="//@cpVariables.0" exp2="//@constants.3" comparator="lessOrEqualTo"/>
  <constraints id="c_2" exp1="//@cpVariables.1" exp2="//@constants.4" comparator="greaterOrEqualTo"/>
  <constraints id="c_3" exp1="//@cpVariables.1" exp2="//@constants.5" comparator="lessOrEqualTo"/>
  <constraints id="c_4" exp1="//@cpVariables.2" exp2="//@constants.6" comparator="greaterOrEqualTo"/>
  <constraints id="c_5" exp1="//@cpVariables.2" exp2="//@constants.7" comparator="lessOrEqualTo"/>
  <constraints id="c_6" exp1="//@cpVariables.3" exp2="//@constants.8" comparator="greaterOrEqualTo"/>
  <constraints id="c_7" exp1="//@cpVariables.3" exp2="//@constants.9" comparator="lessOrEqualTo"/>
  <constraints id="c_8" exp1="//@auxExpressions.2" exp2="//@constants.0" comparator="greaterOrEqualTo"/>
  <constraints id="c_9" exp1="//@auxExpressions.4" exp2="//@constants.0" comparator="greaterOrEqualTo"/>
  <constraints id="c_10" exp1="//@auxExpressions.6" exp2="//@constants.0" comparator="greaterOrEqualTo"/>
  <constraints id="c_11" exp1="//@auxExpressions.8" exp2="//@constants.0" comparator="greaterOrEqualTo"/>
  <constraints id="c_12" exp1="//@auxExpressions.11" exp2="//@constants.16"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_0" expressions="//@cpVariables.2 //@constants.11" operator="minus"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_1" expressions="//@cpVariables.1 //@constants.10" operator="eq"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_2" expressions="//@cpVariables.0 //@auxExpressions.1 //@auxExpressions.0" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_3" expressions="//@constants.12 //@cpVariables.2" operator="minus"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_4" expressions="//@cpVariables.0 //@auxExpressions.1 //@auxExpressions.3" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_5" expressions="//@cpVariables.3 //@constants.13" operator="minus"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_6" expressions="//@cpVariables.0 //@auxExpressions.1 //@auxExpressions.5" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_7" expressions="//@constants.14 //@cpVariables.3" operator="minus"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_8" expressions="//@cpVariables.0 //@auxExpressions.1 //@auxExpressions.7" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_lggzk" expressions="//@constants.15 //@cpMetrics.16" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_rggbu" expressions="//@cpVariables.0 //@cpVariables.2" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="WorkerCoresGreaterThanEstimatedCores" expressions="//@auxExpressions.10 //@auxExpressions.9" operator="minus"/>
  <cpMetrics id="SimulationLeftNumberRawMetric">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </cpMetrics>
  <cpMetrics id="ETPercentileRawMetric" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="SimulationElapsedTimeRawMetric">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </cpMetrics>
  <cpMetrics id="RawCPUUtilisation" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawDiskUtilisation" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawRAMUtilisation" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawGPUUtilisation" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawExecutionTime" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawResponseTime" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawNetworkLatency" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawAvailability" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawMTBF" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawCostDollars" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawCostEuros" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawInstanceNumber">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </cpMetrics>
  <cpMetrics id="RawCoreNumber">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </cpMetrics>
  <cpMetrics id="EstimatedCores">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </cpMetrics>
  <cpMetrics id="NotFinishedOnTime" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="EstimatedRemainingTime">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </cpMetrics>
  <cpMetrics id="WillFinishTooSoon" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageCPUUtilisation" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageDiskUtilisation" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageRAMUtilisation" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageGPUUtilisation" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageExecutionTime" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageResponseTime" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageNetworkLatency" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageAvailability" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageMTBF" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageCostDollars" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageCostEuros" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="ActWorkerCores">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </cpMetrics>
  <cpMetrics id="ActWorkerCardinality">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </cpMetrics>
</cp:ConstraintProblem>
"""
