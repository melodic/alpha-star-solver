import pytest
import pandas as pd
import numpy as np
import xml.etree.ElementTree as ET
from SupervisedGym.utils import DataConf
from SupervisedGym.utils.problem_analyzer import ProblemAnalyzer
from SupervisedGym.utils.data import Train_val_test_perc
from .fcrCP import fcrCP
from .genomCP import genomCP


data_config = DataConf(
    group_id="experimentId",
    pred_horizon=10,
    data_split=Train_val_test_perc(70, 15, 15),
    shuffle=False,
)


@pytest.mark.ProblemAnalyzer
def test_genom_domain():
    """
    tests if problem_analyzer returns genom domain
    from genom constraint problem.
    """

    analyzer = ProblemAnalyzer(data_config)

    xml_file = ET.fromstring(genomCP)
    assert analyzer._get_domains_from_constraint_problem(xml_file) == {
        "WorkerCardinality": range(1, 31),
        "provider_ComponentSparkWorker": [0],
        "WorkerCores": [2, 4, 8, 16],
        "WorkerRam": [
            16384,
            17510,
            22528,
            23552,
            30720,
            31232,
            32768,
            35020,
            61952,
            62464,
            65536,
            70041,
        ],
    }


@pytest.mark.ProblemAnalyzer
def test_FCR_domain():
    """
    tests if problem_analyzer returns FCR domain
    from FCR constraint problem.
    """

    analyzer = ProblemAnalyzer(data_config)

    xml_file = ET.fromstring(fcrCP)
    assert analyzer._get_domains_from_constraint_problem(xml_file) == {
        "AppCardinality": range(1, 41),
        "provider_Component_App": [0],
        "AppCores": [2, 4, 8, 16, 32, 36, 40, 48],
        "AppStorage": [0, 10, 420, 1024, 2097152],
        "LBCardinality": range(1, 2),
        "provider_Component_LB": [0],
        "LBRam": [
            1024,
            1740,
            2048,
            3750,
            3840,
            4096,
            7168,
            7680,
            8192,
            15360,
            15616,
            16384,
            17510,
            22528,
            23552,
            30720,
            31232,
            32768,
            35020,
            62464,
            70041,
        ],
        "LBStorage": [0, 10, 420],
        "DBCardinality": range(1, 2),
        "provider_Component_DB": [0],
        "DBCores": [1, 2, 4, 8],
        "DBStorage": [0, 10, 420],
    }


@pytest.mark.ProblemAnalyzer
def test_genom_static_conf():
    """
    tests if analyzer correctly
    generates time series static conf
    based on segment of genom data
    """

    input_preds = [
        "SimulationLeftNumberRawMetricPrediction",
        "NotFinishedOnTimePrediction",
        "EstimatedRemainingTimePrediction",
        "WillFinishTooSoonPrediction",
        "ETPercentileRawMetricPrediction",
        "MinimumCoresPrediction",
        "MinimumCoresContextPrediction",
    ]

    input_cols = [
        "SimulationElapsedTimeRawMetric",
        "SimulationLeftNumberRawMetric",
        "NotFinishedOnTime",
        "EstimatedRemainingTime",
        "WillFinishTooSoon",
        "ETPercentileRawMetric",
        "ActWorkerCardinality",
        "ActWorkerCores",
        "MinimumCoresReal",
        "MinimumCoresContextReal",
    ] + input_preds

    target_cols = [
        "WorkerCardinality",
        "provider_ComponentSparkWorker",
        "WorkerCores",
        "WorkerRam",
    ]
    cols = input_cols + target_cols + ["experimentId"]
    data = pd.DataFrame({col: np.random.randint(0, 10, 10).tolist() for col in cols})
    xml_file = ET.fromstring(genomCP)

    analyzer = ProblemAnalyzer(data_config)
    conf = analyzer.generate_tsdata_static_conf(data, xml_file)
    assert (conf["data"] == data).all(axis=None)
    assert conf["group_id"] == data_config["group_id"]
    assert conf["input_categorical"] == []
    assert conf["input_continuous"] == input_cols
    assert conf["target_continuous"] == target_cols
    assert conf["target_categorical"] == []
    assert conf["prediction_horizon"] == data_config["pred_horizon"]
    assert set(conf["input_predictions"]) == set(input_preds)
    assert conf["train_val_test_percentage"] == data_config["data_split"]
    assert conf["shuffle_groups"] == data_config["shuffle"]
