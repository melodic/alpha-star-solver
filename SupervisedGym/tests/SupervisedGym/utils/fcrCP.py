fcrCP = r"""<?xml version="1.0" encoding="ASCII"?>
<cp:ConstraintProblem xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:cp="http://www.paasage.eu/eu/paasage/upperware/metamodel/cp" xmlns:types="http://www.paasage.eu/eu/paasage/upperware/metamodel/types" id="FCRclear1579527639568">
  <constants id="0">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="1">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </constants>
  <constants id="cardinality_Component_App_min">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </constants>
  <constants id="cardinality_Component_App_max">
    <value xsi:type="types:IntegerValueUpperware" value="100"/>
  </constants>
  <constants id="provider_Component_App_min">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="provider_Component_App_max">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="cores_Component_App_min">
    <value xsi:type="types:IntegerValueUpperware" value="2"/>
  </constants>
  <constants id="cores_Component_App_max">
    <value xsi:type="types:IntegerValueUpperware" value="48"/>
  </constants>
  <constants id="storage_Component_App_min">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="storage_Component_App_max">
    <value xsi:type="types:IntegerValueUpperware" value="2097152"/>
  </constants>
  <constants id="provider_Component_App_0">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="cores_Component_App_min_p_0">
    <value xsi:type="types:IntegerValueUpperware" value="2"/>
  </constants>
  <constants id="cores_Component_App_max_p_0">
    <value xsi:type="types:IntegerValueUpperware" value="48"/>
  </constants>
  <constants id="storage_Component_App_min_p_0">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="storage_Component_App_max_p_0">
    <value xsi:type="types:IntegerValueUpperware" value="2097152"/>
  </constants>
  <constants id="cardinality_Component_LB_min">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </constants>
  <constants id="cardinality_Component_LB_max">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </constants>
  <constants id="provider_Component_LB_min">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="provider_Component_LB_max">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="ram_Component_LB_min">
    <value xsi:type="types:IntegerValueUpperware" value="1024"/>
  </constants>
  <constants id="ram_Component_LB_max">
    <value xsi:type="types:IntegerValueUpperware" value="70041"/>
  </constants>
  <constants id="storage_Component_LB_min">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="storage_Component_LB_max">
    <value xsi:type="types:IntegerValueUpperware" value="420"/>
  </constants>
  <constants id="provider_Component_LB_0">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="ram_Component_LB_min_p_0" type="Long">
    <value xsi:type="types:LongValueUpperware" value="1024"/>
  </constants>
  <constants id="ram_Component_LB_max_p_0" type="Long">
    <value xsi:type="types:LongValueUpperware" value="70041"/>
  </constants>
  <constants id="storage_Component_LB_min_p_0">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="storage_Component_LB_max_p_0">
    <value xsi:type="types:IntegerValueUpperware" value="420"/>
  </constants>
  <constants id="cardinality_Component_DB_min">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </constants>
  <constants id="cardinality_Component_DB_max">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </constants>
  <constants id="provider_Component_DB_min">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="provider_Component_DB_max">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="cores_Component_DB_min">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </constants>
  <constants id="cores_Component_DB_max">
    <value xsi:type="types:IntegerValueUpperware" value="8"/>
  </constants>
  <constants id="storage_Component_DB_min">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="storage_Component_DB_max">
    <value xsi:type="types:IntegerValueUpperware" value="420"/>
  </constants>
  <constants id="provider_Component_DB_0">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="cores_Component_DB_min_p_0">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </constants>
  <constants id="cores_Component_DB_max_p_0">
    <value xsi:type="types:IntegerValueUpperware" value="8"/>
  </constants>
  <constants id="storage_Component_DB_min_p_0">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="storage_Component_DB_max_p_0">
    <value xsi:type="types:IntegerValueUpperware" value="420"/>
  </constants>
  <constants id="constant_0">
    <value xsi:type="types:IntegerValueUpperware" value="60"/>
  </constants>
  <constants id="constant_1">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="constant_2">
    <value xsi:type="types:IntegerValueUpperware"/>
  </constants>
  <constants id="constant_3">
    <value xsi:type="types:IntegerValueUpperware" value="100"/>
  </constants>
  <cpVariables id="AppCardinality" variableType="cardinality" componentId="Component_App">
    <domain xsi:type="cp:RangeDomain">
      <from xsi:type="types:IntegerValueUpperware" value="1"/>
      <to xsi:type="types:IntegerValueUpperware" value="40"/>
    </domain>
  </cpVariables>
  <cpVariables id="provider_Component_App" variableType="provider" componentId="Component_App">
    <domain xsi:type="cp:NumericListDomain">
      <values xsi:type="types:IntegerValueUpperware"/>
    </domain>
  </cpVariables>
  <cpVariables id="AppCores" variableType="cores" componentId="Component_App">
    <domain xsi:type="cp:NumericListDomain">
      <values xsi:type="types:IntegerValueUpperware" value="2"/>
      <values xsi:type="types:IntegerValueUpperware" value="4"/>
      <values xsi:type="types:IntegerValueUpperware" value="8"/>
      <values xsi:type="types:IntegerValueUpperware" value="16"/>
      <values xsi:type="types:IntegerValueUpperware" value="32"/>
      <values xsi:type="types:IntegerValueUpperware" value="36"/>
      <values xsi:type="types:IntegerValueUpperware" value="40"/>
      <values xsi:type="types:IntegerValueUpperware" value="48"/>
    </domain>
  </cpVariables>
  <cpVariables id="AppStorage" variableType="storage" componentId="Component_App">
    <domain xsi:type="cp:NumericListDomain">
      <values xsi:type="types:IntegerValueUpperware"/>
      <values xsi:type="types:IntegerValueUpperware" value="10"/>
      <values xsi:type="types:IntegerValueUpperware" value="420"/>
      <values xsi:type="types:IntegerValueUpperware" value="1024"/>
      <values xsi:type="types:IntegerValueUpperware" value="2097152"/>
    </domain>
  </cpVariables>
  <cpVariables id="LBCardinality" variableType="cardinality" componentId="Component_LB">
    <domain xsi:type="cp:RangeDomain">
      <from xsi:type="types:IntegerValueUpperware" value="1"/>
      <to xsi:type="types:IntegerValueUpperware" value="1"/>
    </domain>
  </cpVariables>
  <cpVariables id="provider_Component_LB" variableType="provider" componentId="Component_LB">
    <domain xsi:type="cp:NumericListDomain">
      <values xsi:type="types:IntegerValueUpperware"/>
    </domain>
  </cpVariables>
  <cpVariables id="LBRam" variableType="ram" componentId="Component_LB">
    <domain xsi:type="cp:NumericListDomain">
      <values xsi:type="types:IntegerValueUpperware" value="1024"/>
      <values xsi:type="types:IntegerValueUpperware" value="1740"/>
      <values xsi:type="types:IntegerValueUpperware" value="2048"/>
      <values xsi:type="types:IntegerValueUpperware" value="3750"/>
      <values xsi:type="types:IntegerValueUpperware" value="3840"/>
      <values xsi:type="types:IntegerValueUpperware" value="4096"/>
      <values xsi:type="types:IntegerValueUpperware" value="7168"/>
      <values xsi:type="types:IntegerValueUpperware" value="7680"/>
      <values xsi:type="types:IntegerValueUpperware" value="8192"/>
      <values xsi:type="types:IntegerValueUpperware" value="15360"/>
      <values xsi:type="types:IntegerValueUpperware" value="15616"/>
      <values xsi:type="types:IntegerValueUpperware" value="16384"/>
      <values xsi:type="types:IntegerValueUpperware" value="17510"/>
      <values xsi:type="types:IntegerValueUpperware" value="22528"/>
      <values xsi:type="types:IntegerValueUpperware" value="23552"/>
      <values xsi:type="types:IntegerValueUpperware" value="30720"/>
      <values xsi:type="types:IntegerValueUpperware" value="31232"/>
      <values xsi:type="types:IntegerValueUpperware" value="32768"/>
      <values xsi:type="types:IntegerValueUpperware" value="35020"/>
      <values xsi:type="types:IntegerValueUpperware" value="62464"/>
      <values xsi:type="types:IntegerValueUpperware" value="70041"/>
    </domain>
  </cpVariables>
  <cpVariables id="LBStorage" variableType="storage" componentId="Component_LB">
    <domain xsi:type="cp:NumericListDomain">
      <values xsi:type="types:IntegerValueUpperware"/>
      <values xsi:type="types:IntegerValueUpperware" value="10"/>
      <values xsi:type="types:IntegerValueUpperware" value="420"/>
    </domain>
  </cpVariables>
  <cpVariables id="DBCardinality" variableType="cardinality" componentId="Component_DB">
    <domain xsi:type="cp:RangeDomain">
      <from xsi:type="types:IntegerValueUpperware" value="1"/>
      <to xsi:type="types:IntegerValueUpperware" value="1"/>
    </domain>
  </cpVariables>
  <cpVariables id="provider_Component_DB" variableType="provider" componentId="Component_DB">
    <domain xsi:type="cp:NumericListDomain">
      <values xsi:type="types:IntegerValueUpperware"/>
    </domain>
  </cpVariables>
  <cpVariables id="DBCores" variableType="cores" componentId="Component_DB">
    <domain xsi:type="cp:NumericListDomain">
      <values xsi:type="types:IntegerValueUpperware" value="1"/>
      <values xsi:type="types:IntegerValueUpperware" value="2"/>
      <values xsi:type="types:IntegerValueUpperware" value="4"/>
      <values xsi:type="types:IntegerValueUpperware" value="8"/>
    </domain>
  </cpVariables>
  <cpVariables id="DBStorage" variableType="storage" componentId="Component_DB">
    <domain xsi:type="cp:NumericListDomain">
      <values xsi:type="types:IntegerValueUpperware"/>
      <values xsi:type="types:IntegerValueUpperware" value="10"/>
      <values xsi:type="types:IntegerValueUpperware" value="420"/>
    </domain>
  </cpVariables>
  <constraints id="c_0" exp1="//@cpVariables.0" exp2="//@constants.2" comparator="greaterOrEqualTo"/>
  <constraints id="c_1" exp1="//@cpVariables.0" exp2="//@constants.3" comparator="lessOrEqualTo"/>
  <constraints id="c_2" exp1="//@cpVariables.1" exp2="//@constants.4" comparator="greaterOrEqualTo"/>
  <constraints id="c_3" exp1="//@cpVariables.1" exp2="//@constants.5" comparator="lessOrEqualTo"/>
  <constraints id="c_4" exp1="//@cpVariables.2" exp2="//@constants.6" comparator="greaterOrEqualTo"/>
  <constraints id="c_5" exp1="//@cpVariables.2" exp2="//@constants.7" comparator="lessOrEqualTo"/>
  <constraints id="c_6" exp1="//@cpVariables.3" exp2="//@constants.8" comparator="greaterOrEqualTo"/>
  <constraints id="c_7" exp1="//@cpVariables.3" exp2="//@constants.9" comparator="lessOrEqualTo"/>
  <constraints id="c_8" exp1="//@auxExpressions.2" exp2="//@constants.0" comparator="greaterOrEqualTo"/>
  <constraints id="c_9" exp1="//@auxExpressions.4" exp2="//@constants.0" comparator="greaterOrEqualTo"/>
  <constraints id="c_10" exp1="//@auxExpressions.6" exp2="//@constants.0" comparator="greaterOrEqualTo"/>
  <constraints id="c_11" exp1="//@auxExpressions.8" exp2="//@constants.0" comparator="greaterOrEqualTo"/>
  <constraints id="c_12" exp1="//@cpVariables.4" exp2="//@constants.15" comparator="greaterOrEqualTo"/>
  <constraints id="c_13" exp1="//@cpVariables.4" exp2="//@constants.16" comparator="lessOrEqualTo"/>
  <constraints id="c_14" exp1="//@cpVariables.5" exp2="//@constants.17" comparator="greaterOrEqualTo"/>
  <constraints id="c_15" exp1="//@cpVariables.5" exp2="//@constants.18" comparator="lessOrEqualTo"/>
  <constraints id="c_16" exp1="//@cpVariables.6" exp2="//@constants.19" comparator="greaterOrEqualTo"/>
  <constraints id="c_17" exp1="//@cpVariables.6" exp2="//@constants.20" comparator="lessOrEqualTo"/>
  <constraints id="c_18" exp1="//@cpVariables.7" exp2="//@constants.21" comparator="greaterOrEqualTo"/>
  <constraints id="c_19" exp1="//@cpVariables.7" exp2="//@constants.22" comparator="lessOrEqualTo"/>
  <constraints id="c_20" exp1="//@auxExpressions.11" exp2="//@constants.0" comparator="greaterOrEqualTo"/>
  <constraints id="c_21" exp1="//@auxExpressions.13" exp2="//@constants.0" comparator="greaterOrEqualTo"/>
  <constraints id="c_22" exp1="//@auxExpressions.15" exp2="//@constants.0" comparator="greaterOrEqualTo"/>
  <constraints id="c_23" exp1="//@auxExpressions.17" exp2="//@constants.0" comparator="greaterOrEqualTo"/>
  <constraints id="c_24" exp1="//@cpVariables.8" exp2="//@constants.28" comparator="greaterOrEqualTo"/>
  <constraints id="c_25" exp1="//@cpVariables.8" exp2="//@constants.29" comparator="lessOrEqualTo"/>
  <constraints id="c_26" exp1="//@cpVariables.9" exp2="//@constants.30" comparator="greaterOrEqualTo"/>
  <constraints id="c_27" exp1="//@cpVariables.9" exp2="//@constants.31" comparator="lessOrEqualTo"/>
  <constraints id="c_28" exp1="//@cpVariables.10" exp2="//@constants.32" comparator="greaterOrEqualTo"/>
  <constraints id="c_29" exp1="//@cpVariables.10" exp2="//@constants.33" comparator="lessOrEqualTo"/>
  <constraints id="c_30" exp1="//@cpVariables.11" exp2="//@constants.34" comparator="greaterOrEqualTo"/>
  <constraints id="c_31" exp1="//@cpVariables.11" exp2="//@constants.35" comparator="lessOrEqualTo"/>
  <constraints id="c_32" exp1="//@auxExpressions.20" exp2="//@constants.0" comparator="greaterOrEqualTo"/>
  <constraints id="c_33" exp1="//@auxExpressions.22" exp2="//@constants.0" comparator="greaterOrEqualTo"/>
  <constraints id="c_34" exp1="//@auxExpressions.24" exp2="//@constants.0" comparator="greaterOrEqualTo"/>
  <constraints id="c_35" exp1="//@auxExpressions.26" exp2="//@constants.0" comparator="greaterOrEqualTo"/>
  <constraints id="c_36" exp1="//@auxExpressions.29" exp2="//@constants.42" comparator="lessOrEqualTo"/>
  <constraints id="c_37" exp1="//@auxExpressions.33" exp2="//@constants.43" comparator="lessOrEqualTo"/>
  <constraints id="c_38" exp1="//@auxExpressions.38" exp2="//@constants.44"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_0" expressions="//@cpVariables.2 //@constants.11" operator="minus"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_1" expressions="//@cpVariables.1 //@constants.10" operator="eq"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_2" expressions="//@cpVariables.0 //@auxExpressions.1 //@auxExpressions.0" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_3" expressions="//@constants.12 //@cpVariables.2" operator="minus"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_4" expressions="//@cpVariables.0 //@auxExpressions.1 //@auxExpressions.3" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_5" expressions="//@cpVariables.3 //@constants.13" operator="minus"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_6" expressions="//@cpVariables.0 //@auxExpressions.1 //@auxExpressions.5" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_7" expressions="//@constants.14 //@cpVariables.3" operator="minus"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_8" expressions="//@cpVariables.0 //@auxExpressions.1 //@auxExpressions.7" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_9" expressions="//@cpVariables.6 //@constants.24" operator="minus"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_10" expressions="//@cpVariables.5 //@constants.23" operator="eq"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_11" expressions="//@cpVariables.4 //@auxExpressions.10 //@auxExpressions.9" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_12" expressions="//@constants.25 //@cpVariables.6" operator="minus"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_13" expressions="//@cpVariables.4 //@auxExpressions.10 //@auxExpressions.12" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_14" expressions="//@cpVariables.7 //@constants.26" operator="minus"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_15" expressions="//@cpVariables.4 //@auxExpressions.10 //@auxExpressions.14" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_16" expressions="//@constants.27 //@cpVariables.7" operator="minus"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_17" expressions="//@cpVariables.4 //@auxExpressions.10 //@auxExpressions.16" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_18" expressions="//@cpVariables.10 //@constants.37" operator="minus"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_19" expressions="//@cpVariables.9 //@constants.36" operator="eq"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_20" expressions="//@cpVariables.8 //@auxExpressions.19 //@auxExpressions.18" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_21" expressions="//@constants.38 //@cpVariables.10" operator="minus"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_22" expressions="//@cpVariables.8 //@auxExpressions.19 //@auxExpressions.21" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_23" expressions="//@cpVariables.11 //@constants.39" operator="minus"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_24" expressions="//@cpVariables.8 //@auxExpressions.19 //@auxExpressions.23" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_25" expressions="//@constants.40 //@cpVariables.11" operator="minus"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_expression_26" expressions="//@cpVariables.8 //@auxExpressions.19 //@auxExpressions.25" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_mkabi" expressions="//@cpMetrics.15 //@constants.41" operator="div"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_qppuz" expressions="//@cpVariables.0 //@cpVariables.2" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="EnoughCores" expressions="//@auxExpressions.27 //@auxExpressions.28" operator="minus"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_petvx" expressions="//@cpMetrics.16 //@constants.3" operator="div"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_wgidc" expressions="//@auxExpressions.30 //@cpMetrics.28" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_flupp" expressions="//@cpVariables.2 //@cpVariables.0" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="NewConfigurationIsBetter" expressions="//@auxExpressions.31 //@auxExpressions.32" operator="minus"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_ljoke" expressions="//@cpVariables.4 //@cpVariables.7" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_ugdhk" expressions="//@cpVariables.8 //@cpVariables.11" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_gruee" expressions="//@cpVariables.0 //@cpVariables.3" operator="times"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="aux_buizt" expressions="//@auxExpressions.34 //@auxExpressions.35"/>
  <auxExpressions xsi:type="cp:ComposedExpression" id="OverallStorage" expressions="//@auxExpressions.37 //@auxExpressions.36"/>
  <cpMetrics id="NumberOfUsersPerSecond">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </cpMetrics>
  <cpMetrics id="AppResponseTime">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </cpMetrics>
  <cpMetrics id="RawCPUUtilisation" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawDiskUtilisation" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawRAMUtilisation" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawGPUUtilisation" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawExecutionTime" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawResponseTime" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawNetworkLatency" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawAvailability" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawMTBF" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawCostDollars" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawCostEuros" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="RawInstanceNumber">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </cpMetrics>
  <cpMetrics id="RawCoreNumber">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </cpMetrics>
  <cpMetrics id="TotalUsers">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </cpMetrics>
  <cpMetrics id="AvgResponseTime" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageCPUUtilisation" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageDiskUtilisation" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageRAMUtilisation" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageGPUUtilisation" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageExecutionTime" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageResponseTime" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageNetworkLatency" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageAvailability" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageMTBF" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageCostDollars" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AverageCostEuros" type="Float">
    <value xsi:type="types:FloatValueUpperware" value="1.0"/>
  </cpMetrics>
  <cpMetrics id="AppActCores">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </cpMetrics>
  <cpMetrics id="AppActCardinality">
    <value xsi:type="types:IntegerValueUpperware" value="1"/>
  </cpMetrics>
</cp:ConstraintProblem>
"""
