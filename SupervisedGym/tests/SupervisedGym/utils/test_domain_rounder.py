import pytest
from SupervisedGym.utils.domain_rounder import DomainRounder


@pytest.mark.DomainRounder
def test_domain_ints():
    """
    tests if domain rounder correctly
    rounds this peace of data
    """

    rounder = DomainRounder({"A": (1, 5, 10, 15, 20), "B": list(range(10))})

    assert rounder.round({"A": 1.1, "B": 0}) == {"A": 1, "B": 0}
    assert rounder.round({"A": 0.1, "B": 0}) == {"A": 1, "B": 0}
    assert rounder.round({"A": 0.0, "B": 9}) == {"A": 1, "B": 9}
    assert rounder.round({"A": 4.51, "B": 9}) == {"A": 5, "B": 9}
    assert rounder.round({"A": -0.3, "B": 11.1}) == {"A": 1, "B": 9}


@pytest.mark.DomainRounder
def test_domain_floats_and_singletons():
    """
    tests if domain rounder correctly
    rounds this peace of data
    """

    rounder = DomainRounder({"A": [1.1, 5.0, 10.3, 15.5, 20.1], "B": [3.3]})

    assert rounder.round({"A": 1.1, "B": 3.3}) == {"A": 1.1, "B": 3.3}
    assert rounder.round({"A": 5.0, "B": 10}) == {"A": 5.0, "B": 3.3}
    assert rounder.round({"A": 0.12, "B": 9}) == {"A": 1.1, "B": 3.3}
    assert rounder.round({"A": 30.1, "B": 9}) == {"A": 20.1, "B": 3.3}
    assert rounder.round({"A": 13.1, "B": 11.1}) == {"A": 15.5, "B": 3.3}
