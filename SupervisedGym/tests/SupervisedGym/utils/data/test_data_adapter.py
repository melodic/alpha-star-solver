from SupervisedGym.utils.data.TimeSeriesDataSet.timeseries_data import TimeSeriesData
import numpy as np
import torch
import pandas as pd

from SupervisedGym.utils.data import DataAdapter
from sklearn.preprocessing import MaxAbsScaler, RobustScaler


def test_transformed_tensor():
    """
    tests if tensor after being transformed
    by data_adapter still has its dtype
    only for floats, and does not have grad
    """
    torch_dtypes = [torch.float32, torch.float64, torch.float16]

    scaler = MaxAbsScaler()
    # with this fit scaler should scale "down"
    scaler.fit(np.random.rand(100, 1) / 10)

    adapter = DataAdapter(
        input_sequence_len=1,
        input_transformer=scaler,
        target_transformer=None,
        input_columns=["in"],
        target_columns=["out"],
    )

    for i, dtype in enumerate(torch_dtypes):
        t = torch.from_numpy(np.random.rand(1)).to(dtype).view(1, 1)
        if i % 2 == 0:
            t = t.clone().detach().requires_grad_(True)

        init_dtype = t.dtype
        t = adapter.transform_input(t)
        assert t.dtype == init_dtype
        assert not t.requires_grad


def test_inverse_transformation():
    """
    tests if inverse transformation
    brings back orginal values with
    some margin of error
    """
    ERR = 1e-5
    COLS = 2
    BATCH_SIZE = 16
    SIZE = 50

    data = pd.DataFrame(
        {
            "IN1": np.random.rand(SIZE),
            "IN2": np.random.rand(SIZE),
            "OUT1": np.random.rand(SIZE),
            "OUT2": np.random.rand(SIZE),
            "ID": np.arange(SIZE) // 25,
        }
    )
    tsdata = TimeSeriesData(
        data=data,
        group_id="ID",
        input_continuous=["IN1", "IN2"],
        input_categorical=[],
        target_continuous=["OUT1", "OUT2"],
        target_categorical=[],
        input_predictions=[],
        input_sequence_len=3,
        prediction_horizon=5,
        input_scaler_type=MaxAbsScaler,
        target_scaler_type=RobustScaler,
    )
    adapter = DataAdapter.create_from_timeseries_data(tsdata)

    for _ in range(10):
        t = torch.rand((BATCH_SIZE, 5, COLS)) * 13
        same_input = adapter.transform_input(adapter.transform_input(t), inverse=True)
        same_targ = adapter.transform_target(adapter.transform_target(t), inverse=True)
        assert (torch.abs(t - same_input) < ERR).all()
        assert (torch.abs(t - same_targ) < ERR).all()
