import numpy as np
from sklearn.preprocessing import (
    StandardScaler,
    MaxAbsScaler,
    RobustScaler,
    MinMaxScaler,
)

from SupervisedGym.utils.data import ColumnTransformer
from SupervisedGym.utils.data.TimeSeriesDataSet.batched_transformer_minix import (
    BatchedTransformerMinix,
)


def test_column_transformer():
    """
    tests if column transformer
    correctly fits and transforms
    2D data
    """

    MAX_ERR = 1e-8
    N_SAMPLES = 13
    N_FEATURES = 3

    for _ in range(5):
        data = np.random.randn(N_SAMPLES, N_FEATURES) * 100
        col_transformer = ColumnTransformer(
            [StandardScaler(), MaxAbsScaler(), StandardScaler()]
        )
        col_transformer.fit(data)

        transformed = col_transformer.transform(data)
        supposed_org_data = col_transformer.inverse_transform(transformed)

        assert data.shape == supposed_org_data.shape
        assert (np.all(data - supposed_org_data) < MAX_ERR).all()


def test_transforming_batched_data():
    """
    tests if ColumnTransformer can scale
    data that is given in shape of
    (batch_size, n_samples, n_features)
    """
    MAX_ERR = 1e-8
    BATCH_SIZE = 8
    N_SAMPLES = 15
    N_FEATURES = 4

    data = np.random.rand(BATCH_SIZE, N_SAMPLES, N_FEATURES)

    transformer2D = ColumnTransformer(
        [StandardScaler(), MaxAbsScaler(), RobustScaler(), MinMaxScaler()]
    )
    transformer = BatchedTransformerMinix(transformer2D)
    transformer.fit(data)
    transformed_data = transformer.transform(data)
    supposed_org_data = transformer.inverse_transform(transformed_data)

    assert data.shape == supposed_org_data.shape
    assert (np.abs(data - supposed_org_data) < MAX_ERR).all()
