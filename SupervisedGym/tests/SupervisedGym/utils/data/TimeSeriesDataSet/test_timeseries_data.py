import pytest
import pandas as pd
import numpy as np
from pytest import approx
from typing import Set, Tuple
from torch.utils.data import DataLoader
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from itertools import chain

from SupervisedGym.utils.data import TimeSeriesData, TsDataset, Train_val_test_perc


@pytest.mark.TimeSeriesData
def test_identity():
    """
    tests if timeseries dataloader with bunch of random data
    correctly returns data with no shift and no prediction
    and no scaling
    """

    rand_a = np.random.randint(0, 100, size=(100, 4))

    data = pd.DataFrame(
        np.hstack(
            [
                rand_a,
                rand_a,
                np.arange(100).reshape(100, 1) // 10,
            ]
        ),
        columns=list("abcdABCDI"),
    )

    tsdataset = TimeSeriesData(
        data=data,
        input_continuous=list("abcd"),
        input_categorical=[],
        target_continuous=list("ABCD"),
        target_categorical=[],
        group_id="I",
        prediction_horizon=0,
        input_sequence_len=1,
        input_scaler_type=StandardScaler,
        target_scaler_type=StandardScaler,
    )

    train_ds = tsdataset.generate_train_dataset()
    loader = DataLoader(dataset=train_ds)

    for x, y in loader:
        assert (x[-1] == y).all()


@pytest.mark.TimeSeriesData
def test_prediction():
    """
    tests if:
        1. returned target with nonzero prediction is correct
        2. returned input is correct
    """

    SIZE = 50
    data = pd.DataFrame(
        np.hstack(
            [np.arange(SIZE).reshape(SIZE, 1) for _ in range(3)]
            + [np.arange(SIZE).reshape(SIZE, 1) // 17]
        ),
        columns=["AA", "BB", "BBpredicted", "Id-Row__xd"],
    )

    tsdataset = TimeSeriesData(
        data=data,
        group_id="Id-Row__xd",
        input_continuous=["AA", "BBpredicted"],
        input_categorical=[],
        target_continuous=["BBpredicted"],
        target_categorical=[],
        prediction_horizon=5,
        input_sequence_len=7,
    )

    for i in range(len(tsdataset)):
        x, y = tsdataset[i]
        for i, row in enumerate(x):
            target = row[1]
            assert all(target + (7 - i - 1) + 5 == y)


@pytest.mark.TimeSeriesData
def test_input_output_order():
    """
    tests if:
        1. returned target columns have correct order
        2. returned input columns have correct order
    """

    COLS = list("ABC")
    array = np.vstack([np.arange(4).reshape(1, 4) for _ in range(5)])

    data = pd.DataFrame(array, columns=COLS + ["experimentID"])

    desired_input_order = ["B", "A", "C"]
    desired_output_order = ["A", "B"]

    tsdataset = TimeSeriesData(
        data=data,
        group_id="experimentID",
        input_continuous=["B", "A"],
        input_categorical=["C"],
        target_continuous=["A", "B"],
        target_categorical=[],
        prediction_horizon=0,
        input_sequence_len=1,
    )

    assert tsdataset.input_columns == desired_input_order
    assert tsdataset.target_columns == desired_output_order

    for i in range(len(tsdataset)):
        x, y = tsdataset[i]
        for row in x:
            assert (row[0] == 1).all()  # B
            assert (row[1] == 0).all()  # A
            assert (row[2] == 2).all()  # C

            assert (y[0] == 0).all()  # A
            assert (y[1] == 1).all()  # B


@pytest.mark.TimeSeriesData
def test_group_too_small():
    """
    tests if ValueError is raised when too small
    data group is provided
    """

    data = pd.DataFrame(
        np.hstack(
            [
                np.random.randint(0, 100, size=(20, 3)),
                np.array(
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2]
                ).reshape(20, 1),
            ]
        ),
        columns=["COL1", "COL2", "COL3", "GROUPID"],
    )

    try:
        # asser no throw
        TimeSeriesData(
            data=data,
            group_id="GROUPID",
            input_continuous=["COL1", "COL2"],
            input_categorical=[],
            target_continuous=["COL3"],
            target_categorical=[],
            prediction_horizon=2,
            input_sequence_len=2,
        )
    except ValueError:
        pytest.fail("TimeSeriesDataset thrown unexpected exception")

    # assert ValueError is thrown
    with pytest.raises(ValueError):
        TimeSeriesData(
            data=data,
            group_id="GROUPID",
            input_continuous=["COL1", "COL2"],
            input_categorical=[],
            target_continuous=["COL3"],
            target_categorical=[],
            prediction_horizon=2,
            input_sequence_len=3,
        )


@pytest.mark.TimeSeriesData
def test_dataloaders_complete_each_other():
    """
    test if generated tsdataloaders return unique data
    of requested size
    """

    SIZE = 10 ** 2
    array = np.hstack(
        [np.arange(SIZE).reshape(SIZE, 1) for _ in range(3)]
        + [np.zeros(SIZE).reshape(SIZE, 1)]
    )

    data = pd.DataFrame(array, columns=["C1", "C2", "C3", "ID"])

    tsdata = TimeSeriesData(
        data=data,
        group_id="ID",
        input_categorical=["C1", "C2"],
        input_continuous=[],
        target_continuous=[],
        target_categorical=["C3"],
        prediction_horizon=0,
        input_sequence_len=1,
        train_val_test_percentage=Train_val_test_perc(60, 30, 10),
        shuffle_groups=True,
    )

    train_dataloader = tsdata.generate_train_dataset()
    val_dataloader = tsdata.generate_validation_dataset()
    test_dataloader = tsdata.generate_test_dataset()

    total_len = len(tsdata)

    assert len(train_dataloader) / total_len == approx(0.6, abs=1e-1)
    assert len(val_dataloader) / total_len == approx(0.3, abs=1e-1)
    assert len(test_dataloader) / total_len == approx(0.1, abs=1e-1)

    assert len(train_dataloader) + len(val_dataloader) + len(test_dataloader) == SIZE

    def ds_targets(ds: TsDataset) -> Set[Tuple[int, int, int]]:
        """
        returns set of targets from given dataset
        """
        dloader = DataLoader(dataset=ds, batch_size=16, shuffle=True)
        rez = set()
        for _, y_batch in dloader:
            for y in y_batch:
                rez.add(tuple(int(v) for v in y))
        return rez

    train_targets = ds_targets(train_dataloader)
    val_targets = ds_targets(val_dataloader)
    test_targets = ds_targets(test_dataloader)

    # test if dataloaders data is unique
    assert len(train_targets.intersection(val_targets)) == 0
    assert len(train_targets.intersection(test_targets)) == 0
    assert len(val_targets.intersection(test_targets)) == 0

    # test if all targets are contained in train, test, or val
    all_targets = {tuple(int(v) for v in tsdata[i][1]) for i in range(len(tsdata))}
    assert train_targets.union(val_targets).union(test_targets) == all_targets


@pytest.mark.TimeSeriesData
def test_input_prediction_values_shift():
    """
    test if input values that are marked
    as prediction are shifted correctly
    """

    SIZE = 10 ** 2
    arr = np.random.randint(0, 100, SIZE)
    data = pd.DataFrame({"A": arr, "A_pred": arr, "ID": np.zeros(SIZE)})

    tsdata = TimeSeriesData(
        data=data,
        group_id="ID",
        input_continuous=[],
        input_categorical=["A", "A_pred"],
        target_continuous=[],
        target_categorical=["A"],
        prediction_horizon=17,
        input_sequence_len=21,
        input_predictions=["A_pred"],
        shuffle_groups=True,
    )

    for i in range(len(tsdata)):
        x, y = tsdata[i]
        assert x[-1][1] == y


@pytest.mark.TimeSeriesData
def test_not_consistent_continous_categorical():
    """
    tests if exception is risen if some value
    is provided as categorical and continuous
    """
    SIZE = 50
    data = pd.DataFrame(
        {
            "cat": np.random.randint(0, 3, SIZE),
            "cont": np.random.uniform(SIZE),
            "ID": np.arange(SIZE) // 10,
        }
    )

    # cont is given as categorical
    with pytest.raises(ValueError):
        TimeSeriesData(
            data=data,
            group_id="ID",
            input_continuous=["cont"],
            input_categorical=["cat", "cont"],
            target_categorical=["cat"],
            target_continuous=[],
            prediction_horizon=3,
            input_sequence_len=7,
        )

    # cat is given as continuous
    with pytest.raises(ValueError):
        TimeSeriesData(
            data=data,
            group_id="ID",
            input_continuous=["cont"],
            input_categorical=["cat"],
            target_categorical=["cat"],
            target_continuous=["cont", "cat"],
            prediction_horizon=3,
            input_sequence_len=7,
        )


@pytest.mark.TimeSeriesData
def test_getters():
    """
    tests if TimeSeriesData getter
    are working properly
    """
    SIZE = 5
    data = pd.DataFrame(
        {
            "cont1": [1, 2, 3],
            "cont2": [3, 6, 11],
            "cat1": [1.3, 3.5, 8.99],
            "cat2": [1.11, 2.0, 1.99],
            "id": [0, 0, 0],
        }
    )

    tsdata = TimeSeriesData(
        data=data,
        group_id="id",
        input_continuous=["cont1", "cont2"],
        input_categorical=["cat1", "cat2"],
        target_categorical=["cat1", "cat2"],
        target_continuous=["cont1"],
        prediction_horizon=0,
        input_sequence_len=1,
    )

    assert tsdata.columns == ["cont1", "cont2", "cat1", "cat2", "cont1", "cat1", "cat2"]
    assert tsdata.input_columns == ["cont1", "cont2", "cat1", "cat2"]
    assert tsdata.target_columns == ["cont1", "cat1", "cat2"]

    assert tsdata.input_continuous == ["cont1", "cont2"]
    assert tsdata.input_categorical == ["cat1", "cat2"]
    assert tsdata.target_continuous == ["cont1"]
    assert tsdata.target_categorical == ["cat1", "cat2"]

    assert tsdata.prediction_horizon == 0
    assert tsdata.input_sequence_len == 1


@pytest.mark.TimeSeriesData
def test_standard_scaling():
    """
    test if with provided StandardScaler as
    input scaler or target scaler, returned input data
    has mean = 0, std = 1.
    """

    SIZE = 100
    data = pd.DataFrame(
        {
            "cat1": np.random.randint(-20, 10, SIZE),
            "cat2": np.random.randint(-20, 10, SIZE),
            "cont1": np.random.uniform(low=20, high=30, size=SIZE),
            "cont2": np.random.uniform(low=20, high=30, size=SIZE),
            "ID": np.arange(SIZE) // 20,
        }
    )

    tsdata = TimeSeriesData(
        data=data,
        group_id="ID",
        input_continuous=["cont1"],
        input_categorical=["cat1", "cat2"],
        target_continuous=["cont1", "cont2"],
        target_categorical=["cat1"],
        prediction_horizon=0,
        input_sequence_len=1,
        input_predictions=[],
        input_scaler_type=StandardScaler,
        target_scaler_type=StandardScaler,
    )

    dataset = tsdata.generate_train_dataset()
    train_dloader = DataLoader(dataset=dataset, batch_size=len(dataset))

    x, y = next(iter(train_dloader))

    x = x.squeeze().numpy()
    x_mean = np.mean(x, axis=0)
    x_std = np.std(x, axis=0)

    y = y.squeeze().numpy()
    y_mean = np.mean(y, axis=0)
    y_std = np.std(y, axis=0)

    MAX_ERR = 5 * 51e-1

    for err in x_mean:
        assert abs(err) < MAX_ERR

    for err in y_mean:
        assert abs(err) < MAX_ERR

    for err in np.abs(x_std - 1):
        assert abs(err) < MAX_ERR

    for err in np.abs(y_std - 1):
        assert abs(err) < MAX_ERR


@pytest.mark.TimeSeriesData
def test_data_transformers():
    """
    test if with returned by timeseriesdata
    input and target transformers correctly
    rescale data from genrated dataloaders
    """

    SIZE = 100
    cat = np.random.randint(-20, 10, SIZE)
    cont = np.random.uniform(low=20, high=30, size=SIZE)
    data = pd.DataFrame(
        {
            "cat1": cat,
            "cat2": cat,
            "cont1": cont,
            "cont2": cont,
            "ID": np.arange(SIZE) // 20,
        }
    )

    tsdata = TimeSeriesData(
        data=data,
        group_id="ID",
        input_continuous=["cont1"],
        input_categorical=["cat1"],
        target_continuous=["cont2"],
        target_categorical=["cat2"],
        prediction_horizon=0,
        input_sequence_len=1,
        input_predictions=[],
        input_scaler_type=MinMaxScaler,
        target_scaler_type=None,
    )

    MAX_ERR = 1e-3

    dataset = tsdata.generate_test_dataset()
    x_transformer = tsdata.input_transformer
    for i in range(len(dataset)):
        x, y = dataset[i]
        # x should be already transformed
        # y should be orginal value of x
        x_retransformed = x_transformer.inverse_transform(x.numpy())
        np.abs((x_retransformed - y.numpy()) < MAX_ERR).all()


@pytest.mark.TimeSeriesData
def test_generated_uniform_sampler():
    """
    tests if generated random sample applied in
    dataloader makes dataloader return target data
    with uniform distribution
    """

    data = pd.DataFrame(
        {"notUniform": [0] * 100 + [1] * 600 + [2] * 300, "id": [0] * 1000}
    )

    tsdata = TimeSeriesData(
        data=data,
        group_id="id",
        input_continuous=["notUniform"],
        input_categorical=[],
        target_continuous=["notUniform"],
        target_categorical=[],
        prediction_horizon=0,
        input_sequence_len=1,
        train_val_test_percentage=Train_val_test_perc(100, 0, 0),
    )

    dataset = tsdata.generate_train_dataset()
    dataloader = DataLoader(
        dataset=dataset, batch_size=1, sampler=tsdata.generate_uniform_target_sampler()
    )

    occurs = {0: 0, 1: 0, 2: 0}
    for _, y in dataloader:
        occurs[int(y)] += 1

    MAX_REL_ERR = 1e-1
    max_occ, min_occ = float("-inf"), float("inf")
    for occ in occurs.values():
        max_occ = max(max_occ, occ)
        min_occ = min(min_occ, occ)

    assert ((max_occ - min_occ) / 1000) < MAX_REL_ERR


@pytest.mark.TimeSeriesData
def test_edge_case_data_split():
    """
    test if dataloaders behave correctly
    with edge case data split situations
    """

    data = pd.DataFrame(
        {"zeros": np.zeros(100), "ones": np.zeros(100) + 1, "id": np.zeros(100)}
    )

    splits = [
        (100, 0, 0),
        (0, 100, 0),
        (0, 0, 100),
        (50, 50, 0),
        (0, 50, 50),
        (50, 0, 50),
    ]

    for split in splits:
        tsdata = TimeSeriesData(
            data=data,
            group_id="id",
            input_continuous=["zeros"],
            input_categorical=[],
            target_continuous=["ones"],
            target_categorical=[],
            prediction_horizon=0,
            input_sequence_len=1,
            input_predictions=[],
            train_val_test_percentage=Train_val_test_perc(*split),
        )

        datasets = [
            tsdata.generate_train_dataset(),
            tsdata.generate_validation_dataset(),
            tsdata.generate_test_dataset(),
        ]

        for i, dataset in enumerate(datasets):
            # sampler should only be passed to train dataloader
            loader = DataLoader(
                dataset,
                batch_size=21,
                sampler=tsdata.generate_uniform_target_sampler() if i == 0 else None,
            )

            for x, y in loader:
                assert (x + 1 == y).all()


@pytest.mark.TimeSeriesData
def test_deterministic_generated_datasets():
    """
    tests if two datasets generated from the same configs
    without shuffling or sampling are returning same values
    """
    SIZE = 100
    data = pd.DataFrame(
        {
            "IN1": np.random.rand(SIZE),
            "IN2": np.random.rand(SIZE),
            "IN3": np.arange(SIZE) // 11,
            "OUT1": np.random.rand(SIZE),
            "OUT2": np.random.rand(SIZE),
            "OUT3": np.arange(SIZE) // 17,
            "ID": np.arange(SIZE) // 20,
        }
    )

    splits = [(30, 40, 30), (20, 60, 20), (0, 50, 50), (20, 80, 0)]

    for split in splits:
        tsdata1 = TimeSeriesData(
            data=data,
            group_id="ID",
            input_continuous=["IN1", "IN2"],
            input_categorical=["IN3"],
            target_continuous=["OUT1", "OUT2"],
            target_categorical=["OUT3"],
            input_sequence_len=7,
            input_predictions=["IN1", "IN3"],
            prediction_horizon=3,
            train_val_test_percentage=Train_val_test_perc(*split),
            shuffle_groups=False,
        )

        tsdata2 = TimeSeriesData(
            data=data,
            group_id="ID",
            input_continuous=["IN1", "IN2"],
            input_categorical=["IN3"],
            target_continuous=["OUT1", "OUT2"],
            target_categorical=["OUT3"],
            input_sequence_len=7,
            input_predictions=["IN1", "IN3"],
            prediction_horizon=3,
            train_val_test_percentage=Train_val_test_perc(*split),
            shuffle_groups=False,
        )

        train1 = tsdata1.generate_train_dataset()
        val1 = tsdata1.generate_validation_dataset()
        test1 = tsdata1.generate_test_dataset()

        train2 = tsdata2.generate_train_dataset()
        val2 = tsdata2.generate_validation_dataset()
        test2 = tsdata2.generate_test_dataset()

        for batch1, batch2 in zip(
            chain(train1, val1, test1), chain(train2, val2, test2)
        ):
            x1, y1 = batch1
            x2, y2 = batch2
            assert (x1 == x2).all()
            assert (y1 == y2).all()
