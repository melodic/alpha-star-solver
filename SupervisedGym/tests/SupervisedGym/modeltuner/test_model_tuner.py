from SupervisedGym.modeltuner.modeltuner import TrainedModelStats
import pytest
import numpy as np
import torch.nn as nn
import pandas as pd
from typing import Optional

from SupervisedGym.modeltuner import ModelTuner
from SupervisedGym.utils.data import (
    TimeSeriesDataStaticConfig,
    Train_val_test_perc,
)
from SupervisedGym.models.base import IBaseTunableModel
from SupervisedGym.models import StackedRNNTunable


@pytest.mark.ModelTuner
def test_fast_dev_run():
    """
    tests if fast dev run exits
    without any error
    """

    SIZE = 100
    data = pd.DataFrame(
        {"IN": np.zeros(SIZE), "OUT": np.zeros(SIZE) + 1, "GR_ID": np.zeros(SIZE)}
    )

    static_conf = TimeSeriesDataStaticConfig(
        data=data,
        group_id="GR_ID",
        input_continuous=["IN"],
        input_categorical=[],
        target_continuous=["OUT"],
        target_categorical=[],
        prediction_horizon=0,
        input_predictions=[],
        train_val_test_percentage=Train_val_test_perc(70, 15, 15),
        shuffle_groups=True,
    )

    tuner = ModelTuner(
        val_loss=nn.L1Loss(), tsdata_static_config=static_conf, fast_dev_run=True
    )

    trained_model_stat: Optional[TrainedModelStats] = None

    def set_tms(tms: TrainedModelStats):
        nonlocal trained_model_stat
        trained_model_stat = tms

    tuner.tune(
        timeout=10 ** 3,
        n_trials=1,
        tunable_model_type=StackedRNNTunable,
        model_callback=set_tms,
    )

    assert isinstance(trained_model_stat, TrainedModelStats)
    assert isinstance(trained_model_stat.model, IBaseTunableModel)
    assert isinstance(trained_model_stat.val_loss, float)
