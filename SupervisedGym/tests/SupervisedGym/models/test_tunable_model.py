import numpy as np
import pandas as pd
import torch

from SupervisedGym.models.base import IBaseTunableModel
from SupervisedGym.utils.data import (
    DataAdapter,
    TimeSeriesDataStaticConfig,
    TimeSeriesDataFitableConfig,
    make_time_series_data,
    Train_val_test_perc,
)
from tests.SupervisedGym.models.dummy_factory import DummyModelFactory


def check_model_parsability(model: IBaseTunableModel):
    """
    checks if model changes behaviour after
    parsing and loading
    """

    TRIES = 5
    BATCH_SIZE = 2
    in_len = len(model.data_adapter.input_columns)
    seq_len = model.data_adapter.input_sequence_len

    parsed = model.parse_model()
    model_new = model.load_model(parsed)

    model.eval()
    model_new.eval()

    for _ in range(TRIES):
        t_in = torch.rand(size=(BATCH_SIZE, seq_len, in_len))
        org = model.forward(t_in)
        new = model_new.forward(t_in)
        assert (org == new).all()

    model.train(True)


def test_model_parsability():
    """
    checks if model can parse and load itself and
    operate the same way
    """
    SIZE = 50
    SEQ_LEN = 5
    data = pd.DataFrame(
        {
            "IN1": np.random.rand(SIZE),
            "IN2": np.random.rand(SIZE),
            "IN3": np.random.rand(SIZE),
            "OUT1": np.zeros(SIZE),
            "OUT2": np.zeros(SIZE),
            "GR_ID": np.arange(SIZE) // 20,
        }
    )
    static_conf = TimeSeriesDataStaticConfig(
        data=data,
        group_id="GR_ID",
        input_continuous=["IN1", "IN2"],
        input_categorical=["IN3"],
        target_continuous=["OUT1"],
        target_categorical=["OUT2"],
        prediction_horizon=3,
        input_predictions=["IN2"],
        train_val_test_percentage=Train_val_test_perc(70, 15, 15),
        shuffle_groups=True,
    )
    fitable_conf = TimeSeriesDataFitableConfig(input_sequence_len=SEQ_LEN)
    data = make_time_series_data(static_conf, fitable_conf)
    adapter = DataAdapter.create_from_timeseries_data(data)

    for model in DummyModelFactory.all_dummys(adapter):
        check_model_parsability(model)
