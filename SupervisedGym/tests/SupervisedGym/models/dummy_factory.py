from __future__ import annotations
import torch
from typing import List

from SupervisedGym.utils.data import DataAdapter
from SupervisedGym.models.base import IBaseTunableModel
from SupervisedGym.models import (
    FeedForwardTunable,
    StackedRNNTunable,
    TransformerTunable,
)


class DummyModelFactory:
    @staticmethod
    def get_feed_forward_dummy(data_adapter: DataAdapter) -> FeedForwardTunable:
        return FeedForwardTunable(
            data_adapter=data_adapter,
            hidden_layer_sizes=[10],
            dropouts=[1],
            activation=torch.nn.ReLU6(),
            layer_norm=True,
            loss=torch.nn.L1Loss(),
            optimizer_type=torch.optim.SGD,
            lr=1,
            val_loss=torch.nn.L1Loss(),
            output_activation=None,
        )

    @staticmethod
    def get_stacked_rnn_dummy(data_adapter: DataAdapter) -> StackedRNNTunable:
        return StackedRNNTunable(
            data_adapter=data_adapter,
            cell_type="LSTM",
            activation=torch.nn.ReLU6(),
            num_recurrent_layers=1,
            hidden_size=5,
            bidirectional=False,
            dropouts=[0.8, 0.8],
            layer_norm=True,
            loss=torch.nn.L1Loss(),
            optimizer_type=torch.optim.SGD,
            lr=1,
            val_loss=torch.nn.L1Loss(),
        )

    @staticmethod
    def get_transformer_dummy(data_adapter: DataAdapter) -> TransformerTunable:
        return TransformerTunable(
            data_adapter=data_adapter,
            features=3,
            heads_number=1,
            layers_number=1,
            feed_forward_size=3,
            pre_output=3,
            dropout=0,
            activation=torch.nn.ReLU(),
            loss=torch.nn.L1Loss(),
            lr=1,
            val_loss=torch.nn.L1Loss(),
            optimizer_type=torch.optim.SGD,
            patience=5,
        )

    @staticmethod
    def all_dummys(data_adapter: DataAdapter) -> List[IBaseTunableModel]:
        return [
            DummyModelFactory.get_feed_forward_dummy(data_adapter),
            DummyModelFactory.get_stacked_rnn_dummy(data_adapter),
            DummyModelFactory.get_transformer_dummy(data_adapter),
        ]
