import pandas as pd
import numpy as np
import pytest
from SupervisedGym.models.assembled_model import AssembledModel
from SupervisedGym.utils.data import (
    DataAdapter,
    TimeSeriesDataFitableConfig,
    TimeSeriesDataStaticConfig,
    make_time_series_data,
    Train_val_test_perc,
)
from SupervisedGym.utils.domain_rounder import DomainRounder
from tests.SupervisedGym.models import DummyModelFactory


@pytest.mark.Models
def test_simple_case():
    """tests if assembled model made of
    dummy assembled model and domain rounder
    is working correctly
    """

    SIZE = 50
    SEQ_LEN = 5
    data = pd.DataFrame(
        {
            "IN1": np.random.rand(SIZE),
            "IN2": np.random.rand(SIZE),
            "IN3": np.random.rand(SIZE),
            "OUT1": np.zeros(SIZE),
            "OUT2": np.zeros(SIZE),
            "GR_ID": np.arange(SIZE) // 20,
        }
    )
    static_conf = TimeSeriesDataStaticConfig(
        data=data,
        group_id="GR_ID",
        input_continuous=["IN1", "IN2"],
        input_categorical=["IN3"],
        target_continuous=["OUT1"],
        target_categorical=["OUT2"],
        prediction_horizon=3,
        input_predictions=["IN2"],
        train_val_test_percentage=Train_val_test_perc(70, 15, 15),
        shuffle_groups=True,
    )
    fitable_conf = TimeSeriesDataFitableConfig(input_sequence_len=SEQ_LEN)
    data = make_time_series_data(static_conf, fitable_conf)
    adapter = DataAdapter.create_from_timeseries_data(data)
    rounder = DomainRounder({"OUT1": [1], "OUT2": [2]})
    for model in DummyModelFactory.all_dummys(adapter):
        assembled_model = AssembledModel(models=[(model, 1)], domain_rounder=rounder)

        assert (
            assembled_model.predict(
                {
                    "IN1": np.random.rand(SEQ_LEN).tolist(),
                    "IN2": np.random.rand(SEQ_LEN).tolist(),
                    "IN3": np.random.rand(SEQ_LEN).tolist(),
                }
            )
            == {"OUT1": 1, "OUT2": 2}
        )


def test_handles_invalid_input():
    """tests if AssembledModel raises
    correct Exception when given
    invalid input data for prediction
    """
    SIZE = 20
    SEQ_LEN = 3
    data = pd.DataFrame(
        {
            "IN1": np.random.rand(SIZE),
            "IN2": np.random.rand(SIZE),
            "IN3": np.random.rand(SIZE),
            "OUT1": np.zeros(SIZE),
            "OUT2": np.zeros(SIZE),
            "GR_ID": np.arange(SIZE) // 10,
        }
    )
    static_conf = TimeSeriesDataStaticConfig(
        data=data,
        group_id="GR_ID",
        input_continuous=["IN1", "IN2"],
        input_categorical=["IN3"],
        target_continuous=["OUT1"],
        target_categorical=["OUT2"],
        prediction_horizon=3,
        input_predictions=["IN2"],
        train_val_test_percentage=Train_val_test_perc(70, 30, 0),
        shuffle_groups=True,
    )
    fitable_conf = TimeSeriesDataFitableConfig(input_sequence_len=SEQ_LEN)
    data = make_time_series_data(static_conf, fitable_conf)
    adapter = DataAdapter.create_from_timeseries_data(data)
    model = AssembledModel(
        models=[(DummyModelFactory.get_feed_forward_dummy(adapter), 1)],
        domain_rounder=DomainRounder({"OUT1": [69], "OUT2": [420]}),
    )

    # empty dict
    with pytest.raises(ValueError):
        model.predict({})

    # not enough variables
    with pytest.raises(ValueError):
        model.predict(
            {
                "IN1": np.random.rand(SEQ_LEN).tolist(),
                "IN3": np.random.rand(SEQ_LEN).tolist(),
            }
        )

    # wrong sequence length
    with pytest.raises(ValueError):
        model.predict(
            {
                "IN1": np.random.rand(SEQ_LEN).tolist(),
                "IN2": np.random.rand(SEQ_LEN + 1).tolist(),
                "IN3": np.random.rand(SEQ_LEN).tolist(),
            }
        )

    # too many variables but all needed are there
    assert (
        model.predict(
            {
                "IN1": np.random.rand(SEQ_LEN).tolist(),
                "IN2": np.random.rand(SEQ_LEN).tolist(),
                "IN3": np.random.rand(SEQ_LEN).tolist(),
                "GR_ID": np.random.rand(SEQ_LEN).tolist(),
            }
        )
        == {"OUT1": 69, "OUT2": 420}
    )


def test_load_parse():
    """
    tests if AssembledModel can parse and load
    itself and operate the same way
    """
    TRIES = 5
    SIZE = 50
    SEQ_LEN = 5
    data = pd.DataFrame(
        {
            "IN1": np.random.rand(SIZE),
            "IN2": np.random.rand(SIZE),
            "IN3": np.random.rand(SIZE),
            "OUT1": np.zeros(SIZE),
            "OUT2": np.zeros(SIZE),
            "GR_ID": np.arange(SIZE) // 20,
        }
    )
    static_conf = TimeSeriesDataStaticConfig(
        data=data,
        group_id="GR_ID",
        input_continuous=["IN1", "IN2"],
        input_categorical=["IN3"],
        target_continuous=["OUT1"],
        target_categorical=["OUT2"],
        prediction_horizon=3,
        input_predictions=["IN2"],
        train_val_test_percentage=Train_val_test_perc(70, 15, 15),
        shuffle_groups=True,
    )
    fitable_conf = TimeSeriesDataFitableConfig(input_sequence_len=SEQ_LEN)
    data = make_time_series_data(static_conf, fitable_conf)
    adapter = DataAdapter.create_from_timeseries_data(data)
    rounder = DomainRounder(
        {
            "OUT1": [1e-1 * i for i in range(100)],
            "OUT2": [1e-1 * i for i in range(100)],
        }
    )
    for model in DummyModelFactory.all_dummys(adapter):
        assembled = AssembledModel(models=[(model, 1)], domain_rounder=rounder)
        loaded_model = AssembledModel.load_model(assembled.parse_model())
        for _ in range(TRIES):
            pred_in = {
                "IN1": np.random.randn(SEQ_LEN).tolist(),
                "IN2": np.random.randn(SEQ_LEN).tolist(),
                "IN3": np.random.randn(SEQ_LEN).tolist(),
            }
            assert assembled.predict(pred_in) == loaded_model.predict(pred_in)


def test_multi_forward():
    """
    tests if assemby model made of
    same models makes same prediction as
    only single model
    """

    TRIES = 5
    SIZE = 50
    SEQ_LEN = 7
    data = pd.DataFrame(
        {
            "IN1": np.random.rand(SIZE),
            "IN2": np.random.rand(SIZE),
            "IN3": np.random.rand(SIZE),
            "OUT1": np.zeros(SIZE),
            "OUT2": np.zeros(SIZE),
            "GR_ID": np.arange(SIZE) // 20,
        }
    )
    static_conf = TimeSeriesDataStaticConfig(
        data=data,
        group_id="GR_ID",
        input_continuous=["IN1", "IN2"],
        input_categorical=["IN3"],
        target_continuous=["OUT1"],
        target_categorical=["OUT2"],
        prediction_horizon=3,
        input_predictions=["IN2"],
        train_val_test_percentage=Train_val_test_perc(70, 15, 15),
        shuffle_groups=True,
    )
    fitable_conf = TimeSeriesDataFitableConfig(input_sequence_len=SEQ_LEN)
    data = make_time_series_data(static_conf, fitable_conf)
    adapter = DataAdapter.create_from_timeseries_data(data)
    rounder = DomainRounder(
        {
            "OUT1": [1e-1 * i for i in range(100)],
            "OUT2": [1e-1 * i for i in range(100)],
        }
    )
    singe_model = DummyModelFactory.get_feed_forward_dummy(adapter)
    multi_assemby = AssembledModel(
        models=[(model, 1) for model in [singe_model] * 5], domain_rounder=rounder
    )
    single_assembly = AssembledModel(models=[(singe_model, 1)], domain_rounder=rounder)

    for _ in range(TRIES):
        pred_in = {
            "IN1": np.random.randn(SEQ_LEN).tolist(),
            "IN2": np.random.randn(SEQ_LEN).tolist(),
            "IN3": np.random.randn(SEQ_LEN).tolist(),
        }
        assert multi_assemby.predict(pred_in) == single_assembly.predict(pred_in)


def test_ensemble_weights():
    """
    tests if assembly model behaves
    correctly with diffrent weights
    """
    SIZE = 50
    SEQ_LEN = 7
    data = pd.DataFrame(
        {
            "IN1": np.random.rand(SIZE),
            "IN2": np.random.rand(SIZE),
            "IN3": np.random.rand(SIZE),
            "OUT1": np.zeros(SIZE),
            "OUT2": np.zeros(SIZE),
            "GR_ID": np.arange(SIZE) // 20,
        }
    )
    static_conf = TimeSeriesDataStaticConfig(
        data=data,
        group_id="GR_ID",
        input_continuous=["IN1", "IN2"],
        input_categorical=["IN3"],
        target_continuous=["OUT1"],
        target_categorical=["OUT2"],
        prediction_horizon=3,
        input_predictions=["IN2"],
        train_val_test_percentage=Train_val_test_perc(70, 15, 15),
        shuffle_groups=True,
    )
    fitable_conf = TimeSeriesDataFitableConfig(input_sequence_len=SEQ_LEN)
    data = make_time_series_data(static_conf, fitable_conf)
    adapter = DataAdapter.create_from_timeseries_data(data)
    rounder = DomainRounder(
        {
            "OUT1": [1e-1 * i for i in range(100)],
            "OUT2": [1e-1 * i for i in range(100)],
        }
    )

    ffmodel = DummyModelFactory.get_feed_forward_dummy(adapter)
    rrnmodel = DummyModelFactory.get_stacked_rnn_dummy(adapter)
    pred_in = {
        "IN1": np.random.randn(SEQ_LEN).tolist(),
        "IN2": np.random.randn(SEQ_LEN).tolist(),
        "IN3": np.random.randn(SEQ_LEN).tolist(),
    }

    assert AssembledModel(
        models=[(ffmodel, 3), (rrnmodel, 0)], domain_rounder=rounder
    ).predict(pred_in) == AssembledModel(
        models=[(ffmodel, 1)], domain_rounder=rounder
    ).predict(
        pred_in
    )

    assert AssembledModel(
        models=[(ffmodel, 0), (rrnmodel, 5)], domain_rounder=rounder
    ).predict(pred_in) == AssembledModel(
        models=[(rrnmodel, 0.1)], domain_rounder=rounder
    ).predict(
        pred_in
    )
