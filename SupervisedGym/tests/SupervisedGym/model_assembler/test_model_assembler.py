import pytest
import torch
from SupervisedGym.utils.data import Train_val_test_perc
import pandas as pd
import numpy as np
import xml.etree.ElementTree as ET
from SupervisedGym.model_assembler import ModelAssembler
from SupervisedGym.utils import DataConf, ModelResources
from SupervisedGym.models import avaible_model_types


conf = DataConf(
    group_id="group_id",
    pred_horizon=3,
    data_split=Train_val_test_perc(80, 20, 0),
    shuffle=True,
)

cp_xml = r"""<?xml version="1.0" encoding="ASCII"?>
<cp:ConstraintProblem xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:cp="http://www.paasage.eu/eu/paasage/upperware/metamodel/cp" xmlns:types="http://www.paasage.eu/eu/paasage/upperware/metamodel/types" id="FCRclear1579527639568">
  <cpVariables id="AppStorage" variableType="storage" componentId="Component_App">
    <domain xsi:type="cp:NumericListDomain">
      <values xsi:type="types:IntegerValueUpperware"/>
      <values xsi:type="types:IntegerValueUpperware" value="10"/>
      <values xsi:type="types:IntegerValueUpperware" value="420"/>
      <values xsi:type="types:IntegerValueUpperware" value="1024"/>
      <values xsi:type="types:IntegerValueUpperware" value="2097152"/>
    </domain>
  </cpVariables>
</cp:ConstraintProblem>
"""


@pytest.mark.slow
def test_dev_run():
    """
    tests if ModelAssebler can run successfully
    some small fast dev run and produce assembly
    model that also can perform predicting with
    data from FCR app
    """
    SIZE = 100
    data = pd.DataFrame(
        {
            "group_id": (np.zeros(SIZE)).tolist(),
            "AppCardinality": np.arccos(SIZE).tolist(),
            "AppCores": np.arctan(SIZE).tolist(),
            "AppStorage": np.arctan(SIZE).tolist(),
        }
    )

    assebler = ModelAssembler(
        raw_data=data,
        cp_problem=ET.fromstring(cp_xml),
        val_loss=torch.nn.L1Loss(),
        data_conf=conf,
    )

    assebler.assemble_model(
        models={
            model: ModelResources(timeout=5, n_trials=1)
            for model in avaible_model_types
        },
        ensemble_size=1,
        loss_to_weight=lambda v, a: 1,
        fast_dev_run=True,
    )
