"""
Important:
Before running set CLASSPATH to the location of utility-generator-jar-with-dependencies.jar
and set PAASAGE_CONFIG_DIR to the location of the cdo config files (with location of the cdo server)
"""
import json
from pathlib import Path

import requests
from cdoAdapter import CDOAdapter


def run_generator():
    cpproblem = Path("FCRclear-CP.xmi")

    CDOAdapter.importResourceFromFile(cpproblem, "demofcr")

    params = {
        "applicationId": "demo",
        "trainRatio": 0.9,
        "cdoProblemPath": "demofcr",
        "returnEndpoint": "http://listener:8081/",
        # "startTimestamp": "2021-06-17T12:21:39.643Z",
        # "finishTimestamp": "2021-06-17T12:21:39.643Z",
        "delta": 30,
        "metricsInfluxMapping": {
            "real": "AvgResponseTime"
        },
        "configurationInfluxMapping": {}
    }

    print("Sending request!")
    response = requests.post('http://127.0.0.1/generate', data=json.dumps(params))
    print(f"Got response: {response}, content: {response.content}")


if __name__ == "__main__":
    run_generator()
