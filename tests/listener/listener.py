"""
Important:
Before running set CLASSPATH to the location of utility-generator-jar-with-dependencies.jar
and set PAASAGE_CONFIG_DIR to the location of the cdo config files (with location of the cdo server)
"""

import uvicorn
from fastapi import FastAPI

from DataGeneratorService.models import DataGenerationResult

app = FastAPI()


@app.post("/")
async def read_items(result: DataGenerationResult):
    print(result)


if __name__ == "__main__":
    uvicorn.run(app, host='0.0.0.0', port=8081)
