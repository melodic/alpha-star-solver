import datetime as dt
import json
import logging
import os

import stomp
from DataReading.csv_reader import CSVReader
from stomp.exception import ConnectFailedException

from .config import ACTIVEMQ_HOSTNAME, ACTIVEMQ_PORT, ACTIVEMQ_USERNAME, ACTIVEMQ_PASSWORD, \
    ACTIVEMQ_TOPIC, APPLICATION_NAME, CSV_INPUT_PATH

destination = ACTIVEMQ_TOPIC

logger = logging.getLogger(__name__)


def csv_publisher() -> None:
    """Upload content of csv file specified in environmental variable to the InxluxDB."""

    logger.info(f'Starting publishing from directory {CSV_INPUT_PATH}')
    conn = stomp.Connection(host_and_ports=[(ACTIVEMQ_HOSTNAME, ACTIVEMQ_PORT)])
    while True:
        try:
            conn.connect(login=ACTIVEMQ_USERNAME, passcode=ACTIVEMQ_PASSWORD)
            break
        except ConnectFailedException as exc:
            logger.error(f'Unable to connect to ActiveMQ under address {ACTIVEMQ_HOSTNAME}:{ACTIVEMQ_PORT}')
            logger.debug(exc)

    logger.debug(f'Directory content: {os.listdir(CSV_INPUT_PATH)}')
    for file in os.listdir(CSV_INPUT_PATH):
        if not file.endswith('.csv'):
            logger.debug(f'Skipping file {file}')
            continue
        try:
            logger.info(f'Publishing from file {file}')
            reader = CSVReader(os.path.join(CSV_INPUT_PATH, file))
            for row in reader.row_generator_annotated():
                logger.debug(f'Publishing row: {row}')
                timestamp: dt.datetime = row.pop(reader.timestamp_column_name)
                data = {
                    'timestamp': timestamp.timestamp(),
                    "labels":
                        {"hostname": "localhost", "application": APPLICATION_NAME},
                    "metrics": row
                }
                logger.debug(f'Sending {data}')
                conn.send(body=json.dumps(data), destination=destination, persistent='false')

            logger.info(f'Finished publishing from csv file {CSV_INPUT_PATH}')
        finally:
            pass
