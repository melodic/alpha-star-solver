import logging

from .config import RANDOM_FCR
from .csv_publisher import csv_publisher
from .fcr_random_publisher import fcr_publisher

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

if __name__ == '__main__':
    logger.debug(f'RANDOM_FCR is {RANDOM_FCR}')
    if RANDOM_FCR:
        fcr_publisher()
    else:
        csv_publisher()
