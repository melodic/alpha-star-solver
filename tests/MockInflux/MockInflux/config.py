import os

APPLICATION_NAME = os.environ.get("APPLICATION_NAME", "demofcr")

ACTIVEMQ_HOSTNAME = os.environ.get("ACTIVEMQ_HOSTNAME", "activemq")
ACTIVEMQ_PORT = int(os.environ.get("ACTIVEMQ_PORT", "61613"))
ACTIVEMQ_USERNAME = os.environ.get("ACTIVEMQ_USERNAME", "aaa")
ACTIVEMQ_PASSWORD = os.environ.get("ACTIVEMQ_PASSWORD", "111")
ACTIVEMQ_TOPIC = os.environ.get("ACTIVEMQ_TOPIC", "/queue/static-topic-1")

CSV_INPUT_PATH = os.environ.get("CSV_INPUT_PATH")

RANDOM_FCR = os.environ.get("RANDOM_FCR", "False") == "True"
