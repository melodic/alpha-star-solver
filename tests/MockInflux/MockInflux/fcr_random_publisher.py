import json
import random
import time

import stomp

from .config import ACTIVEMQ_HOSTNAME, ACTIVEMQ_PORT, ACTIVEMQ_USERNAME, ACTIVEMQ_PASSWORD, \
    ACTIVEMQ_TOPIC, APPLICATION_NAME

METRICS = ['response_time', 'cpu_usage', 'memory']


def _generateMeasurement(name):
    if name == "response_time":
        return random.randint(20, 100)
    elif name == 'cpu_usage':
        return random.randint(1, 100)
    else:
        return random.randint(50, 250)


def fcr_publisher():

    conn = stomp.Connection(host_and_ports=[(ACTIVEMQ_HOSTNAME, ACTIVEMQ_PORT)])
    conn.connect(login=ACTIVEMQ_USERNAME, passcode=ACTIVEMQ_PASSWORD)
    try:
        while True:
            data = {'timestamp': time.time(),
                    'labels': {'hostname': 'localhost', 'application': APPLICATION_NAME, 'METRICS': []}}
            for metric in METRICS:
                measurement = _generateMeasurement(metric)
                data['labels']['METRICS'].append({metric: measurement})
            conn.send(body=json.dumps(data), destination=ACTIVEMQ_TOPIC, persistent='false')
            print(data)
            time.sleep(3)
    finally:
        conn.disconnect()
        conn.send(body="SHUTDOWN", destination=ACTIVEMQ_TOPIC, persistent='false')
