# MockInflux 

MockInflux is easy to use package providing methods to fill InfluxDB
database with synthetic data. It is possible to supply random data (for FCR only) or already
generated data.

## Setting
By default, MockInflux operates in csv import mode. 
It uploads all the csv files specified via ```CSV_INPUT_PATH``` environment variable.
In order to use it to upload randomly generated data, set ```RANDOM_FCR``` environment variable to ```True```.

Following environmental variables must be set to communicate with InfluxDB

- ```ACTIVEMQ_HOSTNAME```
- ```ACTIVEMQ_PORT```
- ```ACTIVEMQ_USERNAME```
- ```ACTIVEMQ_PASSWORD```
- ```ACTIVEMQ_TOPIC```
  
Set ```APPLICATION_NAME``` to specify app name. By default, metrics are uploaded under app key ```demo```.
