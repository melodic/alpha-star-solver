# Model Database
This is a Mongo Database for data accessible for many different components of AS Solver.


#Running Model Database
To start the database server use command ``docker-compose up`` in the directory with docker-compose.yml.


#Connecting to the database
To facilitate connecting with the database use the class ModelDBClient, which can be found at ModelDB/src/ModelDBClient.py
