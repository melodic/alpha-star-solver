import pymongo
import gridfs
import os
from typing import Dict, Any
from pathlib import Path

DEFAULT_MODELDB_HOST: str = "localhost"
DEFAULT_MODELDB_PORT: int = 27018
MAIN_DATABASE_NAME = "model-database"
TRAINING_DATA_DATABASE_NAME = "training-data-database"
TEST_DATA_DATABASE_NAME = "test-data-database"
TRAINED_MODELS_DATABASE_NAME = "trained-models-database"


class ModelDBClient:
    """Client for importing and exporting data from the Model DB.

    Uses environmental variables:
        MODELDB_HOST: host on which Model DB server listens.
        MODELDB_PORT: port on which Model DB server listens.
    """

    _db: pymongo.database.Database  # database object for storing BSON data of normal size (under 16 MB)
    _fs_training_data: gridfs.GridFS  # gridFS object for storing training data (huge BSON data)
    _fs_test_data: gridfs.GridFS  # gridFS object for storing test data (huge BSON data)
    _fs_trained_model: gridfs.GridFS  # gridFS object for storing trained models (huge BSON data)

    def __init__(self):

        host = os.getenv('MODELDB_HOST', DEFAULT_MODELDB_HOST)
        port = int(os.getenv('MODELDB_PORT', DEFAULT_MODELDB_PORT))

        client = pymongo.MongoClient(host, port)
        self._db = client[MAIN_DATABASE_NAME]
        self._fs_training_data = gridfs.GridFS(client[TRAINING_DATA_DATABASE_NAME])
        self._fs_test_data = gridfs.GridFS(client[TEST_DATA_DATABASE_NAME])
        self._fs_trained_model = gridfs.GridFS(client[TRAINED_MODELS_DATABASE_NAME])

    @staticmethod
    def _importToGridFSFromFile(grid_db: gridfs.GridFS,
                                application_id: str,
                                file_path: Path) -> None:
        """Imports data from the file system to gridFS database under the key of application id.

            grid_db: gridFS database to which the data will be imported
            application_id: unique id of the application
            file_path: file path from which the data will be imported
        """
        document = grid_db.find_one({"_id": application_id})

        with file_path.open(mode="rb") as file:
            if document is not None:
                grid_db.delete(application_id)
            grid_db.put(file, _id=application_id)

    @staticmethod
    def _exportFromGridFSToFile(grid_db: gridfs.GridFS,
                                application_id: str,
                                file_path: Path) -> None:
        """Exports data from gridFS database under the key of application id to the file system.

        Args:
            grid_db: gridFS database from which the data should be exported
            application_id: unique id of the application
            file_path: file path to which the data will be exported

        Raises:
            KeyError: when it could not find data in the database for the given application id
        """
        document = grid_db.find_one({"_id": application_id})
        if document is None:
            raise KeyError(application_id)

        with file_path.open(mode="wb") as file:
            file.write(document.read())

    def saveDocumentToModelDB(self,
                              collection_name: str,
                              application_id: str,
                              document: Dict[str, Any]) -> None:
        """Saves a document to Model Database based on a given collection name and application id.

        Args:
            collection_name: name of the collection to which a document should be saved
            application_id: unique id of the application
            document: document to be saved into the database
        """
        document["_id"] = application_id
        self._db[collection_name].replace_one({"_id": application_id}, document, upsert=True)

    def loadDocumentFromModelDB(self,
                                collection_name: str,
                                application_id: str) -> Dict[str, Any]:
        """Loads a document from Model Database, based on a given collection name and application id.

        Args:
            collection_name: name of the collection from which to load a document
            application_id: unique id of the application

        Returns:
            A document loaded from the Model Database.
            Note that the document will have an additional item ("_id": application_id)

        Raises:
            KeyError: when it could not find data in the database for the given application id or collection name
        """
        document = self._db[collection_name].find_one({"_id": application_id})
        if document is None:
            raise KeyError(application_id)
        return document

    def importCP(self,
                 application_id: str,
                 CP_file_path: Path) -> None:
        """This method is used to import Constraint Problem from the file system to the database and save it under the application id key.

        Args:
            application_id: unique id of the application
            CP_file_path: path to file containing the Constraint Problem

        Raises:
            FileNotFoundError: when the given Path does not lead to an actual file
        """
        with CP_file_path.open(mode="rb") as cp_file:
            self.saveDocumentToModelDB("constraint_problem", application_id, {"constraint_problem": cp_file.read()})

    def exportCP(self,
                 application_id: str,
                 CP_file_path: Path) -> None:
        """This method is used to export Constraint Problem with the given application id from the database to the file system.

        Args:
            application_id: unique id of the application
            CP_file_path: file path to which the Constraint Problem will be exported

        Raises:
            KeyError: when it could not find data in the database for the given application id
        """
        document = self.loadDocumentFromModelDB("constraint_problem", application_id)
        with CP_file_path.open(mode="wb") as cp_file:
            cp_file.write(document["constraint_problem"])

    def importCamelModel(self,
                         application_id: str,
                         camel_model_file_path: Path) -> None:
        """This method is used to import Camel Model from the file system to the database and save it under the application id key.

        Args:
            application_id: unique id of the application
            camel_model_file_path: path to file containing the Camel Model

        Raises:
            FileNotFoundError: when the given Path does not lead to an actual file
        """
        with camel_model_file_path.open(mode="rb") as camel_model_file:
            self.saveDocumentToModelDB("camel_model", application_id, {"camel_model": camel_model_file.read()})

    def exportCamelModel(self,
                         application_id: str,
                         camel_model_file_path: Path) -> None:
        """This method is used to export Camel Model with the given application id from the database to the file system.

        Args:
            application_id: unique id of the application
            camel_model_file_path: file path to which the Camel Model will be exported

        Raises:
            KeyError: when it could not find data in the database for the given application id
        """
        document = self.loadDocumentFromModelDB("camel_model", application_id)
        with camel_model_file_path.open(mode="wb") as camel_model_file:
            camel_model_file.write(document["camel_model"])

    def importNodeCandidates(self,
                             application_id: str,
                             node_candidates_file_path: Path) -> None:
        """This method is used to import Node Candidates from the file system to the database and save it under the application id key.

        Args:
            application_id: unique id of the application
            node_candidates_file_path: path to file containing the Node Candidates

        Raises:
            FileNotFoundError: when the given Path does not lead to an actual file
        """
        with node_candidates_file_path.open(mode="rb") as node_candidates_file:
            self.saveDocumentToModelDB("node_candidates", application_id,
                                       {"node_candidates": node_candidates_file.read()})

    def exportNodeCandidates(self,
                             application_id: str,
                             node_candidates_file_path: Path) -> None:
        """This method is used to export Node Candidates with the given application id from the database to the file system.

        Args:
            application_id: unique id of the application
            node_candidates_file_path: file path to which the Node Candidates will be exported

        Raises:
            KeyError: when it could not find data in the database for the given application id
        """
        document = self.loadDocumentFromModelDB("node_candidates", application_id)
        with node_candidates_file_path.open(mode="wb") as node_candidates_file:
            node_candidates_file.write(document["node_candidates"])

    def importTrainingDataFromFile(self,
                                   application_id: str,
                                   training_data_file_path: Path) -> None:
        """This method is used to import training data from the file system to
        the database and save it under the application id key.

        Args:
            application_id: unique id of the application
            training_data_file_path: path to file containing the training data
            
        Raises:
            FileNotFoundError: when the given Path does not lead to an actual file
        """
        ModelDBClient._importToGridFSFromFile(self._fs_training_data, application_id, training_data_file_path)

    def exportTrainingDataToFile(self,
                                 application_id: str,
                                 training_data_file_path: Path) -> None:
        """This method is used to export training data with the given application id
        from the database to the file system.

        Args:
            application_id: unique id of the application
            training_data_file_path: file path to which the training data will be exported
            
        Raises:
            KeyError: when it could not find data in the database for the given application id
        """
        ModelDBClient._exportFromGridFSToFile(self._fs_training_data, application_id, training_data_file_path)

    def importTestDataFromFile(self,
                               application_id: str,
                               test_data_file_path: Path) -> None:
        """This method is used to import test data from the file system to
        the database and save it under the application id key.

        Args:
            application_id: unique id of the application
            test_data_file_path: path to file containing the training data

        Raises:
            FileNotFoundError: when the given Path does not lead to an actual file
        """
        ModelDBClient._importToGridFSFromFile(self._fs_test_data, application_id, test_data_file_path)

    def exportTestDataToFile(self,
                             application_id: str,
                             test_data_file_path: Path) -> None:
        """This method is used to export test data with the given application id
        from the database to the file system.

        Args:
            application_id: unique id of the application
            test_data_file_path: file path to which the training data will be exported

        Raises:
            KeyError: when it could not find data in the database for the given application id
        """
        ModelDBClient._exportFromGridFSToFile(self._fs_test_data, application_id, test_data_file_path)

    def importTrainedModelFromFile(self,
                                   application_id: str,
                                   trained_model_file_path: Path) -> None:
        """This method is used to import a trained model from the file system to
        the database and save it under the application id key.

        Args:
            application_id: unique id of the application
            trained_model_file_path: path to file containing the trained model
            
        Raises:
            FileNotFoundError: when the given Path does not lead to an actual file
        """
        ModelDBClient._importToGridFSFromFile(self._fs_trained_model, application_id, trained_model_file_path)

    def exportTrainedModelToFile(self,
                                 application_id: str,
                                 trained_model_file_path: Path) -> None:
        """This method is used to export a trained model with the given application id
        from the database to the file system.

        Args:
            application_id: unique id of the application
            trained_model_file_path: file path to which the trained model will be exported
            
        Raises:
            KeyError: when it could not find data in the database for the given application id
        """
        ModelDBClient._exportFromGridFSToFile(self._fs_trained_model, application_id, trained_model_file_path)
