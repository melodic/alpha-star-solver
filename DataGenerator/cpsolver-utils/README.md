How to run data generator with cpsolver in docker?

1. Prepare config folder. You can get it from [here](https://bitbucket.7bulls.eu/projects/MEL/repos/utils/browse/melodic_properties/templates/config). 
Instructions how to modify the config you can find right here: [instructions](https://confluence.7bulls.eu/display/MEL/Build+and+run+a+melodic+component+on+a+local+machine)
   
2. Run command ``docker-compose up`` in cpsolver-utils directory.