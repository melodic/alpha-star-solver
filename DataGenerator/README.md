# DataGenerator tools package

Supplies both a service to schedule training data generation, as a manual tool to generate on premise. 
Retrieves data from CDO about problem structure and 
past performance data about machine deployments from InfluxDB.

## Problems

- Does not work with cpsolver (because it doesnt accept incoming requests in testing docker environment)
- Influx MockProvider does not fill the database correctly (unknown reason, see tests/ for more)

## Contents
- DataGenerator - solution cache & cpsolver summoners
- DataReading - reading data from files/influx, interpolation and aggregation
- DataGeneratorScript - used to manually schedule data generation (UNSTABLE)
- DataGeneratorService - main service for automatic generation

## Contents
- DataGenerator
- DataReadering
- DataGeneratorScript
- DataGeneratorService

### Installation

DataGeneratorService requires connection to CDO. 
To accomplish that, it uses cdoAdapter, which requires jar library.  
See `/controller` for more information.

In local usage run service, type 
```uvicorn DataGeneratorService.fastapi_app:app```
to run app on port 80.

### Configuration

Data generator needs following information to work correctly.
Set following env variables before running:

- HOSTNAME
- CPSOLVER_URL
- CPSOLVER_PORT
- CPSOLVER_TIMEOUT
- MODELDB_HOST
- MODELDB_PORT

##### InfluxDB configuration

- INFLUXDB_HOSTNAME
- INFLUXDB_PORT
- INFLUXDB_USERNAME
- INFLUXDB_PASSWORD
- INFLUXDB_DBNAME
- INFLUXDB_ORG

##### Default delta (time between probes)

- DEFAULT_DELTA

##### cdoAdapter configuration

- PAASAGE_CONFIG_DIR
- esb.url

##### CPSolver watermarks

- WATERMARK_USER
- WATERMARK_SYSTEM

##### Testing and development
- MOCK_INFLUX_CSV_PATH (set it to the csv file location, to load from file instead from influx)