import asyncio
import copy
import json

import httpx
from lxml import etree

from DataGenerator.solution_cache import CPQuery, CPSolution
from .solv_summoner import CPSolverSolutionSummoner, CPSolverSolutionSummonerError


class LocalCPSolverSolutionSummoner(CPSolverSolutionSummoner):
    """
    Responsible for connecting with local cpsolver,
    and generating solved constraint problem solutions,
    based on provided parameters.

    Args:
        problem_name (str): problem name, used for cache purposes.
        cpsolver_config_path (str): path to the cpsolver config path.
        args_names (str): names of the arguments to pass to the cpsolver.
        configuration (Dict): current working configuration of the problem.
        mongo_url (str): url to the mongodb.
    """
    _solver_host: str
    _lock: asyncio.Lock

    def __init__(self,
                 *args,
                 **kwargs):
        super().__init__(*args, **kwargs)
        self._lock = asyncio.Lock()

    async def _ask_solver(self, query: CPQuery) -> CPSolution:
        """ returns solution as list of int """
        async with self._lock:
            request = copy.deepcopy(self._request_template)
            tempfile = self._create_problem(query)

            request['cpProblemFilePath'] = tempfile.name

            solutionFilePath = request['cpProblemFilePath'].parent.joinpath(
                f"{request['cpProblemFilePath'].name[:-4]}-solution.xmi")

            async with httpx.AsyncClient() as client:
                response = await client.post(f'http://{self._solver_host}/constraintProblemSolutionFromFile',
                                             data=json.dumps(request),
                                             headers={'Content-Type': 'application/json'}
                                             )

            if response.status_code != 200:
                raise CPSolverSolutionSummonerError("cp-solver returned non 200 status code")

            tempfile.close()
            sol_xelem = etree.parse(solutionFilePath).find("solution")
            if sol_xelem is None:
                raise CPSolverSolutionSummonerError(f"cp-solver didn't return solution")

            solutionFilePath.unlink()

            return self._solution_builder.buildSolution(solution_filepath=solutionFilePath)
