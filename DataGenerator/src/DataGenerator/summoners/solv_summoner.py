import abc
import itertools
import logging
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import List, Optional, Dict
from xml.etree.ElementTree import ElementTree, Element

from lxml import etree

from DataGenerator.models import CPQuery, CPSolution
from DataGenerator.solution_cache import SolutionCache

MAX_RETRIES = 3

logger = logging.getLogger(__name__)


class CPSolverSolutionSummonerError(Exception):
    """Error related with cpsolver."""
    pass


class CPSolutionBuilder:
    _cpvariables: List[str]

    def __init__(self, cpproblem_path: Path):
        problem_xml = etree.parse(str(cpproblem_path))
        self._cpvariables = []
        for cpvar in problem_xml.findall('cpVariables'):
            self._cpvariables.append(cpvar.attrib['id'])

    def buildSolution(self,
                      applicationId: str,
                      cpproblem_solution_path: Path,
                      configuration: Optional[Dict] = None) -> CPSolution:
        if configuration is None:
            configuration = {}
        solution_xml = etree.parse(str(cpproblem_solution_path)).find("solution")
        solution_str = {}

        for variable_name, variable_value in zip(self._cpvariables, solution_xml.iterfind('variableValue')):
            solution_str[variable_name] = variable_value.find('value').attrib['value']

        return CPSolution(applicationId=applicationId,
                          variables=solution_str,
                          configuration=configuration)


class CPSolverSolutionSummoner(abc.ABC):
    """
    Responsible for connecting with cpsolver,
    and generating solved constraint problem solutions,
    based on provided parameters.

    Args:
        problem_name (str): problem name, used for cache purposes.
        cpsolver_config_path (str): path to the cpsolver config path.
        args_names (str): names of the arguments to pass to the cpsolver.
        configuration (Dict): current working configuration of the problem.
        mongo_url (str): url to the mongodb.
    """
    _config: Dict
    _problem_name: str
    _camelModelFilePath: Path
    _FCRcpxml_path: Path
    _FCRcpxml: ElementTree
    _nodeCandidatesFilePath: Path

    _args_names: List[str]
    _xml_fillings: Dict[str, Element]
    _request_template: Dict
    _cache: SolutionCache
    _solution_params: List[str]

    _solution_builder: CPSolutionBuilder

    def __init__(self,
                 problem_name: str,
                 cpsolver_config: Dict,
                 args_names: List[str],
                 configuration: Dict,
                 solution_builder: CPSolutionBuilder,
                 mongo_url: Optional[str] = None):
        self._config = cpsolver_config
        self._solution_builder = solution_builder
        self._camelModelFilePath = Path(cpsolver_config['request']['camelModelFilePath'])

        self._FCRcpxml_path = Path(cpsolver_config['request']['cpProblemFilePath'])
        self._FCRcpxml = etree.parse(str(self._FCRcpxml_path))
        self._nodeCandidatesFilePath = Path(cpsolver_config['request']['nodeCandidatesFilePath'])

        self._args_names = args_names
        self._xml_fillings = {}
        for name in itertools.chain(args_names, configuration):
            self._xml_fillings[name] = self._FCRcpxml.find(
                f"cpMetrics[@id='{name}']")[0]
        self._request_template = cpsolver_config['request']

        self._solution_params = list(map(lambda el: el.get('id'), self._FCRcpxml.findall('cpVariables')))

        self._cache = SolutionCache(problem_name, mongo_url)

    @abc.abstractmethod
    async def _ask_solver(self, query: CPQuery) -> CPSolution:
        """Asks solver for the solution."""
        pass

    @property
    def solution_params(self) -> List[str]:
        """Solution parameters."""
        return self._solution_params

    def _create_problem(self, query: CPQuery) -> NamedTemporaryFile:
        """Fills xml with appropriate values from the query. Output is static!"""
        for arg_name, arg_value in itertools.chain(query.parameters.items(), query.configuration.items()):
            arg_loc = self._xml_fillings[arg_name]
            arg_loc.set('value', str(arg_value))

        tempfile = NamedTemporaryFile()

        self._FCRcpxml.write(tempfile.name,
                             xml_declaration=True, encoding="ASCII")

        return tempfile

    async def get_solution(self,
                           query: CPQuery,
                           it: int) -> CPSolution:
        """Calls cp-solver for solution.

        Args:
            query (CPQuery): parameters for cpsolver.
            it (int): iteration number, for debug purposes.

        Returns:
            CPSolution: CPsolver solution.
        """
        logger.debug(f'Looking for cached solution for {it}: {query}')
        solution = self._cache.get(query)
        if solution:
            logger.debug(f'Found cached solution for {it}: {query} {solution}')
            return solution
        else:
            logger.debug(f'Cached solution not found. Asking cpsolver for solution {it}: {query}')
            solution = await self._ask_solver(query)
            logger.debug(f'Received solution from cp-solver. Caching solution for {it}: {query} {solution}')
            self._cache.put(query, solution)
            return solution
