import asyncio
import copy
import json
import logging
import shutil
import tarfile
import time
from io import BytesIO
from pathlib import Path
from typing import List, Optional, Dict

import docker
import httpx
from docker.models.containers import Container
from lxml import etree

from DataGenerator.models import CPQuery, CPSolution
from DataGenerator.summoners.solv_summoner import CPSolverSolutionSummoner, MAX_RETRIES, CPSolverSolutionSummonerError

logger = logging.getLogger(__name__)


def create_archive(filepath: Path) -> BytesIO:
    """Packs content of the file to the tar archive.

    Args:
        filepath(Path): path to the file.

    Returns:
        (BytesIO) stream of the tarballed file.
    """
    pw_tarstream = BytesIO()
    pw_tar = tarfile.TarFile(fileobj=pw_tarstream, mode='w')
    with open(filepath, 'rb') as file:
        file_data = file.read()
        tarinfo = tarfile.TarInfo(name=filepath.name)
        tarinfo.size = len(file_data)
        tarinfo.mtime = time.time()
        pw_tar.addfile(tarinfo, BytesIO(file_data))
        pw_tar.close()
        pw_tarstream.seek(0)
        return pw_tarstream


def docker_put(container: Container, src: Path, dst: Path) -> None:
    """Copies specified file to the container.

    Args:
        container(Container): destination container.
        src(Path): path to the source file.
        dst(Path): destination path in the container.
    """
    with create_archive(src) as archive:
        container.put_archive(str(dst), archive)


def docker_get(container, src: Path, dst: Path) -> None:
    """Copies specified file from container.

    Args:
        container(Container): source container.
        src(Path): path to the source file inside the container.
        dst(Path): destination path where to store file.
    """
    file_json = container.get_archive(str(src))
    stream, stat = file_json
    file_obj = BytesIO()
    for i in stream:
        file_obj.write(i)
    file_obj.seek(0)
    tar = tarfile.open(mode='r', fileobj=file_obj)
    text = tar.extractfile(dst.name)
    q = text.read()

    with open(dst, 'wb') as file:
        # stream, stat = container.get_archive(str(src))
        file.write(q)


class DockerCPSolverSolutionSummoner(CPSolverSolutionSummoner):
    """Responsible for connecting with dockerized cpsolver,
    and generating solved constraint problem solutions,
    based on provided parameters.

    Args:
        container_name (str): container name.
        docker_url (str): url to the docker engine.
        problem_name (str): problem name, used for cache purposes.
        cpsolver_config_path (str): path to the cpsolver config path.
        args_names (str): names of the arguments to pass to the cpsolver.
        configuration (Dict): current working configuration of the problem.
        mongo_url (str): url to the mongodb.
    """
    __container: Container
    __data_lock: asyncio.Lock
    __solution_filename: Optional[str]

    def __init__(self,
                 container_name: str,
                 docker_url: Optional[str] = None,
                 *args,
                 **kwargs):
        super().__init__(*args, **kwargs)
        if docker_url:
            docker_client = docker.DockerClient(base_url=docker_url)
        else:
            docker_client = docker.from_env()

        self.__data_lock = asyncio.Lock()

        self._request_template['camelModelFilePath'] = f'/res/{self._camelModelFilePath.name}'
        self._request_template['nodeCandidatesFilePath'] = f'/res/{self._nodeCandidatesFilePath.name}'

        self.__container = docker_client.containers.get(container_name)

        docker_put(self.__container, self._camelModelFilePath, Path('/res'))
        docker_put(self.__container, self._nodeCandidatesFilePath, Path(f'/res'))

    async def _ask_solver(self, query: CPQuery) -> CPSolution:
        """Asks solver for the solution."""
        request = copy.deepcopy(self._request_template)
        tempfile = self._create_problem(query)

        problem_local_path = Path(tempfile.name)
        shutil.copy(problem_local_path, problem_local_path.with_name(self.__container.name + '.xmi'))
        tempfile.close()

        problem_local_path = problem_local_path.with_name(self.__container.name + '.xmi')
        problem_remote_dir = Path('/res')
        request['cpProblemFilePath'] = str(problem_remote_dir.joinpath(problem_local_path.name))
        docker_put(self.__container, src=Path(problem_local_path), dst=problem_remote_dir)

        problem_local_path.unlink(missing_ok=True)
        ip = self.__container.ports["8080/tcp"][0]["HostIp"]
        if ip == '0.0.0.0':
            ip = '127.0.0.1'
        host = f'{ip}:{self.__container.ports["8080/tcp"][0]["HostPort"]}'

        retries = 0
        while retries < MAX_RETRIES:
            try:
                async with httpx.AsyncClient(timeout=(600 * (retries + 1))) as client:
                    response = await client.post(f'http://{host}/constraintProblemSolutionFromFile',
                                                 data=json.dumps(request),
                                                 headers={'Content-Type': 'application/json'},
                                                 )

                if response.status_code != 200:
                    raise CPSolverSolutionSummonerError("cp-solver returned non 200 status code")
                else:
                    break
            except Exception as exc:
                retries += 1
                if retries == MAX_RETRIES:
                    logger.warning(f'Cp-solver error: {exc}. Exceeded MAX_RETRIES')
                    raise
                else:
                    logger.warning(f'Cp-solver error {exc}. Retrying')
                    await asyncio.sleep(15)
                    continue

        if self.__solution_filename:
            solution_remote_path = problem_remote_dir.joinpath(self.__solution_filename)
            solution_local_path = problem_local_path.parent.joinpath(self.__solution_filename)
        else:
            # Change file suffix (from .xmi to -solution.xmi)
            solution_remote_path = problem_remote_dir.joinpath(request['camelModelFilePath'][:-4] + '-solution.xmi')
            solution_local_path = problem_local_path.parent.joinpath(
                Path(request['camelModelFilePath'][:-4]).name + '-solution.xmi')

        docker_get(self.__container, solution_remote_path, solution_local_path)

        sol_xelem = etree.parse(str(solution_local_path)).find("solution")
        if sol_xelem is None:
            raise CPSolverSolutionSummonerError(f"cp-solver didn't return solution")

        solution = self._solution_builder.buildSolution(solution_local_path)
        solution_local_path.unlink()
        return solution

    @classmethod
    def construct_generators(cls,
                             problem_name: str,
                             cpsolver_config: Dict,
                             args_names: List[str],
                             configuration: Dict,
                             mongo_url: str,
                             docker_url: Optional[str] = None) -> List["DockerCPSolverSolutionSummoner"]:
        """Constructs multiple CPSolverSummoners given list of the docker_url-s."""
        container_names = cpsolver_config['containers']
        solution_filename = cpsolver_config.get('solutionFileName', None)

        return [cls(
            problem_name=problem_name,
            cpsolver_config_path=cpsolver_config,
            args_names=args_names,
            configuration=configuration,
            mongo_url=mongo_url,
            docker_url=docker_url,
            container_name=container_name,
            solution_filename=solution_filename
        ) for container_name in container_names]
