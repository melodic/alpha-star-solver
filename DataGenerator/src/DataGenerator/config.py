import os

CPSOLVER_URL = os.getenv("CPSOLVER_ENDPOINT", "localhost")
CPSOLVER_PORT = int(os.getenv("CPSOLVER_PORT", "8000"))
CPSOLVER_TIMEOUT = int(os.getenv("CPSOLVER_TIMEOUT", "300"))

MODELDB_HOST: str = os.getenv("MODELDB_HOST", "modeldb")
MODELDB_PORT: str = os.getenv("MODELDB_PORT", "27017")

WATERMARK_USER = os.environ.get("WATERMARK_USER", "generator")
WATERMARK_SYSTEM = os.environ.get("WATERMARK_SYSTEM", "generator")
