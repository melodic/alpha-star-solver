import logging
from typing import Optional

import pymongo

from .config import MODELDB_HOST, MODELDB_PORT
from .models import CPQuery, CPSolution

DEFAULT_TABLE_NAME: str = 'generator_cpcache'


class SolutionCache:
    """Caches and retrieves saved solutions in mongodb."""
    _collection: pymongo.collection.Collection
    _db: pymongo.MongoClient
    _collection_name: str

    def __init__(self,
                 table_name: Optional[str] = None,
                 mongo_uri: Optional[str] = None):
        """
        Args:
            table_name: Name of the MongoDB table where to store cached cpsolver queries.
            mongo_uri: URI to the MongoDB.
        """
        self._logger = logging.getLogger(__name__)
        self._db = pymongo.MongoClient(mongo_uri if mongo_uri else f"mongodb://{MODELDB_HOST}:{MODELDB_PORT}/")

        self._collection_name = table_name if table_name is not None else DEFAULT_TABLE_NAME

        self._collection = self._db['cache'][self._collection_name]

    def get(self, query: CPQuery) -> Optional[CPSolution]:
        """Checks if query has solution in cache, returns if exists.

        Args:
            query: CPQuery to check in cache.

        Returns:
            solution if found.
        """
        solution = None
        query_result = self._collection.find_one({'query_json': query.json()})
        if query_result is not None:
            solution = CPSolution.parse_raw(query_result['solution_json'])

        return solution

    def put(self,
            query: CPQuery,
            solution: CPSolution) -> None:
        """Stores solution for given query in cache.

        Args:
            query: CPQuery to update - the one which does not have solution yet.
            solution: CPSolution to set for the certain CPQuery.

        """
        self._collection.insert_one({
            'applicationId': query.applicationId,
            'query_json': query.dict(),
            'solution_json': solution.dict()
        })
