import datetime as dt
from typing import Dict, Optional

from pydantic import BaseModel


class CPQuery(BaseModel):
    """CP-solver query data model."""
    applicationId: str
    parameters: Dict[str, str]
    configuration: Dict[str, str]
    it: int


class CPSolution(BaseModel):
    """CP-solver solution data model."""
    applicationId: str
    variables: Dict[str, str]
    configuration: Dict[str, str]

