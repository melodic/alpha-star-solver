import datetime as dt
import logging
from typing import Generator, Dict, List, Optional, Union

import pandas as pd
from morphemic.dataset import DatasetMaker

from DataReading.abstractreader import _match_columns, TIME_COLUMN_NAMES, IRowDataProvider

logger = logging.getLogger(__name__)


def _is_float(s: str) -> bool:
    """Helper function, returns true if variable is float."""
    try:
        float(s)
        return True
    except ValueError:
        return False


class InfluxDataProvider(IRowDataProvider):
    """
    Provides serialized data from InfluxDB. Recognizes datatypes and
    time columns from specific source.

    Args:
        - applicationId: unique applicationId, needed to distinguish datasets from InfluxDB
        - influxConfigs: influx configs, required by morphemic-datasetmaker
        - startTimestamp: start of the data (inclusive)
        - finishTimestamp: end of the data (inclusive)
    """
    _timestamp_column_name: str
    _time_columns: List[str]
    _startTimestamp: dt.datetime
    _finishTimestamp: dt.datetime
    _headers: List[str]
    _data: List[Dict[str, Union[dt.datetime, float, int, str]]]

    _column_types: Dict[str, type]

    def __init__(self,
                 applicationId: str,
                 influxConfigs: Dict[str, str],
                 startTimestamp: Optional[dt.datetime] = None,
                 finishTimestamp: Optional[dt.datetime] = None,
                 *args,
                 **kwargs):
        super().__init__(*args, **kwargs)
        self._startTimestamp = startTimestamp
        self._finishTimestamp = finishTimestamp

        if startTimestamp is not None:
            # Workaround because DatasetMaker requires time period in MySql time format
            # and it must be 1 word -> we are formatting it to seconds since now :)
            time_difference = dt.datetime.now().replace(tzinfo=startTimestamp.tzinfo) - startTimestamp
            startTimestamp_sql = f'{round(time_difference.total_seconds())}s'
        else:
            startTimestamp_sql = None

        logger.info(f'Retrieving metrics from InfluxDB, appID: {applicationId}')
        logger.info(f'INFLUX CONFIGS: {influxConfigs}')
        datasetmaker = DatasetMaker(applicationId, startTimestamp_sql, influxConfigs)

        raw_data = datasetmaker.getData()
        self._headers = raw_data[0]
        self._data = raw_data[1]
        time_columns = _match_columns(self._headers, TIME_COLUMN_NAMES)

        timestamp_columns = _match_columns(self._headers, self._timestamp_column_names)
        if len(timestamp_columns) == 0:
            if 'time' in time_columns:
                timestamp_columns.append('time')
        assert len(
            timestamp_columns) == 1, f'Cannot specify timestamp column, found column names: {self._headers}'
        self._timestamp_column_name = timestamp_columns[0]
        self._time_columns = time_columns
        self._column_types = {}
        for key in self._headers:
            if key in self._time_columns:
                new_value = dt.datetime
            elif key.isnumeric():
                new_value = int
            elif _is_float(self._data[0][key]):
                new_value = float
            else:
                new_value = str
            self._column_types[key] = new_value

    @property
    def timestamp_column_name(self) -> str:
        """Timestamp column name"""
        return self._timestamp_column_name

    def peek_t0(self) -> dt.datetime:
        """Returns timestamp of the first row"""
        row = self._convert_to_pytype(self._data[0])
        return row[self._timestamp_column_name]

    @property
    def column_names(self) -> List[str]:
        """Names of the variable columns"""
        return self._headers

    @property
    def column_types(self) -> Dict[str, type]:
        """Returns mapping column name -> type."""
        return self._column_types

    def _convert_to_pytype(self, row):
        res = {}
        for key, val in row.items():
            if self._column_types[key] == dt.datetime:
                res[key] = pd.to_datetime(val).to_pydatetime().replace(tzinfo=None)
            else:
                res[key] = val

        return res

    def row_generator(self) -> Generator[List[any], None, None]:
        """Generator over raw data, provides rows of data in increasing order (by timestamp)
        Returns list with values (order same as in column_names).
        """
        for row in self.row_generator_annotated():
            yield [row[name] for name in self._headers]

    def row_generator_annotated(self) -> Generator[Dict[str, any], None, None]:
        """Generator over raw data, provides rows of data in increasing order (by timestamp).
        Returns dict with column names mapping to current values.
        """
        for row in self._data:
            vals = self._convert_to_pytype(row)
            if self._finishTimestamp:
                if vals[self._timestamp_column_name] > self._finishTimestamp:
                    return
            yield vals
