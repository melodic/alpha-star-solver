import datetime as dt
from dataclasses import dataclass
from pathlib import Path
from typing import Optional, List, Dict

from pydantic import BaseModel


class ValueSpec(BaseModel):
    """Stores information about value in datasource"""
    colname: str
    alias: Optional[str] = None
    type: Optional[str] = None

    @classmethod
    def from_json(cls, spec: Dict) -> "ValueSpec":
        """Builds instance from json specification."""
        return cls(colname=spec['colname'], alias=spec.get('alias', None), type=spec.get('type', None))


@dataclass
class DataSpec:
    """Stores information about datasource (without data)."""
    desc: str
    timestamp_column: Optional[str]
    values: List[ValueSpec]
    time_root: bool

    def get_column_type(self, colname: str) -> Optional[str]:
        for col in self.values:
            if col.colname == colname:
                return col.type
        raise KeyError


@dataclass
class DataSource:
    """Stores specific datasource data."""
    tag: str
    data_type: str
    path: Path
    spec: DataSpec

    @classmethod
    def from_json(cls, source: Dict, spec: DataSpec) -> "DataSource":
        """Builds instance from json specification."""
        return cls(
            tag=source['tag'],
            data_type=source['type'],
            path=source['path'],
            spec=spec
        )


@dataclass
class DataGroup:
    """Group of related datasources from the same day (tag)."""
    tag: str
    sources: Dict[str, DataSource]
    solver_cols: List[str]
    timedelta: dt.timedelta
    max_time_gap: dt.timedelta

    @classmethod
    def from_json(cls,
                  solver_cols: List[str],
                  timedelta: dt.timedelta,
                  max_time_gap: dt.timedelta,
                  datasources: List[Dict]) -> List["DataGroup"]:
        """Builds instance from json specification."""
        groups_by_desc = {}
        groups_by_tag = {}
        for datasource in datasources:
            spec = DataSpec(desc=datasource['desc'],
                            timestamp_column=datasource.get('timestamp_column', None),
                            values=[ValueSpec.from_json(val_spec) for val_spec in datasource['values']],
                            time_root=datasource.get('time_root', False))
            groups_by_desc[spec.desc] = {}
            for raw_source in datasource['sources']:
                source = DataSource.from_json(raw_source, spec)
                if source.tag in groups_by_desc[spec.desc]:
                    raise KeyError(f'Same datasource have two sources with same tag! tag: {source.tag}, '
                                   f'desc: {spec.desc}')
                groups_by_desc[spec.desc][source.tag] = source
                if source.tag not in groups_by_tag:
                    groups_by_tag[source.tag] = {}
                groups_by_tag[source.tag][spec.desc] = source

        assert len(set([len(specs) for specs in groups_by_tag.values()])) == 1, "Some data are missing"  # TODO

        res = []
        for tag, group in groups_by_tag.items():
            res.append(DataGroup(
                sources=group,
                tag=tag,
                solver_cols=solver_cols,
                timedelta=timedelta,
                max_time_gap=max_time_gap
            ))
        return res


@dataclass
class DataConfig:
    """Complete information about data provided with json configuration"""
    outpath: Path
    problem_name: str
    wildcard: Optional[List[str]]
    datagroups: List[DataGroup]
    solver_cols: List[str]
    timedelta: dt.timedelta
    max_time_gap: dt.timedelta
    configuration: Dict

    @classmethod
    def from_json(cls, source: Dict):
        """Builds instance from json specification."""
        solver_vars = source['solver_cols']
        problem_name = source['problem_name']
        wildcard = source.get("wildcard", None)
        outpath = Path(source["outpath"]).resolve()
        assert outpath.suffix == '.csv', "Outpath file must be a csv type"
        datagroups = DataGroup.from_json(
            datasources=source['datasources'],
            solver_cols=solver_vars,
            timedelta=dt.timedelta(seconds=source['timedelta']),
            max_time_gap=dt.timedelta(seconds=source['max_time_gap']))
        return cls(
            outpath=outpath,
            problem_name=problem_name,
            wildcard=wildcard,
            solver_cols=solver_vars,
            timedelta=dt.timedelta(seconds=source['timedelta']),
            max_time_gap=dt.timedelta(seconds=source['max_time_gap']),
            datagroups=datagroups,
            configuration=source['configuration']
        )
