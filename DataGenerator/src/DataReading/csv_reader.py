import csv
import datetime as dt
import logging
from pathlib import Path
from typing import Generator, Dict, Iterable, Union, List, Optional, Type

import pandas as pd

from DataReading.abstractreader import IRowDataProvider, TIME_COLUMN_NAMES, _match_columns
from DataReading.config_data_structures import ValueSpec

logger = logging.getLogger(__name__)


class CSVReader(IRowDataProvider):
    """CSV data row_generator implementation."""

    __path: Path
    __delimiter: str
    __arr: pd.DataFrame

    __lines: List[str]

    _timestamp_column_name: str

    __csv_header: List[str]
    __column_types: Dict[str, type]

    def __init__(self,
                 path: Union[Path, str],
                 max_time_difference: Optional[dt.timedelta] = None,
                 delimiter: str = ',',
                 timestamp_column_name: Optional[str] = None):
        super().__init__(timestamp_column_name=timestamp_column_name,
                         max_time_difference=max_time_difference)

        """
        Args:
            path: (Union[Path, str]) path to the csv file.
            delimiter: (str) csv file felimiter.
            timestamp_column_name: (str) timestamp column name (if unset, it will be guessed).
        """
        # Check args
        assert path is not None, 'Unset path!'
        if isinstance(path, str):
            path = Path(path)
        if not path.exists():
            raise FileNotFoundError(f'File {path} not found')
        self.__path = path
        self.__delimiter = delimiter

        # Peek headers
        with open(self.__path, newline='') as file:
            assert csv.Sniffer().has_header(file.readline()), "CSV file has no header"
            file.seek(0)
            self.__csv_header = file.readline().strip('\n').split(delimiter)

            time_columns = _match_columns(self.__csv_header, TIME_COLUMN_NAMES)

            timestamp_columns = _match_columns(self.__csv_header, self._timestamp_column_names)
            if len(timestamp_columns) == 0 and len(time_columns) == 1:
                timestamp_columns = time_columns

            assert len(
                timestamp_columns) == 1, f'Cannot specify timestamp column, found column names: {self.__csv_header}'
            timestamp_column_name = timestamp_columns[0]

        # Read from file = be aware of high memory usage
        self.__arr = pd.read_csv(
            path,
            parse_dates=time_columns
        )
        assert self.__arr[timestamp_column_name].is_monotonic_increasing, 'Timestamps in column are not increasing'
        assert self.__arr[timestamp_column_name].is_unique, 'Found >=2 equal timestamps'

        self.__timestamp_column_name = timestamp_column_name
        self.__column_types = {}
        for key, name in self.__arr.dtypes.apply(lambda x: x.name).to_dict().items():
            if name == 'object':
                new_value = str
            elif name.startswith('datetime'):
                new_value = dt.datetime
            elif name.startswith('float'):
                new_value = float
            elif name.startswith('int'):
                new_value = int
            elif name == 'bool':
                new_value = bool
            else:
                raise NotImplementedError("Unknown datatype format")
            self.__column_types[key] = new_value

    @property
    def column_names(self) -> List[str]:
        """Names of the variable columns"""
        return self.__csv_header

    @property
    def column_types(self) -> Dict[str, Type]:
        """Names of the variable columns"""
        return self.__column_types

    @property
    def columns(self) -> List[ValueSpec]:
        """Names of the variable columns"""
        return [ValueSpec(colname=colname, type=str(coltype)) for colname, coltype in self.__column_types.items()]

    @property
    def timestamp_column_name(self) -> str:
        """Timestamp column name"""
        return self.__timestamp_column_name

    def peek_t0(self) -> dt.datetime:
        """Peeks first timestamp from file

        Returns:
            (dt.datetime) first timestamp in file`
            """
        return CSVReader._convert_to_pytype(getattr(self.__arr.iloc[0], self.__timestamp_column_name))

    @staticmethod
    def _convert_to_pytype(value):
        if isinstance(value, pd._libs.tslibs.timestamps.Timestamp):
            return value.to_pydatetime().replace(tzinfo=None)
        else:
            return value

    def row_generator(self) -> Generator[Iterable[type], None, None]:
        """Returns iterator over rows.

        Yields:
            Iterable: values in the next row (order is the same as in column_names).

        Raises:
            TooBigTimeDifference
        """
        timestamp_index = self.__csv_header.index(self.__timestamp_column_name)
        for _, row in self.__arr.iterrows():
            pandas_values: List = row.values.tolist()
            values = list(map(CSVReader._convert_to_pytype, pandas_values))
            self._check_time_difference(values[timestamp_index])
            yield values

    def row_generator_annotated(self) -> Generator[Dict[str, any], None, None]:
        """Returns dict iterator over rows.

        Yields:
            Dicttype[str, any]: name of the columns mapping to its values in the current row.

        Raises:
            TooBigTimeDifference
        """
        for index, row in self.__arr.iterrows():
            mapped_values: Dict = dict(row.to_dict())
            for key, val in mapped_values.items():
                mapped_values[key] = CSVReader._convert_to_pytype(val)
            self._check_time_difference(mapped_values[self.__timestamp_column_name])
            yield mapped_values
