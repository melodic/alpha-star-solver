import datetime as dt
from numbers import Number
from scipy import interpolate
from typing import Dict, Generator, Optional, List

from DataReading.abstractreader import IRowDataProvider


class EmptyReaderException(Exception):
    """Raised when reader has no values in it."""


def _delta_in_seconds(t1: dt.datetime, t2: dt.datetime) -> int:
    return (t1 - t2).seconds


class InterpolatedDataStream:
    """
        generator that interpolates data given by
        row_generator in points given by timestamp_generator
        with additional config from datasource.

        Interpolator returns dict which includes:
            1. Mapping column names given by row_generator
                (only numerical types) to its
                interpolated values in points returned
                by timestamp_generator.
            2. Mapping from row_generator column name that
                contains data timestamp to timestamp
                given by timestamp_generator

        NOTE:
            timestamps are type of datatime.datatime

        If value is set to None interpolator ignores
        this value.

        Interpolation stops after last numerical value
        other columns are extrapolated if not given
        to that point.


        NOTE:
            currently interpolates all of the data
            if dataset will grow it might have some
            performance issues.
    """

    _last_ts: Optional[dt.datetime]
    _t0: Optional[dt.datetime]

    def __init__(self,
                 reader: IRowDataProvider,
                 timestamp_generator: Generator[dt.datetime, None, None],
                 solver_vars: List[str]):

        # Preserve timestamp generator
        self._gen = timestamp_generator
        self._t0 = self._last_ts = None
        previous_timestamp: Optional[dt.datetime] = None

        self._reader = reader

        interpolants_col_embed = []
        interpolants_col_str = []

        for colname, coltype in reader.column_types.items():
            if colname not in solver_vars and reader.timestamp_column_name != colname:
                continue
            if issubclass(coltype, Number):
                interpolants_col_embed.append(colname)
            elif issubclass(coltype, str):
                interpolants_col_str.append(colname)

        # col name -> (timestamp embedding, given value)
        self.raw_data = {colname: ([], []) for colname in interpolants_col_embed if colname in solver_vars}
        self._str_interpolants = {colname: [] for colname in interpolants_col_str if colname in solver_vars}

        # populate self.inp_data
        for row in reader.row_generator_annotated():

            row_ts = row[self._reader.timestamp_column_name]
            if self._t0 is None:
                self._t0 = row[self._reader.timestamp_column_name]
            self._last_ts = row_ts

            if previous_timestamp:
                tdelta_embed = _delta_in_seconds(row_ts, previous_timestamp)
            else:
                tdelta_embed = 0
                previous_timestamp = row_ts

            for colname in interpolants_col_embed:
                if row[colname] is None:
                    continue

                self.raw_data[colname][0].append(tdelta_embed)
                self.raw_data[colname][1].append(row[colname])

            for colname_str in interpolants_col_str:
                if row[colname_str] is None:
                    continue

                row_ts = row[self._reader.timestamp_column_name]
                self._str_interpolants[colname_str].append((row_ts, row[colname_str]))

        if any(map(lambda v: len(v[0]) == len(v[1]) == 0, self.raw_data.values())):
            raise EmptyReaderException

        # interpolate aggregated data
        self.interpolants = {}
        for colname, cdata in self.raw_data.items():
            # May be optimized
            self.interpolants[colname] = interpolate.interp1d(x=cdata[0], y=cdata[1], fill_value='extrapolate')

    def __iter__(self):
        return self

    def __next__(self) -> Dict:
        ts = next(self._gen)

        if ts > self._last_ts:
            raise StopIteration

        row_dict = {self._reader.timestamp_column_name: ts}
        ts_embed = _delta_in_seconds(ts, self._t0)

        for colname, interpolant in self.interpolants.items():
            row_dict[colname] = interpolant(ts_embed).flat[0]
            if self._reader.column_types[colname] == int:
                row_dict[colname] = round(row_dict[colname])

        for colname, interpolant in self._str_interpolants.items():
            timestamp, value = interpolant[0]
            while ts > timestamp and len(interpolant) > 1:
                interpolant.pop(0)
                timestamp, value = interpolant[0]
            row_dict[colname] = value

        return row_dict
