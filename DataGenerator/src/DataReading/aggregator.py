import datetime as dt
import logging
from pathlib import Path
from typing import Dict, List, Tuple, Generator, Optional

import numpy as np

from DataReading.config_data_structures import DataGroup, DataSource
from DataReading.interpolator import InterpolatedDataStream
from DataReading.abstractreader import IRowDataProvider
from DataReading.csv_reader import CSVReader


def _timestamp_generator(t0: dt.datetime, tstep: dt.timedelta) -> Generator[dt.datetime, None, None]:
    t = t0
    while True:
        yield t
        t += tstep


class DataAggregator:
    """Aggregates data from multiple sources and uneven time periods.

    Args:
        datagroup (DataGroup): datagroup with data & information about the data.
        timedelta (dt.timedelta): time between data samples.
        max_time_gap (dt.timedelta): max time between 2 data samples (in source files).
        aggregated_cols: (List[str]): list of the column names to extract.
    """
    __datagroup: DataGroup
    __datasources: List[Tuple[DataSource, IRowDataProvider]]

    __header: List[str]

    __roottime_reader: IRowDataProvider
    __roottime_datasource: DataSource

    __delta: dt.timedelta
    __max_time_gap: dt.timedelta

    def __init__(self,
                 datagroup: DataGroup,
                 timedelta: dt.timedelta,
                 max_time_gap: dt.timedelta,
                 wildcard: Optional[str] = None):
        self.__datagroup = datagroup
        self.__datasources = []
        self.__header = []
        # Roottime data source
        roottime_data = None
        self.__max_time_gap = max_time_gap
        # Parse data sources
        for desc, source in self.__datagroup.sources.items():
            timestamp_column = source.spec.timestamp_column
            logging.debug(f'Found datasource {desc}')

            if source.data_type == 'csv':
                csv_path = source.path
                if wildcard:
                    csv_path = Path(str(csv_path).replace('*', wildcard))
                reader = CSVReader(csv_path, timestamp_column_name=timestamp_column,
                                   max_time_difference=self.__max_time_gap,
                                   data_spec=source.spec)
                self.__datasources.append((source, reader))
            else:
                raise NotImplementedError("Unsupported source type")

            # if datasource specified as root time - save it
            if source.spec.time_root:
                if roottime_data is None:
                    roottime_data = (source, reader)
                else:
                    raise AttributeError("Specified more than 1 time root")

            self.__header.extend(reader.column_names)

        self.__delta = timedelta

        # if no roottime datasource specified - get first
        if roottime_data is None:
            roottime_data = self.__datasources[0]

        self.__t0 = roottime_data[1].peek_t0()
        self.__roottime_reader = roottime_data[1]
        self.__roottime_datasource = roottime_data[0]

    @property
    def column_names(self) -> List[str]:
        """Names of the variable columns"""
        res = []
        for sub in self.__datagroup.sources.values():
            res.extend(map(lambda x: x.alias if x.alias else x.colname, sub.spec.values))
        return res

    def row_generator_annotated(self) -> Generator[Dict[str, any], None, None]:
        """Generates values mapping with correct and even time periods.
        Combines all values from different sources and unify their timestamps.

        Yields:
            (Mapping[str, any]) column_alias -> value      """
        # Interpolate each stream
        interpolated_streams = []
        for datasource, reader in self.__datasources:
            interpolated_streams.append(
                (datasource,
                 InterpolatedDataStream(reader=reader,
                                        timestamp_generator=_timestamp_generator(self.__t0, self.__delta),
                                        solver_vars=[value.colname for value in datasource.spec.values])))
        try:
            while True:
                grouped_values = []
                combined_values = {}
                # Get columns values and save their mappings
                for datasource, stream in interpolated_streams:
                    interpolated_rowdict = next(stream)
                    grouped_values.append((datasource, interpolated_rowdict))

                    for values_spec in datasource.spec.values:
                        final_colname = values_spec.alias if values_spec.alias is not None else values_spec.colname
                        combined_values[final_colname] = interpolated_rowdict[values_spec.colname]

                # Convert values to pythonic
                for key, value in combined_values.items():
                    if any(map(lambda numpy_type: isinstance(value, numpy_type),
                               [np.float64, np.float32, np.int16, np.uint32])):
                        combined_values[key] = value.item()

                yield combined_values
        except StopIteration:
            return
