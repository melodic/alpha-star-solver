import abc
import datetime as dt
from typing import Dict, Generator, Optional, List

TIME_COLUMN_NAMES = ['time', 'date', 'timestamp', 'datetime']
TIMESTAMP_KEY_COLUMN_NAMES = ['timestamp', 'datetime']


class TooBigTimeDifference(Exception):
    """Raised when two next rows are too far away from each other (timestamps)."""
    pass


def _match_columns(column: List[str], keys: List[str]) -> List[str]:
    """Filters column with names including any phrase from keys list."""
    return list(filter(lambda itername: any(map(lambda keyname: keyname in itername, keys)), column))


class IRowDataProvider(abc.ABC):
    """Data provider interface specification. Supplied with
    time-series data generators, which provides data measured in
    increasing, non repetitive timestamps. Time between timestamps *may*
    vary.

    Args:
        max_time_difference: Max time difference between samples in provided data source.
        timestamp_column_name: Optionally name of the column with timestamp.
    """

    _timestamp_column_names: List[str]
    __previous_timestamp: Optional[dt.datetime]
    __max_time_difference: Optional[dt.timedelta]

    def __init__(self,
                 max_time_difference: Optional[dt.timedelta] = None,
                 timestamp_column_name: Optional[str] = None):
        self.__max_time_difference = max_time_difference
        self.__previous_timestamp = None

        if timestamp_column_name:
            self._timestamp_column_names = [timestamp_column_name]
        else:
            self._timestamp_column_names = TIMESTAMP_KEY_COLUMN_NAMES

    def _check_time_difference(self, timestamp: dt.datetime):
        if self.__previous_timestamp is None:
            self.__previous_timestamp = timestamp
        else:
            if self.__max_time_difference is not None:
                if self.__previous_timestamp + self.__max_time_difference < timestamp:
                    raise TooBigTimeDifference(
                        f'Previous timestamp: {self.__previous_timestamp}, '
                        f'actual timestamp: {timestamp}, max difference: {self.__max_time_difference}')
            self.__previous_timestamp = timestamp

    @property
    @abc.abstractmethod
    def timestamp_column_name(self) -> str:
        """Timestamp column name"""
        pass

    @abc.abstractmethod
    def peek_t0(self) -> dt.datetime:
        """Returns timestamp of the first row"""
        pass

    @property
    @abc.abstractmethod
    def column_names(self) -> List[str]:
        """Names of the variable columns"""
        pass

    @property
    @abc.abstractmethod
    def column_types(self) -> Dict[str, type]:
        """Returns mapping column name -> type."""
        raise NotImplementedError

    @abc.abstractmethod
    def row_generator(self) -> Generator[List[any], None, None]:
        """Generator over raw data, provides rows of data in increasing order (by timestamp)
        Returns list with values (order same as in column_names).
        """
        pass

    @abc.abstractmethod
    def row_generator_annotated(self) -> Generator[Dict[str, any], None, None]:
        """Generator over raw data, provides rows of data in increasing order (by timestamp).
        Returns dict with column names mapping to current values.
        """
        pass
