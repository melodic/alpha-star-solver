""" Generates data using CP-solver and FCR time series for network training """


# OBSOLETE CODE
# Used only in 1st phase of manual data generation

import argparse
import asyncio
import copy
import csv
import logging
from pathlib import Path
from typing import List

from DataGenerator.summoners.docker_summoner import DockerCPSolverSolutionSummoner
from DataGenerator.summoners.local_summoner import CPSolverSolutionSummoner
from DataGenerator.summoners.local_summoner import LocalCPSolverSolutionSummoner
from DataReading.aggregator import DataAggregator
from DataReading.config_data_structures import DataConfig
from .configuration.config_schemas import DATA_CONFIG_SCHEMA, CPSOLVER_CONFIG_SCHEMA
from .configuration.validate_config import validate_config

logging.basicConfig(format='%(name)s: %(levelname)s:%(message)s', level=logging.INFO)


def _json_path(path_str: str) -> Path:
    if Path(path_str).is_file() and path_str.endswith('.json'):
        return Path(path_str)
    else:
        raise argparse.ArgumentTypeError(f"{path_str} is not a valid path")


class Generator:
    """Async multiprocess data generator.

    Args:
        data_config (DataConfig): parsed DataConfig.
        solver_args_names (List[str]): list of the arguments, which must be passed to the cpsolver.
        summoners (List[CPSolverSolutionSummoner]): solver summoners (each for one worker).
    """
    __data_config: DataConfig
    __job_queue: asyncio.Queue
    __result_queue: asyncio.PriorityQueue
    __is_done = asyncio.Event
    __cpsolvers: List[CPSolverSolutionSummoner]

    def __init__(self,
                 data_config: DataConfig,
                 solver_args_names: List[str],
                 summoners: List[CPSolverSolutionSummoner]):
        self.__data_config = data_config
        self.__query_builder = CPQueryBuilder(data_config.problem_name, solver_args_names, data_config.configuration)
        self.__cpsolvers = summoners
        self.__solution_params = summoners[0].solution_params
        self._logger = logging.getLogger(__name__)

    def run(self) -> None:
        """Runs data generation"""
        asyncio.run(self._run())

    async def _run(self) -> None:
        # Create async data structures
        self.__job_queue = asyncio.Queue()
        self.__result_queue = asyncio.PriorityQueue()
        self.__is_done = asyncio.Event()
        # Create aggregators for each data source group
        self.__aggregators = []
        if self.__data_config.wildcard is None:
            self.__data_config.wildcard = [None]
        for wildcard in self.__data_config.wildcard:
            for datagroup in self.__data_config.datagroups:
                self.__aggregators.append(DataAggregator(datagroup,
                                                         self.__data_config.timedelta,
                                                         self.__data_config.max_time_gap,
                                                         wildcard))
        # Save original header for csvwriter
        self.__original_header = list(dict.fromkeys(self.__aggregators[0].column_names))
        await self.schedule()

        # Create worker tasks to process the queue concurrently.
        tasks = []
        for cpsolver in self.__cpsolvers:
            task = asyncio.create_task(self.worker(cpsolver))
            tasks.append(task)
        # Run scheduler, after it ends signalise end of the work to the acquirer and wait for it's end
        await self.__job_queue.join()

        # Cancel our worker tasks.
        for task in tasks:
            task.cancel()
        # Wait until all worker tasks are cancelled.
        await asyncio.gather(*tasks, return_exceptions=True)
        await self.retrieve_tasks()

    async def worker(self, cpsolver: CPSolverSolutionSummoner) -> None:
        """Performs tasks from the task queue with appropriate, unique cpsolver."""
        try:
            while not self.__job_queue.empty():
                tag, acquired_row, query_real = await self.__job_queue.get()
                it, experimentId = tag

                solution_real = await cpsolver.get_solution(query_real, it)

                await self.__result_queue.put((tag, (acquired_row, solution_real)))
                self.__job_queue.task_done()
        except Exception as exc:
            self._logger.critical(f'Worker received exception {exc}')

    async def retrieve_tasks(self) -> None:
        """Retrieves task result from the queue and saves to the output file."""
        try:
            with open(self.__data_config.outpath.with_suffix('.csv'), 'w', newline='') as csv_output:
                writer = csv.writer(csv_output, delimiter=',')
                solver_header = copy.deepcopy(self.__solution_params)

                # Add configuration headers
                configuration_keys = self.__data_config.configuration.keys()
                solver_header += configuration_keys

                writer.writerow(self.__original_header + ["experimentId"] + solver_header)

                while not self.__result_queue.empty():
                    (it, experimentId), (acquired_row, solution_real) = await self.__result_queue.get()
                    print(f'Row {it} acquired')

                    output_row = [acquired_row[colname] for colname in
                                  list(dict.fromkeys(self.__aggregators[0].column_names))] \
                                 + [experimentId] \
                                 + solution_real.list_variables() \
                                 + [self.__data_config.configuration[key] for key in configuration_keys]

                    writer.writerow(output_row)
        except Exception as exc:
            self._logger.critical(exc)
            raise

    async def schedule(self) -> None:
        """Schedules task requests."""
        it = 0
        experimentId = 0
        for aggregator in self.__aggregators:
            # For each row
            for row in aggregator.row_generator_annotated():
                # for real times
                query_real = {}
                for solver_col in self.__data_config.solver_cols:
                    query_real[solver_col] = row[solver_col]
                real_query = self.__query_builder.buildQuery(query_real)
                await self.__job_queue.put(((it, experimentId), row, real_query))
                it += 1
            experimentId += 1
        self._logger.info('Finished scheduling tasks')


def run():
    """Main program workflow."""
    # Argument parsing
    parser = argparse.ArgumentParser(description="FCR data aggregation interpolation and generation tool")

    parser.add_argument('cp_config', action='store', type=_json_path, help="Path to cpsolver json config file")
    parser.add_argument('gendata_config', action='store', type=_json_path,
                        help="Path to cpsolver json config file")
    parser.add_argument('-r', '--docker', action='store_true', default=False,
                        help="Use dockerized cpsolver instead of localhost")
    parser.add_argument('--docker-url', action='store', type=str, default=None,
                        help='URL to the remote docker (local by default)')
    parser.add_argument('--mongo-url', action='store', type=str, default=None,
                        help='URL to the MongoDB')
    parser.add_argument('-d', '--debug', action='store_true', default=False,
                        help='Output debug data')

    args = parser.parse_args()
    if args.debug:
        logging.getLogger().setLevel(logging.DEBUG)

    # Construct data classes
    cpsolver_config_path = args.cp_config
    gendata_config_path = Path(args.gendata_config)

    gendata_config = validate_config(gendata_config_path, DATA_CONFIG_SCHEMA)
    data_config = DataConfig.from_json(gendata_config)

    # Create CPSolver summoner
    solver_args_names = [solver_var for solver_var in data_config.solver_cols]

    configuration = data_config.configuration
    cpsolver_config = validate_config(cpsolver_config_path, CPSOLVER_CONFIG_SCHEMA)

    if args.docker:
        cpsolvers = DockerCPSolverSolutionSummoner.construct_generators(problem_name=data_config.problem_name,
                                                                        args_names=solver_args_names,
                                                                        configuration=configuration,
                                                                        docker_url=args.docker_url,
                                                                        mongo_url=args.mongo_url,
                                                                        cpsolver_config=cpsolver_config)

        gen = Generator(data_config, solver_args_names, cpsolvers)
        gen.run()

    else:
        cpsolver = LocalCPSolverSolutionSummoner(data_config.problem_name,
                                                 cpsolver_config_path,
                                                 solver_args_names,
                                                 args.mongo_url,
                                                 configuration=configuration)

        gen = Generator(data_config, solver_args_names, [cpsolver])
        gen.run()


if __name__ == "__main__":
    run()
