""" Generates data using CP-solver and FCR time series for network training """

# OBSOLETE CODE
# Used only in 1st phase of manual data generation

from DataGeneratorScript.manual import run

if __name__ == "__main__":
    run()
