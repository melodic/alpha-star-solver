"""config validation module

Defines validate_config function responsible for validating path to config file,
it's structure and content
"""

import json
import logging
import sys
from pathlib import Path
from typing import Dict

import jsonschema

# create two custom types: "filepath" (path to exsisting file)
# and "creatablepath" (path in which new file can be created)
type_checker = jsonschema.Draft3Validator.TYPE_CHECKER.redefine_many({
    "filepath": lambda checker, path: Path(path).is_file() or '*' in path,
    "dirpath": lambda checker, path: Path(path).parent.is_dir()
})
customValidator = jsonschema.validators.extend(
    jsonschema.Draft3Validator, type_checker=type_checker)


def validate_config(conf_p: Path, schema: Dict):
    """ validates given config path, config path structure and it content,
        returns validated json object
    """
    if not conf_p.is_file():
        logging.critical(f"given path: {conf_p} doesn't lead to file")
        sys.exit(1)

    j_conf = None
    with conf_p.open() as f:
        try:
            j_conf = json.load(f)
        except json.decoder.JSONDecodeError as err:
            logging.critical(f"error decoding json config: {err}")
            sys.exit(1)

    try:
        validator = customValidator(schema=schema)
        validator.validate(j_conf)
    except jsonschema.exceptions.ValidationError as e:
        logging.critical(f'json validation error: {e.message}')
        sys.exit(1)
    except jsonschema.exceptions.SchemaError as e:
        logging.critical(f'json schema error: {e.message}')
        sys.exit(1)

    return j_conf
