CPSOLVER_CONFIG_SCHEMA = {
    "type": "object",
    "properties": {
        "request": {
            "type": "object",
            "properties": {
                "applicationId": {"type": "string"},
                "camelModelFilePath": {"type": "filepath"},
                "cpProblemFilePath": {"type": "filepath"},
                "nodeCandidatesFilePath": {"type": "filepath"},
                "watermark": {
                    "type": "object",
                    "properties": {
                        "user": {"type": "string"},
                        "system": {"type": "string"},
                        "date": {"type": "string"},
                        "uuid": {"type": "string"},
                    },
                    "minProperties": 4
                }
            },
            "minProperties": 5
        },
        "cpSolverHost": {"type": "string"},  # for example "localhost:8080"
        "containers": {
            "type": "array",
            "minItems": 1,
            "items": {
                "type": "string",
            }
        }
    },
    "required": ["request"]
}

DATA_CONFIG_SCHEMA = {
    "type": "object",
    "properties": {
        "outpath": {"type": "dirpath"},
        "problem_name": {"type": "string"},
        "wildcard": {
            "type": "array",
            "minItems": 1,
            "items": {"type": "string"}
        },
        "datasources": {
            "type": "array",
            "minItems": 1,
            "items": {
                "type": "object",
                "properties": {
                    "desc": {"type": "string"},
                    "type": {"type": "string"},
                    "sources": {
                        "type": "array",
                        "minItems": 1,
                        "items": {
                            "type": "object",
                            "properties": {
                                "tag": {"type": "string"},
                                "type": {"enum": ["csv"]},
                                "path": {"type": "filepath"}
                            },
                            "required": ["type", "path", "tag"]
                        }
                    },
                    "timestamp_column": {"type": "string"},
                    "values": {
                        "type": "array",
                        "minItems": 1,
                        "items": {
                            "type": "object",
                            "properties": {
                                "column_name": {"type": "string"},
                                "alias": {"type": "string"},
                                "cumulation_period": {"type": "integer"},
                                "type": {"enum": ["string", "float", "int"]}
                            },
                            "required": ["column_name"]
                        }
                    },
                    "time_root":
                        {"type": "boolean"}
                },
                "required": ["desc", "type", "sources", "values"]
            }
        },
        "timedelta":
            {"type": "integer"},
        "max_time_gap":
            {"type": "integer"},
        "solver_cols":
            {
                "type": "array",
                "minItems": 1,
                "items": {"type": "string"},
            },
        "configuration":
            {
                "type": "object"
            }
    },
    "required": ["outdir_path", "problem_name", "datafiles", "timedelta", "max_time_gap",
                 "solver_cols"]
}
