import datetime as dt
from enum import Enum
from typing import Optional, Dict, Union

from pydantic import validator
from pydantic.main import BaseModel

from DataGenerator.models import CPQuery, CPSolution
from .config import DEFAULT_DELTA


class MongoModel(BaseModel):

    def to_mongo(self):
        def _format(dump: Dict):
            dumped = {}
            for key, value in dump.items():
                if isinstance(value, dt.datetime):
                    dumped[key] = value.isoformat()
                elif isinstance(value, dt.timedelta):
                    dumped[key] = value.total_seconds()
                elif isinstance(value, Dict):
                    dumped[key] = _format(value)
                else:
                    dumped[key] = value
            return dumped

        return _format(self.dict())

    @classmethod
    def from_mongo(cls, raw: Dict):
        def _format(dump: Dict):
            dumped = {}
            for key, value in dump.items():
                if isinstance(key, dt.datetime):
                    dumped[key] = dt.datetime.fromisoformat(value)
                elif isinstance(key, dt.timedelta):
                    dumped[key] = dt.timedelta(seconds=value)
                elif isinstance(key, Dict):
                    dumped[key] = _format(value)
                else:
                    dumped[key] = value
            return dumped

        return cls(**_format(raw))


class JobResult(Enum):
    """Possible data generation job results"""
    ok = "ok"
    error = "error"


class DataGenerationInfo(MongoModel):
    """Data generation job related information."""
    applicationId: str
    trainRatio: float  # [0;1] range
    cdoProblemPath: str
    returnEndpoint: str
    startTimestamp: Optional[dt.datetime]
    finishTimestamp: Optional[dt.datetime]
    delta: dt.timedelta = DEFAULT_DELTA
    metricsInfluxMapping: Dict[str, str] = {}
    configurationInfluxMapping: Dict[str, str] = {}

    @validator('trainRatio')
    def ratio_in_range(cls, v: float):
        """Validates if test ratio is in 0-1 range"""
        if not (0 <= v <= 1):
            raise ValueError(f'Train ratio not in [0;1] range: {v}')
        return v


class DataGenerationResult(MongoModel):
    """Data generation job status"""
    dataGenerationInfo: DataGenerationInfo
    result: JobResult
    error_msg: Optional[str] = None


class CPSolverJobInfo(MongoModel):
    """Data generation job information.

    Scheduled by external source, consists of many tasks.
    """
    jobId: str
    remainingJobs: int
    dataGenerationInfo: DataGenerationInfo
    errorInfo: Optional[str] = None


class CPSolverTaskInfo(MongoModel):
    """Data model about single request to the CPSolver.

    Stores both query and solution (if already generated)."""
    query: CPQuery
    solution: Optional[CPSolution]
    jobId: str
    taskId: str
    state: str  # TODO replace with enum
    timestamp: dt.datetime
    predictions: Dict[str, Union[str, float, int]]


class CPSolverWatermark(MongoModel):
    """CP-solver watermark data model.

    Used in direct communication with CP-solver
    """
    user: str
    system: str
    date: dt.datetime
    uuid: str


class CPSolverNotificationResult(MongoModel):
    """CP-solver notification result data model."""
    status: str
    errorCode: str
    errorDescription: str


class CPSolverNotification(MongoModel):
    """Cp-solver notification data model.

    Received from cp-solver when problem solution is created (or some error has occured).
    """
    applicationId: str
    cdoResourcePath: str
    result: CPSolverNotificationResult
    watermark: CPSolverWatermark


class CPSolverRequest(MongoModel):
    """CP-solver request data model.

    Used in communication to cp-solver, to schedule problem solving.
    """
    applicationId: str
    cdoModelsPath: str
    notificationURI: str
    timeLimit: Optional[int] = None
    watermark: CPSolverWatermark
