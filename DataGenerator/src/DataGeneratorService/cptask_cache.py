import datetime as dt
import logging
import uuid as uuid
from typing import Optional, List, Dict, Union

import pymongo

from DataGeneratorService.config import MODELDB_HOST, MODELDB_PORT
from DataGeneratorService.models import CPSolverTaskInfo, CPSolverJobInfo, CPSolution, CPQuery, DataGenerationInfo

DEFAULT_TABLE_NAME: str = 'generator_cptasks'
logger = logging.getLogger(__name__)


class CPTaskCache:
    """Stores information about data generation job and it's subtasks.

    Each data generation query (job) is made of many tasks, which consists of
    a query and optionally solution to the query. Solution is provided by the cpsolver
    at the exposed endpoint to the cpsolver.
    """
    _collection: pymongo.collection.Collection
    _client: pymongo.MongoClient

    def __init__(self,
                 mongo_uri: Optional[str] = None):
        self._client = pymongo.MongoClient(mongo_uri if mongo_uri else f"mongodb://{MODELDB_HOST}:{MODELDB_PORT}/")

        self._collection_name = DEFAULT_TABLE_NAME

        self._db = self._client[self._collection_name]

    def create_job(self,
                   jobAmount: int,
                   data_generation_info: DataGenerationInfo) -> CPSolverJobInfo:
        """
        Registers job in the database.

        Args:
            jobAmount: amount of tasks to process.
            data_generation_info: data generation information.

        Returns:
            Newly created job info.
        """
        jobId = str(uuid.uuid4())
        job = CPSolverJobInfo(jobId=jobId,
                              remainingJobs=jobAmount,
                              dataGenerationInfo=data_generation_info)
        collection = self._db[f'generatorjobs']
        collection.insert_one(job.to_mongo())
        return job

    def create_task(self,
                    jobId: str,
                    query: CPQuery,
                    timestamp: dt.datetime,
                    predictions: Dict[str, Union[str, float, int]]) -> CPSolverTaskInfo:
        """
        Registers task in MongoDB.

        Args:
            jobId: related job_id.
            query: cpsolver query to ask.
            timestamp: query timestamp.
            predictions: query predictions.

        Returns:
            CPSolver task informatio.

        """
        collection = self._db['generatortasks']
        task_info = CPSolverTaskInfo(query=query,
                                     taskId=str(uuid.uuid4()),
                                     jobId=jobId,
                                     predictions=predictions,
                                     timestamp=timestamp,
                                     state="scheduled")
        collection.insert_one(task_info.to_mongo())
        return task_info

    def retrieve_task(self, taskId: str) -> CPSolverTaskInfo:
        """
        Retrieve task information from database.

        Args:
            taskId: related task_id.

        Returns:
            CPSolver task information.

        Raises:
            KeyError if taskID not found in DB
        """
        collection = self._db[f'generatortasks']
        res = collection.find_one({'taskId': taskId})

        if res is None:
            raise KeyError(f'Task with task_id: {taskId} was not found in db.')
        else:
            return CPSolverTaskInfo.from_mongo(res)

    def retrieve_job(self, jobId: str) -> CPSolverJobInfo:
        """
        Retrieves job with given job_id from database.

        Args:
            jobId: related JobId.

        Returns:
            Data generation job information.

        Raises:
            KeyError if job_id not found in DB
        """
        collection = self._db[f'generatorjobs']
        res = collection.find_one({'jobId': jobId})

        if res is None:
            raise KeyError(f'Job with job_id: {jobId} was not found in db.')
        else:
            return CPSolverJobInfo.from_mongo(res)

    def retrieve_undone_tasks(self,
                              jobId: str,
                              max_tasks: Optional[int]) -> List[CPSolverTaskInfo]:
        """
        Retrieve undone tasks from DB.

        Args:
            jobId: related task_id.
            max_tasks: max amount of tasks to retrieve

        Returns:
            CPSolver task information.

        Raises:
            KeyError if taskID not found in DB
        """
        collection = self._db[f'generatortasks']
        cursor = collection.find({'jobId': jobId, 'state': 'scheduled'})
        if max_tasks is not None:
            cursor.limit(max_tasks)

        res = []
        for raw_task in cursor:
            res.append(CPSolverTaskInfo.from_mongo(raw_task))

        return res

    def update_task_state(self,
                          taskId: str,
                          new_state: str) -> None:
        """
        Updates task state.

        Args:
            taskId: related task_id.
            new_state: new task state.
        """
        collection = self._db[f'generatortasks']
        collection.find_one_and_update({"taskId": taskId}, {'$set': {'state': new_state}})

    def add_solution(self,
                     taskId: str,
                     solution: CPSolution) -> None:
        """
        Adds solution to CPQuery in database.

        Args:
            taskId: related task_id.
            solution: cpproblem solution.
        """
        collection = self._db[f'generatortasks']
        task_info_raw = collection.find_one(filter={"taskId": taskId})
        if task_info_raw is not None:
            collection.remove(query={
                "taskId": taskId
            })
            task_info = CPSolverTaskInfo.from_mongo(task_info_raw)
            task_info.solution = solution
            task_info.state = "done"
            collection.insert_one(task_info.to_mongo())

    def update_job(self, jobId: str) -> bool:
        """
        Registers tasks completion.

        Args:
            jobId: related job_id.

        Returns:
            True if no jobs left.
        """
        collection = self._db[f'generatorjobs']
        collection.find_one_and_update({'jobId': jobId}, {'$inc': {'remainingJobs': -1}})
        job_info = CPSolverJobInfo.from_mongo(self._db[f'generatorjobs'].find_one({'jobId': jobId}))
        return job_info.remainingJobs == 0

    def is_job_ok(self, jobId: str) -> bool:
        """
        Checks if any errors occured during job generation.

        Args:
            jobId: related job_id.

        Returns:
            True if no errors has occured.
        """
        job = self.retrieve_job(jobId)
        return job.errorInfo is None

    def set_job_error(self, jobId: str, error_msg: str) -> None:
        """
        Sets job error.

        Args:
            jobId: related job_id.
            error_msg: error message to set.
        """
        collection = self._db[f'generatorjobs']
        collection.find_one_and_update(filter={"jobId": jobId},
                                       update={'$set': {'errorInfo': error_msg}})

    def acquire_tasks(self, jobId: str) -> List[CPSolverTaskInfo]:
        """
        Acquires all tasks related to job with id.

        Args:
            jobId: related job id.

        Returns:
            All tasks with given job_id.
        """
        collection = self._db[f'generatortasks']
        tasks_raw = collection.find({'jobId': jobId})
        tasks = list(map(CPSolverTaskInfo.from_mongo, tasks_raw))
        tasks.sort(key=lambda task: task.query.it)
        return tasks

    def clean(self, jobId: str) -> None:
        """
        Cleans database. Removes all information related with given job_id.

        Args:
            jobId: related job_id.
        """
        self._db[f'generatorjobs'].delete_many(filter={'jobId': jobId})
        self._db[f'generatortasks'].delete_many(filter={'jobId': jobId})
