import csv
import tempfile
from pathlib import Path

import requests
from modeldb import ModelDBClient

from DataGenerator.models import CPSolution
from DataGenerator.solution_cache import SolutionCache
from DataGeneratorService.cptask_cache import CPTaskCache
from DataGeneratorService.models import CPSolverJobInfo, JobResult
from DataGeneratorService.models import DataGenerationResult
from DataGeneratorService.remote_summoner import RemoteCPSolverSolutionSummoner

modeldb = ModelDBClient()

task_cache = CPTaskCache()
solution_cache = SolutionCache()


def run_task(job_info: CPSolverJobInfo,
             tasks_amount: int = 1) -> None:
    """
    Schedules task to cpsolver.

    Args:
        job_info: job related info.
        tasks_amount: how many tasks to run
    """
    summoner = RemoteCPSolverSolutionSummoner(solution_cache=solution_cache)
    tasks_to_run = task_cache.retrieve_undone_tasks(job_info.jobId, tasks_amount)

    for task in tasks_to_run:

        cached_solution = solution_cache.get(task.query)
        if cached_solution is not None:
            ack_task(solution=cached_solution, task_id=task.taskId, job_id=job_info.jobId)
        else:
            task_cache.update_task_state(task.taskId, "pending")
            summoner.ask_solver(applicationId=task.query.applicationId,
                                cdo_path=job_info.dataGenerationInfo.cdoProblemPath,
                                query=task.query,
                                task_id=task.taskId)


def ack_task(solution: CPSolution,
             task_id: str,
             job_id: str) -> None:
    """Acknowledge task completion.

    Stores information in cache about solution generation for given problem and stores it in database.
    If all problems are solved, dumps data to one file and saves it to modeldb.

    Args:
         solution: Solution to the problem.
         task_id: ID to the query which was resolved.
         job_id: JobID related to the given task.
    """
    task_cache.add_solution(task_id, solution=solution)

    job_info = task_cache.retrieve_job(job_id)
    if task_cache.update_job(job_id):
        collect(job_info)
    else:
        run_task(job_info)


def collect(job_info: CPSolverJobInfo) -> None:
    """Collects all computed solutions and generates files for training.

    Args:
        job_info: related job info which was processed.
    """

    # Collect info about job
    acq_tasks = task_cache.acquire_tasks(job_info.jobId)
    n_acq_task = len(acq_tasks)

    # Dump all data to temporary files (separate training and test files)
    with tempfile.NamedTemporaryFile(suffix='.csv') as train_csvfile, \
            tempfile.NamedTemporaryFile(suffix='.csv') as test_csvfile:
        train_writer = csv.writer(train_csvfile, delimiter=',')
        test_writer = csv.writer(test_csvfile, delimiter=',')

        # Headers
        header = []
        q0 = acq_tasks[0].query
        s0 = acq_tasks[0].solution
        header.extend(q0.parameters.keys())
        header.extend(q0.configuration.keys())
        header.extend(s0.variables.keys())
        header.extend(acq_tasks[0].predictions.keys())
        train_writer.writerow(header)
        test_writer.writerow(header)

        for i in range(n_acq_task):
            if i / n_acq_task <= job_info.dataGenerationInfo.trainRatio:
                writer = train_writer
            else:
                writer = test_writer

            task = acq_tasks[i]
            combined = {**task.query.configuration, **task.query.parameters, **task.solution.variables,
                        **task.predictions}
            row = [combined[key] for key in header]

            writer.writerow(row)

        # Upload to modeldb
        modeldb.importTestDataFromFile(application_id=job_info.dataGenerationInfo.applicationId,
                                       test_data_file_path=Path(test_csvfile.name))
        modeldb.importTrainingDataFromFile(application_id=job_info.dataGenerationInfo.applicationId,
                                           training_data_file_path=Path(train_csvfile.name))

        requests.post(job_info.dataGenerationInfo.returnEndpoint,
                      DataGenerationResult(dataGenerationInfo=job_info.dataGenerationInfo, result=JobResult.OK))
