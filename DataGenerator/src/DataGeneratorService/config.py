import datetime as dt
import os

HOSTNAME = os.getenv("HOSTNAME", "generator")

MODELDB_HOST: str = os.getenv("MODELDB_HOST", "modeldb")
MODELDB_PORT: str = os.getenv("MODELDB_PORT", "27017")

INFLUXDB_HOSTNAME = os.environ.get("INFLUXDB_HOSTNAME", "influxdb")
INFLUXDB_PORT = int(os.environ.get("INFLUXDB_PORT", "8086"))
INFLUXDB_USERNAME = os.environ.get("INFLUXDB_USERNAME", "morphemic")
INFLUXDB_PASSWORD = os.environ.get("INFLUXDB_PASSWORD", "password")
INFLUXDB_DBNAME = os.environ.get("INFLUXDB_DBNAME", "morphemic")
INFLUXDB_ORG = os.environ.get("INFLUXDB_ORG", "morphemic")

MAX_PARALLEL_TASKS = int(os.environ.get("MAX_PARALLEL_TASKS", "3"))

DEFAULT_DELTA = dt.timedelta(seconds=int(os.getenv("DEFAULT_DELTA"))) if os.getenv("DEFAULT_DELTA") \
    else dt.timedelta(seconds=30)

LOGLEVEL = os.environ.get("LOGLEVEL", "DEBUG")
MOCK_INFLUX_CSV_PATH = os.environ.get("MOCK_INFLUX_CSV_PATH")