import datetime as dt
import logging
import tempfile
import traceback
from pathlib import Path
from typing import List, Tuple

import requests
from cdoAdapter import CDOAdapter
from fastapi import FastAPI
from fastapi import Response, status, BackgroundTasks
from lxml import etree

from DataGenerator.models import CPSolution
from DataGenerator.solution_cache import CPQuery, SolutionCache
from DataGeneratorService.config import INFLUXDB_HOSTNAME, INFLUXDB_PORT, INFLUXDB_USERNAME, INFLUXDB_PASSWORD, \
    INFLUXDB_DBNAME, MAX_PARALLEL_TASKS, LOGLEVEL, MOCK_INFLUX_CSV_PATH
from DataGeneratorService.cptask_cache import CPTaskCache
from DataGeneratorService.models import DataGenerationInfo, DataGenerationResult, JobResult, CPSolverNotification
from DataGeneratorService.tasks import collect, run_task
from DataReading.aggregator import _timestamp_generator
from DataReading.csv_reader import CSVReader
from DataReading.influx_data_provider import InfluxDataProvider
from DataReading.interpolator import InterpolatedDataStream

task_cache = CPTaskCache()
solution_cache = SolutionCache()

app = FastAPI()

PREDICTION_PREFIX = 'prediction_'

logger = logging.getLogger()
logger.setLevel(level=LOGLEVEL)
logger.addHandler(logging.StreamHandler())


async def generate_data(data_generation_info: DataGenerationInfo) -> None:
    """Retrieves data from InfluxDB, interpolates it and splits to
    separate queries, sent sequentially to the cpsolver.

    Args:
        data_generation_info: DataModel containing all information needed to generate data.
    """
    logger.info(f'Received data generation job for appid {data_generation_info.applicationId}')
    logger.debug(f'DATA GENERATION INFO {data_generation_info}')
    try:
        with tempfile.TemporaryDirectory() as tempdir:
            configs = {
                'hostname': INFLUXDB_HOSTNAME,
                'port': INFLUXDB_PORT,
                'username': INFLUXDB_USERNAME,
                'password': INFLUXDB_PASSWORD,
                'dbname': INFLUXDB_DBNAME,
                'path_dataset': tempdir

            }
            if MOCK_INFLUX_CSV_PATH is not None:
                data_provider = CSVReader(MOCK_INFLUX_CSV_PATH)
            else:
                data_provider = InfluxDataProvider(applicationId=data_generation_info.applicationId,
                                                   startTimestamp=data_generation_info.startTimestamp,
                                                   finishTimestamp=data_generation_info.finishTimestamp,
                                                   influxConfigs=configs)

            # data interpolation
            interpolated_data = InterpolatedDataStream(data_provider,
                                                       _timestamp_generator(data_provider.peek_t0(),
                                                                            data_generation_info.delta),
                                                       data_provider.column_names)

            queries: List[Tuple[CPQuery, dt.datetime]] = []
            it = 0
            for row in interpolated_data:
                metrics = {data_generation_info.metricsInfluxMapping[key]: str(value)
                           for key, value in row.items() if key in data_generation_info.metricsInfluxMapping}
                actual_configuration = {data_generation_info.configurationInfluxMapping[key]: str(value)
                                        for key, value in row.items() if key
                                        in data_generation_info.configurationInfluxMapping}
                predictions = {}
                for key, value in row.items():
                    if key.startswith(PREDICTION_PREFIX):
                        suffix = key[len(PREDICTION_PREFIX):]
                        if suffix in data_generation_info.metricsInfluxMapping:
                            predictions[
                                f'{PREDICTION_PREFIX}{data_generation_info.metricsInfluxMapping[suffix]}'] = value

                timestamp = row[data_provider.timestamp_column_name]
                query = CPQuery(applicationId=data_generation_info.applicationId,
                                parameters=metrics,
                                configuration=actual_configuration,
                                it=it)
                it += 1
                queries.append((query, timestamp))

            job_info = task_cache.create_job(jobAmount=len(queries),
                                             data_generation_info=data_generation_info)

            for query, timestamp in queries:
                task_cache.create_task(jobId=job_info.jobId,
                                       query=query,
                                       timestamp=timestamp,
                                       predictions=predictions)

            run_task(job_info, MAX_PARALLEL_TASKS)
    except Exception as exc:
        try:
            logger.error(
                f'Error occurred during preparation of data generation {exc}.'
                f'Replying to {data_generation_info.returnEndpoint}')
            logger.debug(traceback.print_exc())
            requests.post(data_generation_info.returnEndpoint,
                          DataGenerationResult(dataGenerationInfo=data_generation_info, result=JobResult.error,
                                               error_msg=str(exc)).json().encode('ascii'))
        except Exception as exc:
            logger.error(
                f'Unable to send fail message to the return endpoint {data_generation_info.returnEndpoint}: {exc}')


@app.post('/generate',
          responses={
              "202": {
                  "description": "Data generation scheduled"
              },
              "422": {
                  "description": "Validation Error",
                  "content": {
                      "application/json": {
                          "schema": {
                              "$ref": "#/components/schemas/HTTPValidationError"
                          }
                      }
                  }
              }
          })
async def gen_data(data_generation_info: DataGenerationInfo, background_tasks: BackgroundTasks):
    background_tasks.add_task(generate_data, data_generation_info)
    return Response(status_code=status.HTTP_202_ACCEPTED)


@app.post('/ack/{taskId}')
async def ack_notify(taskId: str, notification: CPSolverNotification):
    task_info = task_cache.retrieve_task(taskId=taskId)
    job_info = task_cache.retrieve_job(task_info.jobId)

    with tempfile.TemporaryDirectory() as tempdir:
        cpproblem_solution_path = Path(tempdir.name).joinpath('cpproblem_solution.xmi')
        cpproblem_path = Path(tempdir.name).joinpath('cpproblem.xmi')
        # Retrieve solution and problem
        if not CDOAdapter.exportResourceToFile(job_info.dataGenerationInfo.cdoProblemPath, cpproblem_path):
            raise KeyError(f"Problem not found in cdo: {cpproblem_path}")

        if not CDOAdapter.exportResourceToFile(notification.cdoResourcePath, cpproblem_solution_path):
            raise KeyError(f"Solution not found in cdo: {cpproblem_solution_path}")

        # Get cpvariable names
        problem_xml = etree.parse(str(cpproblem_path))
        cpvariables = []
        for cpvar in problem_xml.findall('cpVariables'):
            cpvariables.append(cpvar.attrib['id'])

        # Read solution
        solution_str = {}
        solution_xml = etree.parse(str(cpproblem_solution_path)).find("solution")
        for variable_name, variable_value in zip(cpvariables, solution_xml.iterfind('variableValue')):
            solution_str[variable_name] = variable_value.find('value').attrib['value']

        # Update job status
        cpsolution = CPSolution(applicationId=notification.applicationId,
                                variables=solution_str,
                                configuration={})
        task_cache.add_solution(taskId, solution=cpsolution)

        if task_cache.update_job(job_info.jobId):
            collect(job_info)
        else:
            run_task(job_info)
