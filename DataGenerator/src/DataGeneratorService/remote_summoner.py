import datetime as dt
import itertools
import logging
import tempfile
import uuid
from pathlib import Path

import requests
from cdoAdapter import CDOAdapter
from lxml import etree

from DataGenerator.config import CPSOLVER_URL, WATERMARK_USER, WATERMARK_SYSTEM, CPSOLVER_TIMEOUT, CPSOLVER_PORT
from DataGenerator.solution_cache import CPQuery, SolutionCache
from DataGenerator.summoners.solv_summoner import CPSolverSolutionSummonerError
from DataGeneratorService.config import HOSTNAME
from DataGeneratorService.models import CPSolverRequest, CPSolverWatermark

logger = logging.getLogger(__name__)


class RemoteCPSolverSolutionSummoner:
    """Responsible for connecting with cpsolver,
    and generating solved constraint problem solutions,
    based on provided parameters.
    """

    _cache: SolutionCache

    def __init__(self,
                 solution_cache: SolutionCache):
        """
        Args:
            solution_cache: SolutionCache object.
        """

        self._cache = solution_cache

    def ask_solver(self,
                   applicationId: str,
                   cdo_path: str,
                   query: CPQuery,
                   task_id: str) -> None:
        """Asks solver for the solution.

        Args:
            applicationId: needed for the cpsolver to access appropriate problem data.
            cdo_path: path to the template cpproblem.
            query: problem query.
            task_id: related task id, saved in db.
        """

        # Retrieve cpproblem from cdo and modify it with custom problem data
        with tempfile.TemporaryDirectory() as cpproblem_template_tempdir:
            cpproblem_template_path = Path(cpproblem_template_tempdir).joinpath('cpproblem_template.xmi')
            logger.info(f'Retrieving {cdo_path} to {cpproblem_template_path}')
            if not CDOAdapter.exportResourceToFile(cdo_path, cpproblem_template_path):
                raise KeyError(f"Problem not found in cdo: {cpproblem_template_path}")

            cpproblem_tempfile = self._create_problem(cpproblem_template_path, query)
            cpproblem_template_path.unlink()

        problem_local_path = Path(cpproblem_tempfile.name)
        problem_cdo_path = f'generator_tmp_cpproblem/{task_id}'
        # Upload task
        logger.info(f'Uploading {cpproblem_template_path} to {problem_cdo_path}')
        if not CDOAdapter.importResourceFromFile(problem_local_path, problem_cdo_path):
            raise KeyError(f"Unable to send problem to cdo: {problem_cdo_path}")

        cpproblem_tempfile.close()
        # Prepare package for cpsolver
        cpwatermark = CPSolverWatermark(
            uuid=str(uuid.uuid4()),
            date=dt.datetime.now(),
            user=WATERMARK_USER,
            system=WATERMARK_SYSTEM
        )

        cprequest = CPSolverRequest(
            applicationId=applicationId,
            cdoModelsPath=problem_cdo_path,
            notificationURI=f'http://{HOSTNAME}/ack/{task_id}',
            watermark=cpwatermark
        )
        # Upload to cpsolver
        logger.info(f'Sending request to the CPSolver: {cprequest.json()}')
        response = requests.post(url=f'http://{CPSOLVER_URL}:{CPSOLVER_PORT}/constraintProblemSolution',
                                 data=cprequest.json(),
                                 timeout=CPSOLVER_TIMEOUT,
                                 headers={'Content-Type': 'application/json'}
                                 )
        if response.status_code != 200:
            raise CPSolverSolutionSummonerError("cp-solver returned non 200 status code")

    @staticmethod
    def _create_problem(cpproblem_template_path: Path,
                        query: CPQuery) -> tempfile.NamedTemporaryFile:
        """Based on cpproblem file, generates unique cpproblem provided data from query.

        Args:
            cpproblem_template_path: path to the cpproblem template file.
            query: query with problem data.

        Returns:
            A temporary file, containing information about cpproblem in query.
        """

        cpproblem_xml = etree.parse(str(cpproblem_template_path))
        for name, value in itertools.chain(query.parameters.items(), query.configuration.items()):
            cpproblem_xml.find(f"cpMetrics[@id='{name}']")[0].set('value', str(value))

        output_problem = tempfile.NamedTemporaryFile()
        logger.debug(f'Creating CPProblem {output_problem.name} with query: {query}')

        cpproblem_xml.write(output_problem.name, xml_declaration=True, encoding="ASCII")

        return output_problem
