
# OBSOLETE CODE
# Used only in 1st phase of manual data generation

import abc
import csv
import datetime as dt
import io
from typing import Callable, Optional

import pytest

from DataReading.abstractreader import IRowDataProvider


class RawDataProviderTestTemplate(abc.ABC):
    @abc.abstractmethod
    @pytest.fixture
    def reader_factory(self) -> Callable[[str, Optional[str]], IRowDataProvider]:
        pass

    def test_import_basic(self, reader_factory):
        header = ['time', 'int', 'str', 'float', 'bool']
        content = []
        for i in range(5):
            row = [dt.datetime.now() + dt.timedelta(days=i), i, str(i) + 'pln', i / 10, i % 2 == 0]
            assert len(row) == len(header)
            content.append(row)

        s = io.StringIO()
        csv.writer(s, delimiter=',').writerows([header] + content)

        s.seek(0)
        # unknown timestamp column
        with pytest.raises(AssertionError) as exc:
            reader = reader_factory(s.read(), max_time_difference=dt.timedelta(days=1))

        assert 'Cannot specify timestamp column' in exc.value.args[0]

        reader_factory(s.read(), timestamp_column_name='time', max_time_difference=dt.timedelta(days=1))

    def test_time_desc(self, reader_factory):
        header = ['time', 'int', 'str', 'float', 'bool']
        content = []
        for i in range(5):
            row = [dt.datetime.now() - dt.timedelta(days=i), i, str(i) + 'pln', i / 10, i % 2 == 0]
            assert len(row) == len(header)
            content.append(row)

        s = io.StringIO()
        csv.writer(s, delimiter=',').writerows([header] + content)

        s.seek(0)
        # unknown timestamp column
        with pytest.raises(AssertionError) as exc:
            reader = reader_factory(s.read(), timestamp_column_name='time', max_time_difference=dt.timedelta(days=1))

        assert 'Timestamps in column are not increasing' in exc.value.args[0]

    def test_time_eq(self, reader_factory):
        header = ['time', 'int', 'str', 'float', 'bool']
        content = []
        t = dt.datetime.now()
        for i in range(5):
            row = [t, i, str(i) + 'pln', i / 10, i % 2 == 0]
            assert len(row) == len(header)
            content.append(row)

        s = io.StringIO()
        csv.writer(s, delimiter=',').writerows([header] + content)

        s.seek(0)
        # unknown timestamp column
        with pytest.raises(AssertionError) as exc:
            reader = reader_factory(s.read(), timestamp_column_name='time', max_time_difference=dt.timedelta(days=1))

        assert 'Found >=2 equal timestamps' in exc.value.args[0]

    def test_column_getters(self, reader_factory):
        header = ['time', 'int', 'str', 'float', 'bool']
        content = []
        for i in range(5):
            row = [dt.datetime.now() + dt.timedelta(days=i), i, str(i) + 'pln', i / 10, i % 2 == 0]
            assert len(row) == len(header)
            content.append(row)

        s = io.StringIO()
        csv.writer(s, delimiter=',').writerows([header] + content)

        s.seek(0)

        reader: IRowDataProvider = reader_factory(s.read(), timestamp_column_name='time', max_time_difference=dt.timedelta(days=1))

        assert len(reader.column_names) == len(header)
        for reader_col, header_col in zip(reader.column_names, header):
            assert reader_col == header_col

        col_types = reader.columns

        # check types
        assert col_types['time'] == dt.datetime
        for i in range(1, len(header)):
            col = header[i]
            assert col_types[col].__name__ == col

    def test_read(self, reader_factory):
        header = ['time', 'int', 'str', 'float', 'bool']
        content = []
        t0 = dt.datetime.now()
        for i in range(5):
            row = [t0 + dt.timedelta(days=i), i, str(i) + 'pln', i / 10, i % 2 == 0]
            assert len(row) == len(header)
            content.append(row)

        s = io.StringIO()
        csv.writer(s, delimiter=',').writerows([header] + content)

        s.seek(0)

        reader: IRowDataProvider = reader_factory(s.read(), timestamp_column_name='time', max_time_difference=dt.timedelta(days=1))

        row_gen = reader.row_generator()
        for row, grow in zip(content, row_gen):
            assert row == grow

        dict_gen = reader.row_generator_annotated()
        for row, row_dict in zip(content, dict_gen):
            for col_name, value in zip(header, row):
                assert row_dict[col_name] == value
