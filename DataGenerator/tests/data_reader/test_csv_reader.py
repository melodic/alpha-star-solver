import tempfile
from typing import Callable

import pytest
import datetime as dt

from DataReading.csv_reader import CSVReader
from .data_reader_template import RawDataProviderTestTemplate


class TestCSVReader(RawDataProviderTestTemplate):

    @pytest.fixture
    def reader_factory(self) -> Callable[[str], CSVReader]:
        tfile = tempfile.NamedTemporaryFile(suffix='.csv', mode='w')
        path = tfile.name

        def _write(s, *args, **kwargs):
            tfile.write(s)
            tfile.flush()
            return RowCSVReader(path, *args, **kwargs)

        yield _write
