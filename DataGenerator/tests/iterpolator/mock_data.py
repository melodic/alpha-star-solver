import datetime as dt
from typing import TypedDict, List, Any, Iterator, Tuple, Callable


class MockData(TypedDict):
    '''
        Dict representing mock data
        for IRowDataProvider.

        data is List of rows.
        Every row should contain timestamp in form
        of dt.datetime
    '''
    headers: List[str]
    types: List[type]
    ts_col_name: str
    data: List[Tuple[Any]]


def mock_data_factory(
            header_names: List[str],
            ts_col_name: str,
            col_types: List[type],
            col_gens: List[Iterator[Any]]
        ):
    '''
        generates mock data dict based on
        data headers, timestamp column name,
        column types and column generators
    '''

    return MockData(
        headers=header_names,
        types=col_types,
        ts_col_name=ts_col_name,
        data=[row for row in zip(*col_gens)]
    )


def lambda_generator(lamb: Callable[[int], Any], iters: int) -> Iterator[Any]:
    '''
        simple wrapper for generator.
        if iters eq -1, generate sequnce is infinite
    '''

    i = 0
    if iters == -1:
        iters = float('inf')

    while i < iters:
        yield lamb(i)
        i += 1
