import datetime as dt
from typing import Tuple, Dict, Generator, Iterable, List

from DataReading.abstractreader import IRowDataProvider
from .mock_data import MockData


class MockRowDataProvider(IRowDataProvider):
    '''
        Mock IRowDataProvider implementation
        for testing purposes. Returns data
        based on given MockData
    '''
    def __init__(self,
                 data: MockData,
                 max_time_difference: dt.timedelta):
        super().__init__(timestamp_column_name=data['ts_col_name'],
                         max_time_difference=max_time_difference)
        self.data = data['data']
        self.headers = data['headers']
        self.ts_col_name = data['ts_col_name']
        self.types = data['types']

    @property
    def timestamp_column_name(self) -> str:
        return self.ts_col_name

    def peek_t0(self) -> dt.datetime:
        ts_col_idx = self.headers.index(self.ts_col_name)
        return self.data[0][ts_col_idx]

    @property
    def column_names(self) -> List[str]:
        return self.headers

    @property
    def columns(self) -> Dict[str, type]:
        return {cname: ctype for cname, ctype in zip(self.headers, self.types)}

    def row_generator_annotated(self) -> Generator[Dict[str, any], None, None]:
        yield from map(lambda row: dict(zip(self.headers, row)), self.data)

    def row_generator(self) -> Generator[List[type], None, None]:
        yield from self.data
