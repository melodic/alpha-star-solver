import pytest
import datetime as dt
import random
from typing import Dict, Any
from numbers import Number

from DataReading.interpolator import InterpolatedDataStream
from .mock_row_provider import MockRowDataProvider
from .mock_data import mock_data_factory, lambda_generator as lg


def dict_eq_w_margin(d1: Dict[str, Any], d2: Dict[str, Any], margin: float) -> bool:
    '''
        checks if both dict are equal with margin.
        Meaning that both dicts have same keys with same values
        but Numerical values can differ no more then given margin
    '''
    if d1.keys() != d2.keys():
        return False
    
    for key in d1:
        if isinstance(d1[key], Number) and isinstance(d2[key], Number):
            if abs(d1[key] - d2[key]) > margin:
                return False
        else:
            if d1[key] != d2[key]:
                return False

    return True


@pytest.mark.interpolation
def test_same_vals_at_interpolation_nodes():
    '''
        checks if values at interpolation nodes
        are still same (with small margin)
    '''

    MAX_ERR = 1e-4
    ITERS = 10
    t0 = dt.datetime(year=2000, month=1, day=1)

    data = mock_data_factory(
        header_names=['ts', 'rand1', 'rand2', 'rand3'],
        ts_col_name='ts',
        col_types=[dt.datetime, float, float, float],
        col_gens=[
            lg(lambda n: t0 + dt.timedelta(minutes=n), ITERS),
            lg(lambda n: -random.random(), ITERS),
            lg(lambda n: random.random() * 100, ITERS),
            lg(lambda n: random.random() * 1000, ITERS),
        ]
    )

    rowProvider = MockRowDataProvider(
        data=data,
        max_time_difference=dt.timedelta(days=1)
    )
    interp = InterpolatedDataStream(
        reader=rowProvider,
        timestamp_generator=lg(lambda n: t0 + dt.timedelta(minutes=n), ITERS * 2),
    )

    rowProvGen = rowProvider.row_generator_annotated()
    for d1, d2 in zip(interp, rowProvGen):
        assert dict_eq_w_margin(d1, d2, MAX_ERR)


@pytest.mark.interpolation
def test_no_data_but_timestamps():
    '''
        checks if interpolator raises
        correct exception when rowProvider returns
        no rows
    '''
    data = mock_data_factory(
        header_names=['ts', 'v1', 'v2'],
        ts_col_name='ts',
        col_types=[dt.datetime, float],
        col_gens=[range(0), range(0)]
    )

    rowProv = MockRowDataProvider(
        data=data,
        max_time_difference=dt.timedelta(seconds=1)
    )

    t0 = dt.datetime(year=2000, month=1, day=1)
    def ts_gen():
        i = 0
        while True:
            yield t0 + dt.timedelta(seconds=i)
    with pytest.raises(EmptyReaderExcpetion):
        interp = InterpolatedDataStream(reader=rowProv, timestamp_generator=ts_gen())
        

@pytest.mark.interpolation
def test_only_none_data():
    '''
        checks if interpolator raises
        correct exception when rowProvider returns
        only None values and correct timestamps
    '''
    ITERS = 29
    t0 = dt.datetime(year=2000, month=1, day=1)
    data = mock_data_factory(
        header_names=['time stamp', 'i1', 'c2'],
        ts_col_name='time stamp',
        col_types=[dt.datetime, int, complex],
        col_gens=[
            lg(lambda n: t0 + dt.timedelta(hours=n), ITERS),
            lg(lambda n: None, ITERS),
            lg(lambda n: None, ITERS)
        ]
    )

    t0 = dt.datetime(year=2000, month=1, day=1)
    rowProv = MockRowDataProvider(data, dt.timedelta(seconds=1))
    with pytest.raises(EmptyReaderExcpetion):
        interp = InterpolatedDataStream(
            reader=rowProv,
            timestamp_generator=lg(lambda n: t0 + dt.timedelta(days=2*n), -1)
        )

@pytest.mark.interpolation
def test_some_col_empty():
    '''
        checks if correct exception is raised
        when some cols are full of data and 
        some are empty
    '''

    ITERS = 10
    t0 = dt.datetime(year=2010, month=3, day=3)
    data = mock_data_factory(
        header_names=['ts', 'full', 'empty'],
        ts_col_name='ts',
        col_types=[dt.datetime, int, complex],
        col_gens=[
            lg(lambda n: t0 + dt.timedelta(hours=n), ITERS),
            lg(lambda n: random.random() * 100, ITERS),
            lg(lambda n: None, ITERS)
        ]
    )

    rowProv = MockRowDataProvider(data, dt.timedelta(seconds=1))
    with pytest.raises(EmptyReaderExcpetion):
        interp = InterpolatedDataStream(
            reader=rowProv,
            timestamp_generator=lg(lambda n: t0 + dt.timedelta(seconds=n), -1)
        )


@pytest.mark.interpolation
def test_sparse_col_extrapolation():
    '''
        checks if correct exception is raised
        when some cols are full of data and 
        some are sparse and are extrapolated
        out of their bounds
    '''

    ITERS = 10
    t0 = dt.datetime(year=2010, month=3, day=3)
    data = mock_data_factory(
        header_names=['ts', 'full', 'rare'],
        ts_col_name='ts',
        col_types=[dt.datetime, int, float],
        col_gens=[
            lg(lambda n: t0 + dt.timedelta(minutes=n), ITERS),
            lg(lambda n: random.random() * 10 * n, ITERS),
            lg(lambda n: {0:1, 1:10, 2:2, 3:2}[n] if n <= 3 else None, ITERS)
        ]
    )

    rowProv = MockRowDataProvider(data, dt.timedelta(minutes=1))
    interp = InterpolatedDataStream(
        reader=rowProv,
        timestamp_generator=lg(lambda n: t0 + dt.timedelta(minutes=n), -1)
    )

    const_start = t0 + dt.timedelta(minutes=3)

    MAX_ERR = 1e-2
    n_row = 0
    for rowdict in interp:
        if rowdict['ts'] > const_start:
            assert abs(rowdict['rare'] - 2) <= MAX_ERR
        n_row += 1
    assert n_row == ITERS


'''
    TODO:
        test if data taken not from sample node is somewhat ok

        test what happens on larger dataset > 5000 sample nodes
'''
