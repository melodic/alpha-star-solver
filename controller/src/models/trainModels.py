import datetime as dt
from enum import Enum
from typing import Optional, Dict

from pydantic import validator
from pydantic.main import BaseModel


class TrainInfo(BaseModel):
    """
    application_id: unique id of the application
    cp_cdo_path: path to the constraint problem in CDO
    camel_model_cdo_path: path to the camel model in CDO
    proc_finished_url: URL at which a notification will be sent after training has finished
    max_utility_error: maximal MAE error of the predicted configurations to deem a trained network successful
    force_retrain: whether to force retraining of the model. Forcing retraining when another training is in progress may
                   lead to undefined behaviour.
    """
    application_id: str
    cp_cdo_path: str
    camel_model_cdo_path: str
    proc_finished_url: str
    max_utility_error: float
    force_retrain: Optional[bool] = False


class TrainFromFileInfo(BaseModel):
    """
    application_id: unique id of the application
    cp_file_path: path to the file containing constraint problem
    camel_model_file_path: path to the file containing camel model
    node_candidates_file_path: path to the file containing node candidates
    proc_finished_url: URL at which a notification will be sent after training has finished
    max_utility_error: maximal MAE error of the predicted configurations to deem a trained network successful
    force_retrain: whether to force retraining of the model. Forcing retraining when another training is in progress may
                   lead to undefined behaviour.
    """
    application_id: str
    cp_file_path: str
    camel_model_file_path: str
    node_candidates_file_path: str
    proc_finished_url: str
    max_utility_error: float
    force_retrain: Optional[bool] = False

class JobResult(Enum):
    """Possible data generation job results"""
    ok = "ok"
    error = "error"

class DataGenerationInfo(BaseModel):
    """Data generation job related information."""
    applicationId: str
    trainRatio: float  # [0;1] range
    cdoProblemPath: str
    returnEndpoint: str
    startTimestamp: Optional[dt.datetime]
    finishTimestamp: Optional[dt.datetime]
    delta: dt.timedelta = 30
    metricsInfluxMapping: Dict[str, str] = {}
    configurationInfluxMapping: Dict[str, str] = {}

    @validator('trainRatio')
    def ratio_in_range(cls, v: float):
        """Validates if test ratio is in 0-1 range"""
        if not (0 <= v <= 1):
            raise ValueError(f'Train ratio not in [0;1] range: {v}')
        return v

class DataGenerationResult(BaseModel):
    """Data generation job status"""
    dataGenerationInfo: DataGenerationInfo
    result: JobResult = JobResult.ok
    error_msg: Optional[str] = None


class NetworkTrainedInfo(BaseModel):
    """
    application_id: unique id of the application
    status: status of network training - either successful or failed
    """
    application_id: str
    status: str
