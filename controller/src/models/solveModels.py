from datetime import datetime

from pydantic import BaseModel


class SolveInfo(BaseModel):
    """
    application_id: unique id of the application
    problem_datetime: time for which the prediction will be calculated
    cp_CDO_path: path to the constraint problem in CDO
    notification_uri: uri of the ESB at which a notification will be sent
    request_uuid: request uuid which will be passed in the notification to ESB
    """
    application_id: str
    problem_datetime: datetime
    cp_CDO_path: str
    notification_uri: str
    request_uuid: str


class SolveFromFileInfo(BaseModel):
    """
    application_id: unique id of the application
    input_file_path: path to the file containing the input for supervised gym
    cp_CDO_path: path to the constraint problem in CDO
    notification_uri: uri of the ESB at which a notification will be sent
    request_uuid: request uuid which will be passed in the notification to ESB
    """
    application_id: str
    input_file_path: str
    cp_CDO_path: str
    notification_uri: str
    request_uuid: str
