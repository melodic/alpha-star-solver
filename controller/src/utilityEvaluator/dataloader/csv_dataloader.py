import logging
from pathlib import Path
from typing import List

import numpy as np
import torch


class Dataloader:
    """
        Based on a .csv file, it maps a sequence of metrics and predictions
        gathered at consecutive timestamps to an ideal configuration and enables
        getting those (metrics, configuration) pairs by their index.
        Both the sequence length and the prediction step are passed as parameters.

        The .csv file can be divided into 3 parts:
        - X part - columns containing metrics and predictions, which will be the input to neural networks
        - Y part - columns containing the ideal configuration, which will be the desired output of neural networks
        - Experiment Id column - column defining the experiment to which a given row corresponds to.

        All the rows with the same experiment id must be grouped together.
        The timestamp difference between continuous rows in a given one experiment should always be the same.
        The difference in time or amount of rows in different experiments is not relevant.
    """

    def __init__(self,
                 seq_len: int,
                 pred_step: int,
                 file: Path,
                 usecols: List[int],
                 experiment_id_col: int,
                 x_y_split: int,
                 x_predictions_cols: List[int],
                 delimiter=',',
                 skip_header=1
                 ):
        """
        Args:
            seq_len: number of following records returned as a sequence.
            pred_step: distance between last record in sequence and correctly predicted row.
            file: path to the .csv file containing the necessary data
            usecols: ids of columns to be read from csv (it must include experiment_id_col). Other columns will be dismissed.
            experiment_id_col: id of the column (after dismissing unused columns) defining the experiment
                to which a given row corresponds to. This column will be dismissed.
                All the rows with the same experiment id must be grouped together.
            x_y_split: id of column (after dismissing unused columns) which is the border
                between metrics and predictions (x) and desired configuration (y).
                In cols [0, x_y_split-1] are the values describing the input to a network.
                In cols [x_y_split, end-1] are the values describing the desired output from a network.
            x_predictions_cols: ids of columns (after dismissing unused columns) that contain some prediction value.
                They will be pushed by pred_step rows.
            delimiter: delimiter between values in .csv file
            skip_header: number of rows to skip at the beggining of the .csv file
        """

        if seq_len < 0:
            raise ValueError(f"seq_len can't be negative")

        if pred_step < 0:
            raise ValueError(f"pred_step can't be negative")

        self.__file = file
        self.__seq_len = seq_len
        self.__x_y_split = x_y_split
        self.__usecols = usecols
        self.__experiment_id_col = experiment_id_col
        self.__x_predictions_cols = x_predictions_cols
        self.__delimiter = delimiter
        self.__skip_header = skip_header

        self.__preproces_data(seq_len, pred_step)

    def get_size(self) -> int:
        """ Returns the size of the dataset.
        Returns:
            the size of the dataset
        """
        return self.__size

    def __load_series(self, file: Path) -> np.array:
        """Loads data from the .csv file and splits it so that each experiment is separated from others.
        
        Args:
            file: path to the file containing the data
        
        Returns:
            array of data where each element describes data for different experiment
        """
        series = np.genfromtxt(
            file.as_posix(),
            delimiter=self.__delimiter,
            skip_header=self.__skip_header,
            usecols=self.__usecols,
            dtype=np.float32
        )
        split_idxes = []  # rows at which a new experiment begins
        current_experiment_id = series[0][self.__experiment_id_col]
        for i in range(1, series.shape[0]):
            if series[i][self.__experiment_id_col] != current_experiment_id:
                split_idxes.append(i)
                current_experiment_id = series[i][self.__experiment_id_col]

        series = np.delete(series, self.__experiment_id_col, 1)  # deleting the column with experiment_id
        series = np.vsplit(series, split_idxes)  # spliting the data into experiments
        return series

    def __preproces_data(self,
                         seq_len: int,
                         pred_step: int) -> None:
        """Prepares the data.
        Firstly loads and splits the data into experiments.
        Then, within each experiment, it is pushing/cutting rows according to seq_len and pred_step parameters.
        Lastly, it calculates idx_experiment_map and idx_experiment_idx_map so that each pair of (sequence, configuration)
        can be accessed using a numerical index (in the method __getitem(idx)).
        
        Args:
            seq_len: length of the sequence required by the supervised gym's model
            pred_step: how many data rows in the future the model is predicting for
        """
        series = self.__load_series(self.__file)
        sizes = np.zeros(len(series), dtype=np.int32)
        self.__x = [None] * len(series)
        self.__y = [None] * len(series)

        i = 0
        while i < len(series):
            x, y = np.hsplit(series[i], [self.__x_y_split])
            x_cols = np.hsplit(x, x.shape[1])

            x_cols = self.__push_cols(x_cols, self.__x_predictions_cols, pred_step)
            x = np.hstack(x_cols)
            y = y[seq_len + pred_step - 1:]  # make it fit with x sequences

            self.__x[i] = x
            self.__y[i] = torch.from_numpy(y)
            sizes[i] = y.shape[0]

            if sizes[i] <= 0:
                sizes = np.delete(sizes, i)
                series = np.delete(series, i, axis=0)
                self.__x.pop(i)
                self.__y.pop(i)
                i -= 1

            i += 1
        if len(sizes) == 0:
            self.__size = 0
            logging.warning("Cannot produce even a single sequence out of the provided data file. " +
                            "Either dataset is empty or the seq_len and prediction horizon are to big while number of rows in each group is too small.")
            return

        cumsizes = np.cumsum(sizes, dtype=np.int32)
        self.__size = cumsizes[-1].item()
        self.__idx_experiment_map = np.searchsorted(cumsizes, np.arange(self.__size) + 1).astype(np.int32)
        self.__idx_experiment_idx_map = np.zeros(self.__size).astype(np.int32)

        self.__idx_experiment_idx_map[0] = 0
        cnt = 1
        for i in range(1, len(self.__idx_experiment_map)):
            if self.__idx_experiment_map[i] != self.__idx_experiment_map[i - 1]:
                cnt = 0
            self.__idx_experiment_idx_map[i] = cnt
            cnt += 1

    def __push_cols(self,
                    cols: np.array,
                    push_idx: List[int],
                    d: int) -> np.array:
        """Pushes cols of indexes in push_idx by value d, while other cols get cut by value d

        Args:
            cols: columns of data to be modified
            push_idx: indexes of columns that need to be pushed
            d: the amount of rows to be pushed

        Returns:
            modified columns of data
        """

        if d == 0:
            return cols

        new_cols = [None] * len(cols)

        for i, _ in enumerate(cols):
            if i in push_idx:
                new_cols[i] = cols[i][d:]
            else:
                new_cols[i] = cols[i][:-d]

        return new_cols

    def getitem(self, idx: int) -> torch.tensor:
        """Gets the data sequence of the specified index

        Args:
            idx: index of data sequence to be extracted

        Returns:
            tuple (seq, conf) where:
            - seq is the sequence of metrics and predictions (input to neural networks)
            - conf is the ideal configuration based on the sequence (desired output from neural networks)
        """

        f = self.__idx_experiment_map[idx]
        f_idx = self.__idx_experiment_idx_map[idx]
        x = self.__x[f][f_idx:f_idx + self.__seq_len]
        y = self.__y[f][f_idx]

        return (x, y)
