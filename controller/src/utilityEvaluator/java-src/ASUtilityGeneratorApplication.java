package eu.melodic.upperware.utilitygenerator;

import eu.melodic.cache.NodeCandidates;
import eu.melodic.cache.impl.FilecacheService;
import eu.melodic.upperware.penaltycalculator.PenaltyFunctionProperties;
import eu.melodic.upperware.utilitygenerator.cdo.cp_model.DTO.VariableValueDTO;
import eu.melodic.upperware.utilitygenerator.properties.UtilityGeneratorProperties;
import eu.paasage.upperware.security.authapi.properties.MelodicSecurityProperties;
import eu.paasage.upperware.security.authapi.token.JWTServiceImpl;

import java.util.Collection;

public class ASUtilityGeneratorApplication{
    private UtilityGeneratorApplication utilityGeneratorApplication;

    public ASUtilityGeneratorApplication(String camelModelFilePath, String cpModelFilePath, String NODE_CANDIDATES_FILE_PATH){
        utilityGeneratorApplication = createUtilityGeneratorApplication(camelModelFilePath, cpModelFilePath, NODE_CANDIDATES_FILE_PATH);
    }

    public double evaluate(Collection<VariableValueDTO> solution) {
        return this.utilityGeneratorApplication.evaluate(solution);
    }

    private static UtilityGeneratorApplication createUtilityGeneratorApplication(String camelModelFilePath, String cpModelFilePath, String NODE_CANDIDATES_FILE_PATH) {
        boolean readFromFile = true;
        NodeCandidates nodeCandidates = new FilecacheService().load(NODE_CANDIDATES_FILE_PATH);
        UtilityGeneratorProperties utilityGeneratorProperties = new UtilityGeneratorProperties();
        utilityGeneratorProperties.setUtilityGenerator(new UtilityGeneratorProperties.UtilityGenerator());
        utilityGeneratorProperties.getUtilityGenerator().setDlmsControllerUrl("");
        MelodicSecurityProperties melodicSecurityProperties = new MelodicSecurityProperties();
        JWTServiceImpl jWTServiceImpl = new JWTServiceImpl(melodicSecurityProperties);
        PenaltyFunctionProperties penaltyFunctionProperties = new PenaltyFunctionProperties();
        return new UtilityGeneratorApplication(camelModelFilePath, cpModelFilePath, readFromFile, nodeCandidates, utilityGeneratorProperties, melodicSecurityProperties, jWTServiceImpl, penaltyFunctionProperties);
    }
}
