import logging
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import List, Dict
from xml.etree.ElementTree import ElementTree, Element

from jnius import autoclass
from lxml import etree

ASUtilityGeneratorApplication = autoclass("eu.melodic.upperware.utilitygenerator.ASUtilityGeneratorApplication")
IntVariableValueDTO = autoclass("eu.melodic.upperware.utilitygenerator.cdo.cp_model.DTO.IntVariableValueDTO")
ArrayList = autoclass("java.util.ArrayList")


class UtilityGenerator:
    """
        Class used for utilizing the "utility generator" java library to calculate the utility function value
        for given constraint problem and camel model.

        Requires java library with utility generator (and its dependencies) and ASUtilityGeneratorApplication from java-src.
        The .jar containing the library should be included in the classpath declared at the beginning of this file.
    """
    _cp_model: ElementTree
    _xml_fillings: Dict[str, Element]

    _cp_model_path: str
    _camel_model_path: str
    _node_candidates_path: str

    def __init__(self,
                 cp_model_path: str,
                 camel_model_path: str,
                 node_candidates_path: str,
                 metric_names: List[str]):
        """
        Args:
            cp_model_path: path to the file containing the constraint problem model
            camel_model_path: path to the file containing the camel model
            node_candidates_path: path to the file containing node candidates
            metric_names: names of the metrics that might be changed (exactly as they appear in the cp model file)
        """

        self._cp_model_path = Path(cp_model_path)
        self._cp_model = etree.parse(str(Path(self._cp_model_path)))
        self._camel_model_path = camel_model_path
        self._node_candidates_path = node_candidates_path

        self._xml_fillings = {}
        for name in metric_names:
            xml_filling = self._cp_model.find(
                f"cpMetrics[@id='{name}']")
            if xml_filling is not None:
                self._xml_fillings[name] = xml_filling[0]
            else:
                logging.warning(f"Received metric name which was not found in the CP: {name}. Ommiting it.")

    def _add_metrics(self,
                     filename: str,
                     metrics: Dict[str, int]) -> None:
        """Adds metrics to the constraint problem model.

        Args:
            filename: name of the file containing the constraint problem model
            metrics: dictionary with pairs (arg_name, arg_value) describing the metrics to be added. If metrics are
                     empty, then no value wil be changed
        """
        for arg_name, arg_value in metrics.items():
            if arg_name not in self._xml_fillings:
                logging.warning(f"Received metric name which was not found in the CP: {arg_name}. Ommiting it.")
                continue
            arg_loc = self._xml_fillings[arg_name]
            value_type = arg_loc.get(r'{http://www.w3.org/2001/XMLSchema-instance}type')
            if value_type == 'types:IntegerValueUpperware':
                arg_loc.set('value', str(int(arg_value)))
            else:
                arg_loc.set('value', str(arg_value))

        self._cp_model.write(filename,
                             xml_declaration=True,
                             encoding="ASCII")

    def evaluate(self,
                 configuration: Dict[str, int],
                 metrics: Dict[str, int]) -> float:
        """Creates java objects based on the parameters and a tmeporary file with an updated contraint problem model.
        Then it calculates the utility function value using the java's ASUtilityGeneratorApplication.

        Args:
            configuration: dictionary with pairs (arg_name, arg_value) describing the configuration
            metrics: dictionary with pairs (arg_name, arg_value) describing the metrics
        Returns:
            the utility function value for the given parameters
        """
        utility_value = 0.0
        with NamedTemporaryFile(delete=False) as tempfile:

            self._add_metrics(filename=tempfile.name, metrics=metrics)
            variable_list = ArrayList()
            for (name, value) in configuration.items():
                variable_list.add(IntVariableValueDTO(name, round(value)))

            utility_generator = ASUtilityGeneratorApplication(self._camel_model_path, tempfile.name,
                                                              self._node_candidates_path)
            utility_value = utility_generator.evaluate(variable_list)

        return utility_value
