import logging
import os
from numbers import Number
from pathlib import Path
from typing import List, Dict, Union, Tuple

import torch
from fastapi import HTTPException
from modeldb import ModelDBClient

from .UtilityGenerator import UtilityGenerator
from .dataloader.csv_dataloader import Dataloader
from ..models.trainModels import NetworkTrainedInfo
from ..solving_controller import SolvingController


class UtilityEvaluator:
    """
        Evaluator which uses UtilityGenerator to calculate mean absolute error between
        utility function values of predicted and target configurations.
    """
    
    __metrics_labels_and_positions: Dict[str, int]
    __target_labels: List[str]
    __utility_generator: UtilityGenerator
    
    def __init__(self,
                 metrics_labels_and_positions: Dict[str, int],
                 target_labels: List[str],
                 cp_model_file_path: str,
                 camel_model_file_path: str,
                 node_candidates_file_path: str):
        """
        Args:
            metrics_labels_and_positions: dictionary specifying the names of the metrics and their positions in the test file
            target_labels: names of the configuration columns in the test file
            cp_model_file_path: path to the file containing the constraint problem
            camel_model_file_path: path to the file containing the camel model
            node_candidates_file_path: path to the file containing the node candidates
        """

        self.__metrics_labels_and_positions = metrics_labels_and_positions
        self.__target_labels = target_labels
        self.__utility_generator = UtilityGenerator(cp_model_path=cp_model_file_path,
                                                     camel_model_path=camel_model_file_path,
                                                     node_candidates_path=node_candidates_file_path,
                                                     metric_names=list(metrics_labels_and_positions.keys()))

    def evaluateUtilityValueDifference(self,
                                       x: torch.Tensor,
                                       preds: torch.Tensor,
                                       target: torch.Tensor) -> float:
        """Calculates the difference in utility values of the given configurations

        Args:
            x: input tensor to the network
            preds: predicted configuration
            target: optimal configuration

        Returns:
            difference in utility values of the given configurations
        """
        assert preds.shape == target.shape, "Target and prediction tensors are of different shape."
        target_utility_value = self.__utility_generator.evaluate(configuration=self._get_configuration(target),
                                                                  metrics=self._get_metrics(x))
        preds_utility_value = self.__utility_generator.evaluate(configuration=self._get_configuration(preds),
                                                                 metrics=self._get_metrics(x))
        return abs(target_utility_value - preds_utility_value)

    def _get_metrics(self, x: torch.Tensor) -> Dict[str, int]:
        """Prepare a dictionary describing metrics based on the given tensor and self.__metrics_labels_and_positions

        Args:
            x: input tensor to the network

        Returns:
            dictionary with pairs (metric_name: metric_value)
        """
        last_row_of_x = x[-1]
        metrics: Dict[str, int] = {}
        for label, position in self.__metrics_labels_and_positions.items():
            metrics[label] = last_row_of_x[position].item()
        return metrics

    def _get_configuration(self, solve: torch.Tensor) -> Dict[str, int]:
        """Prepare a dictionary describing the configuration based on the given tensor and self.__target_labels

        Args:
            solve: tensor contatining the configuration

        Returns:
            dictionary with pairs (variable_name: variable_value)
        """
        configuration: Dict[str, int] = {}
        for position, label in enumerate(self.__target_labels):
            configuration[label] = solve[position].item()
        return configuration

    @staticmethod
    def _getCsvHeaders(test_data_file_path: Path) -> List[str]:
        """ Extract the column headers of the training data

        Args:
            test_data_file_path: path to the file containing the test data

        Returns:
            Column headers of the test data
        """
        csv_headers = []
        with test_data_file_path.open(mode='r') as test_data_file:
            header = test_data_file.readline().rstrip('\n')  # read only first line with column headers
            csv_headers = header.split(',')
        return csv_headers

    @staticmethod
    def _getInputDataFromTensor(seq: torch.tensor,
                                input_columns: List[str],
                                csv_headers: List[str]) -> Dict[str, List[Union[float, int]]]:
        """Transforms tensor data into input data for supervised gym to predict configuration

        Args:
            seq: sequence of metrics and predictions
            input_columns: names of the input columns
            csv_headers: column headers of the test data

        Returns:
            input data for supervised gym
        """
        input_data = {}
        for col in input_columns:
            input_data[col] = []
        for line in seq:
            for position in range(len(line)):
                col = csv_headers[position]
                value = (line[position].item())
                input_data[col].append(value)
        return input_data

    @staticmethod
    def _getPredictionAsTensor(prediction: Dict[str, Number],
                               target_labels: List[str],
                               input_columns: List[str],
                               csv_headers: List[str]) -> torch.tensor:
        """Transforms the prediction produced by the supervised gym to a tensor

        Args:
            prediction: configuration prediction produced by the supervised gym
            target_labels: names of the configuration columns in the test file
            input_columns: names of the input columns
            csv_headers: column headers of the test data

        Returns:
            Tensor created from the configuration prediction
        """
        prediction_list = [0 for _ in range(len(target_labels))]
        for key in prediction.keys():
            index = csv_headers.index(key) - len(input_columns)
            prediction_list[index] = prediction[key]
        return torch.IntTensor(prediction_list)

    @staticmethod
    def _getNetworkInfo(client: ModelDBClient,
                        network_trained_info: NetworkTrainedInfo) -> Tuple[int, List[str], float, int]:
        """Loads network information from model DB and the environmental variables

        Args:
            client: Model DB Client
            network_trained_info: information pass to the controller in the request

        Returns:
            prediction step, list of input column names, maximal utility error and the sequence length of input
        """
        pred_step = 0
        input_columns = []
        max_utility_error = 0.0
        seq_len = 0
        try:
            pred_step = int(os.environ['PREDICTION_HORIZON'])
            document = client.loadNetworkInfo(network_trained_info.application_id)
            input_columns = document["input_columns"]
            max_utility_error = document["max_utility_error"]
            seq_len = document["input_sequence_len"]
        except (KeyError, ValueError):
            logging.error("Could not load network information from Model DB")
            raise

        return pred_step, input_columns, max_utility_error, seq_len

    @staticmethod
    def _downloadFilesFromModelDB(client: ModelDBClient,
                                  application_id: str,
                                  cp_path: Path,
                                  camel_path: Path,
                                  nodes_path: Path,
                                  test_data_file_path) -> None:
        """Exports necessary files from ModelDB to the file system.

        Args:
            client: Model DB client
            application_id: unique id of the application
            cp_path: file path at which the constraint problem will be stored
            camel_path: file path at which the camel model will be stored
            nodes_path: file path at which the node candidates will be stored
            test_data_file_path: file path at which the test data will be stored

        Raises:
            KeyError: when it could find files in ModelDB for the given application id
        """
        try:
            client.exportCP(application_id, cp_path)
            client.exportCamelModel(application_id, camel_path)
            client.exportNodeCandidates(application_id, nodes_path)
            client.exportTestDataToFile(application_id, test_data_file_path)
        except KeyError:
            logging.error(
                f'Could not export files from model DB because at least one of them is not there for application id: {application_id}')
            raise

    @staticmethod
    def _parseCsvHeader(test_data_file_path: Path) -> Tuple[List[str], List[int], int, List[int]]:
        """Parses the header of the test data file to extract necessary information

        Args:
            test_data_file_path: path to file which describes the test data

        Returns:
            training data column names, indexes of columns to be used, index of the group column, indexes of columns with predictions
        """
        csv_headers = UtilityEvaluator._getCsvHeaders(test_data_file_path)
        usecols = list(range(len(csv_headers)))
        try:
            time_col = csv_headers.index("time")
            usecols.pop(time_col)
            csv_headers.pop(time_col)
        except ValueError:
            pass  # Ignore as apparently there was no "time" column in this data

        experiment_id_col = 0
        try:
            group_id_name = os.environ.get("GROUP_ID", "experimentId")
            experiment_id_col = csv_headers.index(group_id_name)
        except ValueError:
            logging.error("The test data does not have an experimentId column")
            raise

        csv_headers.pop(experiment_id_col)
        prediction_cols = [i for i, header in enumerate(csv_headers) if "PREDICTION" in header.upper()]

        return csv_headers, usecols, experiment_id_col, prediction_cols

    @staticmethod
    def _generateMetricLabelsAndPositions(csv_headers: List[str],
                                          prediction_cols: List[int],
                                          input_columns: List[str]) -> Dict[str, int]:
        """Generates a dictionary with metric names and their positions in the test data

        Args:
            csv_headers: headers of test data columns
            prediction_cols: indexes of columns containing predictions
            input_columns: names of the columns describing the input to a network

        Returns:
            dictionary with metric names and their positions in the test data
        """
        metrics_labels_and_positions = {}
        for i, header in enumerate(csv_headers):
            if i not in prediction_cols and i < len(input_columns):
                metrics_labels_and_positions[header] = i
        return metrics_labels_and_positions

    def _calculateMeanUtilityError(self,
                                   dataloader: Dataloader,
                                   input_columns: List[str],
                                   target_labels: List[str],
                                   csv_headers: List[str],
                                   network_trained_info: NetworkTrainedInfo) -> float:
        """Calculates the Mean Average Utility Error by comparing utility values of configurations
        described in the test data file with the ones predicted by supervised gym's trained model.

        Args:
            dataloader: Dataloader object for loading sequences of data from the test data file
            input_columns: names of the columns which are the input to the supervised gym's model
            target_labels: names of the columns which describe the optimal configuration
            csv_headers: names of the test data column headers
            network_trained_info: information about network training received by the controller in the request

        Returns:
            Calculated Mean Average Utility Error

        Raises:
            HTTPException: when the supervised gym returned an error message after asking it to predict a configuration
        """
        utility_error_sum = 0
        try:
            for i in range(dataloader.get_size()):
                seq, target = dataloader.getitem(i)
                input_data = UtilityEvaluator._getInputDataFromTensor(seq, input_columns, csv_headers)

                prediction = SolvingController._sendPredictionRequest(network_trained_info.application_id, input_data)
                prediction_as_tensor = UtilityEvaluator._getPredictionAsTensor(prediction, target_labels,
                                                                               input_columns, csv_headers)

                # if the prediction tensor and target tensor are the same then we know the utility error will be 0
                # so we don't have to evaluate those configurations
                if not torch.eq(prediction_as_tensor, target).all():
                    utility_error_sum += self.evaluateUtilityValueDifference(seq, prediction_as_tensor, target)

        except HTTPException as e:
            logging.error(f"The supervised gym did not predict a configuration for one of the sequences returning: {e}")
            raise
        return float(utility_error_sum) / dataloader.get_size()

    @staticmethod
    def checkUtilityFuntionMAE(network_trained_info: NetworkTrainedInfo) -> bool:
        """Checks whether the network has trained well enough.

        Args:
            network_trained_info: information about network training received by the controller in the request

        Returns:
            True if the network can be deemed as trained well or False otherwise
        """
        logging.info(f'Checking if the network has trained for application id: {network_trained_info.application_id}')
        test_data_file_path = Path(f"test-data-{network_trained_info.application_id}-utility-check")
        cp_path = Path(f'CP-{network_trained_info.application_id}-utility-check')
        camel_path = Path(f'camel-model-{network_trained_info.application_id}-utility-check')
        nodes_path = Path(f'node-candidates-{network_trained_info.application_id}-utility-check')

        try:
            client = ModelDBClient()
            pred_step, input_columns, max_utility_error, seq_len = UtilityEvaluator._getNetworkInfo(client, network_trained_info)
            UtilityEvaluator._downloadFilesFromModelDB(client, network_trained_info.application_id,
                                                       cp_path,
                                                       camel_path,
                                                       nodes_path,
                                                       test_data_file_path)
            csv_headers, usecols, experiment_id_col, prediction_cols = UtilityEvaluator._parseCsvHeader(
                test_data_file_path)
            metrics_labels_and_positions = UtilityEvaluator._generateMetricLabelsAndPositions(csv_headers,
                                                                                              prediction_cols,
                                                                                              input_columns)
            target_labels = csv_headers[len(input_columns):]

            dataloader = Dataloader(seq_len=seq_len,
                                    pred_step=pred_step,
                                    file=test_data_file_path,
                                    usecols=usecols,
                                    experiment_id_col=experiment_id_col,
                                    x_y_split=len(input_columns),
                                    x_predictions_cols=prediction_cols)
            if dataloader.get_size() is 0:
                logging.warning(
                    f'The test dataset for application id: {network_trained_info.application_id} is empty, so skipping utility testing. ' +
                    f'Omit this warning if utility testing was disabled on purpose.')
                return True

            evaluator = UtilityEvaluator(metrics_labels_and_positions=metrics_labels_and_positions,
                                         target_labels=target_labels,
                                         cp_model_file_path=cp_path.absolute().as_posix(),
                                         camel_model_file_path=camel_path.absolute().as_posix(),
                                         node_candidates_file_path=nodes_path.absolute().as_posix())
            mean_utility_error = evaluator._calculateMeanUtilityError(dataloader, input_columns,
                                                                      target_labels, csv_headers,
                                                                      network_trained_info)

            if mean_utility_error > max_utility_error:
                logging.info(
                    f'The trained model does NOT predict within error threshold for application id: {network_trained_info.application_id}\n' +
                    f'Mean utility error: {mean_utility_error}, Error threshold: {max_utility_error}')
                return False

        except (KeyError, HTTPException):
            logging.error(f'An error occurred while checking the utility function MAE.')
            return False
        finally:
            test_data_file_path.unlink(missing_ok=True)
            cp_path.unlink(missing_ok=True)
            camel_path.unlink(missing_ok=True)
            nodes_path.unlink(missing_ok=True)

        logging.info(
            f'Successfully validated that trained model predicts within error threshold for application id: {network_trained_info.application_id}\n' +
            f'Mean utility error: {mean_utility_error}, Error threshold: {max_utility_error}')
        return True
