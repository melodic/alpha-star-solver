import logging
import os
from pathlib import Path
from uuid import uuid1

import requests
from jnius import JavaException
from modeldb.ModelDBClient import ModelDBClient, TrainingStatus

from .cdoAdapter.cdoAdapter import CDOAdapter
from .models.trainModels import TrainInfo, TrainFromFileInfo, DataGenerationResult, NetworkTrainedInfo, JobResult
from .nodeCandidatesCacheAdapter.nodeCandidatesCacheAdapter import NodeCandidatesCacheAdapter
from .utilityEvaluator.UtilityEvaluator import UtilityEvaluator

DEFAULT_TRAIN_TO_TEST_RATIO = 0.85

class TrainingController:
    """
    With the use of the Training Data Generator and Supervised Learning Gym,
    it trains the network.
    """

    @staticmethod
    async def TrainfromFile(train_from_file_info: TrainFromFileInfo) -> None:
        """Schedules network training while extracting models from provided files.
        
        Args:
            train_from_file_info: training information passed in the request to the controller
        """
        try:
            if train_from_file_info.force_retrain is False:
                TrainingController._checkTheTrainingStatus(train_from_file_info.application_id)

            cp_cdo_path = train_from_file_info.application_id + "-SLSolver-" + str(uuid1())
            TrainingController._saveTrainInfoToModelDBandCDO(train_from_file_info.application_id,
                                                             Path(train_from_file_info.cp_file_path),
                                                             Path(train_from_file_info.camel_model_file_path),
                                                             Path(train_from_file_info.node_candidates_file_path),
                                                             cp_cdo_path)

            TrainingController._sendRequestForDataGeneration(train_from_file_info.application_id,
                                                             train_from_file_info.proc_finished_url,
                                                             train_from_file_info.max_utility_error,
                                                             cp_cdo_path)

        except WrongStatusForTrainingException:
            TrainingController._notifyTrainingFailed(
                train_from_file_info.application_id,
                train_from_file_info.proc_finished_url,
                "Cannot handle the train request because model is well trained or training is already underway.\n" +
                "Forcing retraining the network can be done by setting force_retrain to True.\n" +
                "But be wary that forcing retraining when another training is in progress may lead to undefined behaviour.")
            return
        except (FileNotFoundError, CDOException):
            TrainingController._notifyTrainingFailed(
                train_from_file_info.application_id,
                train_from_file_info.proc_finished_url,
                "Failed importing files from file system to Model DB and CDO")
            return
        except Exception as e:
            logging.error(f"{train_from_file_info.application_id}: Error occurred: {e}")
            TrainingController._notifyTrainingFailed(
                train_from_file_info.application_id,
                train_from_file_info.proc_finished_url,
                "Failed the training due to unknown error.")
            raise

    @staticmethod
    def requestNetworkTraining(data_generated_info: DataGenerationResult) -> None:
        """Schedules network training by the supervised gym.

        Args:
            data_generated_info: information passed in the request to the controller
        """
        client = ModelDBClient()
        document = client.loadTrainingStatus(application_id=data_generated_info.dataGenerationInfo.applicationId)
        proc_finished_url = document["proc_finished_url"]

        try:
            if data_generated_info.result == JobResult.ok:
                TrainingController._sendRequestForNetworkTraining(data_generated_info.dataGenerationInfo.applicationId)
            else:
                TrainingController._notifyTrainingFailed(data_generated_info.dataGenerationInfo.applicationId, proc_finished_url,
                                                         "Error occurred during training data generation")
        except Exception as e:
            logging.error(f"{data_generated_info.dataGenerationInfo.applicationId}: Error occurred: {e}")
            TrainingController._notifyTrainingFailed(data_generated_info.dataGenerationInfo.applicationId,
                                                     proc_finished_url,
                                                     "Error occurred while requesting network training.")
            raise

    @staticmethod
    async def checkTrainingEffects(network_trained_info: NetworkTrainedInfo) -> None:
        """Checks whether the network has trained well enough.
        
        Args:
            network_trained_info: information passed in the request to the controller
        """
        client = ModelDBClient()
        document = client.loadTrainingStatus(application_id=network_trained_info.application_id)
        proc_finished_url = document["proc_finished_url"]

        try:
            if network_trained_info.status == "successful":
                if UtilityEvaluator.checkUtilityFuntionMAE(network_trained_info):
                    TrainingController._notifyTrainingSuccess(network_trained_info.application_id,
                                                              proc_finished_url,
                                                              "Network has successfully trained")
                else:
                    TrainingController._notifyTrainingFailed(network_trained_info.application_id,
                                                             proc_finished_url,
                                                             "Network did not pass the max utility error threshold test.")

            else:
                TrainingController._notifyTrainingFailed(network_trained_info.application_id,
                                                         proc_finished_url,
                                                         "Error occurred during network training")
        except Exception as e:
            logging.error(f"{network_trained_info.application_id}: Error occurred: {e}")
            TrainingController._notifyTrainingFailed(network_trained_info.application_id,
                                                     proc_finished_url,
                                                     "Error occurred while checking training effects")
            raise

    @staticmethod
    def transformToTrainFromFileInfo(train_info: TrainInfo) -> TrainFromFileInfo:
        """Downloads models to files transforming the given train information to TrainFromFileInfo
        
        Args:
            train_info: training information describing from where to extract necessary models

        Returns:
            Train from file information describing file paths at which the models are stored.

        Raises:
            KeyError: when the controller could not download files from the given paths
        """
        cp_path = Path(f'CP-{train_info.application_id}')
        camel_path = Path(f'camel-model-{train_info.application_id}')
        nodes_path = Path(f'node-candidates-{train_info.application_id}')

        try:
            TrainingController._downloadModelsToFileSystem(train_info, cp_path, camel_path, nodes_path)
        except (CDOException, KeyError):
            TrainingController._notifyTrainingFailed(
                train_info.application_id,
                train_info.proc_finished_url,
                "Failed while downloading files from CDO and Memcache")
            raise KeyError

        train_from_file_info = TrainFromFileInfo(application_id=train_info.application_id,
                                                 cp_file_path=cp_path.absolute().as_posix(),
                                                 camel_model_file_path=camel_path.absolute().as_posix(),
                                                 node_candidates_file_path=nodes_path.absolute().as_posix(),
                                                 proc_finished_url=train_info.proc_finished_url,
                                                 max_utility_error=train_info.max_utility_error,
                                                 force_retrain=train_info.force_retrain)
        return train_from_file_info

    @staticmethod
    def _checkTheTrainingStatus(application_id: str) -> None:
        """Checks if the training status allows for a network to be trained for this application_id

        Args:
            application_id: unique id of the application

        Raises:
            WrongStatusForTrainingException: when the model for this application id is either trained or undergoing training
        """
        client = ModelDBClient()
        try:
            train_status = client.loadTrainingStatus(application_id=application_id)["status"]
        except KeyError:
            return  # It means this is the first time we want to train application of this id, so its fine

        if train_status is not TrainingStatus.FAILED:
            # There is a trained network for this application_id or another train request is underway
            raise WrongStatusForTrainingException

    @staticmethod
    def _saveTrainInfoToModelDBandCDO(application_id: str,
                                      cp_path: Path,
                                      camel_path: Path,
                                      nodes_path: Path,
                                      cp_cdo_path: str) -> None:
        """Imports the models to the Model DB and CDO databases to be later used by other services.

        Args:
            application_id: unique id of the application
            cp_path: file path at which the constraint problem is stored
            camel_path: file path at which the camel model is stored
            nodes_path: file path at which the node candidates are stored
            cp_cdo_path: path in CDO under which the constraint problem will be stored

        Raises:
            FileNotFoundError: when it could not import files to model DB
            CDOException: when it failed to save the constraint problem to CDO under the key cp_cdo_path
        """
        logging.info(
            f'{application_id}: Saving CP, camel model and node candidates to modelDB and CDO')
        client = ModelDBClient()
        try:
            client.importCP(application_id, cp_path)
            client.importCamelModel(application_id, camel_path)
            client.importNodeCandidates(application_id, nodes_path)
            if CDOAdapter.importResourceFromFile(cp_path, cp_cdo_path) is False:
                raise CDOException
        except FileNotFoundError:
            logging.error(
                f'{application_id}: Could not import files to model DB because at least one of them does not exist: '
                f'{cp_path.absolute().as_posix()} {camel_path.absolute().as_posix()} {nodes_path.absolute().as_posix()}')
            raise
        finally:
            cp_path.unlink(missing_ok=True)
            camel_path.unlink(missing_ok=True)
            nodes_path.unlink(missing_ok=True)

    @staticmethod
    def _downloadModelsToFileSystem(train_info: TrainInfo,
                                    cp_path: Path,
                                    camel_path: Path,
                                    nodes_path: Path) -> None:
        """Downloads the models from CDO and Memcache into the filesystem at specified paths

        Args:
            train_info: training information describing from where to extract necessary models
            cp_path: file path at which the constraint problem will be stored
            camel_path: file path at which the camel model will be stored
            nodes_path: file path at which the node candidates will be stored

        Raises:
            CDOException: when there was an error with downloading recources from CDO
            KeyError: when it could not download the node candidates from Memcache
        """
        logging.info(
            f'{train_info.application_id}: Downloading Constraint Problem, Camel model and Node Candidates')
        download_successful = True
        try:
            if not CDOAdapter.exportResourceToFile(train_info.cp_cdo_path, cp_path):
                download_successful = False
                logging.error(
                    f'Could not download Constraint Problem from CDO for the given key: {train_info.cp_cdo_path}')

            if not CDOAdapter.exportResourceToFile(train_info.camel_model_cdo_path, camel_path):
                download_successful = False
                logging.error(
                    f'Could not download Camel Model from CDO for the given key: {train_info.camel_model_cdo_path}')
        except JavaException:
            logging.error('Error occurred while downloading resources from CDO')
            download_successful = False

        if download_successful is False:
            cp_path.unlink(missing_ok=True)
            camel_path.unlink(missing_ok=True)
            raise CDOException

        cache_key = CDOAdapter.createCacheKey(train_info.cp_cdo_path)
        if not NodeCandidatesCacheAdapter.exportNodeCandidatesToFile(cache_key,
                                                                     nodes_path):
            logging.error(f'Could not download Node Candidates from Memcache for the given key: {cache_key}')
            cp_path.unlink(missing_ok=True)
            camel_path.unlink(missing_ok=True)
            nodes_path.unlink(missing_ok=True)
            raise KeyError(cache_key)


    @staticmethod
    def _sendRequestForNetworkTraining(application_id: str) -> None:
        """Sends a request to the supervised gym to begin model training for this application id

        Args:
            application_id: unique id of the application
        """
        logging.info(f'{application_id}: Ordering supervised gym to start network training')

        gym_host = os.environ['SUPERVISED_GYM_HOST']
        gym_port = os.environ['SUPERVISED_GYM_PORT']
        controller_host = os.environ['CONTROLLER_HOST']
        controller_port = os.environ['CONTROLLER_PORT']
        client = ModelDBClient()
        document = client.loadTrainingStatus(application_id=application_id)
        proc_finished_url = document["proc_finished_url"]

        r = requests.post(f'http://{gym_host}:{gym_port}/scheduleTraining', json={
            "application_id": application_id,
            "proc_finished_uri": f'http://{controller_host}:{controller_port}/networkTrained'})

        if r.status_code >= 400:
            TrainingController._notifyTrainingFailed(
                application_id,
                proc_finished_url,
                f'Data generator returned error response: {r.text} while requesting training data generation')
            return

        client.updateTrainingStatus(application_id=application_id,
                                    status=TrainingStatus.IN_NETWORK_TRAINING)

    @staticmethod
    def _sendRequestForDataGeneration(application_id: str,
                                      proc_finished_url: str,
                                      max_utility_error: float,
                                      cp_cdo_path: str) -> None:
        """Sends a request to the data generator to begin generating training data for this application id

        Args:
            application_id: unique id of the application
            proc_finished_url: url which waits for notification after the training is finished
            max_utility_error: maximal MAE error of the predicted configurations to deem a trained network successful
            cp_cdo_path: path in CDO which contains the constraint problem which will be used by the data generator

        Raises:
            KeyError: when one of the required environmental variables was not set
        """
        logging.info(
            f'{application_id}: Ordering data generator to start data training')

        data_generator_host = os.environ['DATA_GENERATOR_HOST']
        data_generator_port = os.environ['DATA_GENERATOR_PORT']
        controller_host = os.environ['CONTROLLER_HOST']
        controller_port = os.environ['CONTROLLER_PORT']
        train_to_test_ratio = 0
        try:
            data_split = os.environ["DATA_SPLIT"].split(",")
            test_percentage = int(data_split[2])
            train_to_test_ratio = float(100 - test_percentage)/100
        except KeyError:
            # the variable wasn't set so set the default train ratio
            logging.info(
                f"{application_id}: The DATA_SPLIT environmental variable wasn't set so setting the default train-test ratio: {DEFAULT_TRAIN_TO_TEST_RATIO}")
            train_to_test_ratio = DEFAULT_TRAIN_TO_TEST_RATIO
        timestamp_difference = float(os.environ['TIMESTAMP_DIFFERENCE'])
        r = requests.post(f"http://{data_generator_host}:{data_generator_port}/generate", json={
            "applicationId": application_id,
            "trainRatio": train_to_test_ratio,
            "cdoProblemPath": cp_cdo_path,
            "returnEndpoint": f"http://{controller_host}:{controller_port}/dataGenerated",
            "delta": timestamp_difference
        })

        if r.status_code >= 400:
            TrainingController._notifyTrainingFailed(
                application_id,
                proc_finished_url,
                f'Data generator returned error response: {r.text} while requesting training data generation')
            return

        client = ModelDBClient()
        client.updateTrainingStatus(application_id=application_id,
                                    status=TrainingStatus.IN_DATA_GENERATION,
                                    proc_finished_url=proc_finished_url)
        client.updateNetworkInfo(application_id=application_id,
                                 max_utility_error=max_utility_error)


    @staticmethod
    def _notifyTrainingFailed(application_id: str,
                              proc_finished_url: str,
                              msg: str) -> None:
        """Notifies at the given url that the training has failed.

        Args:
            application_id: unique id of the application
            proc_finished_url: url at which the notification will be sent
            msg: message describing the network status
        """
        logging.error(f'{application_id}: {msg}')
        ModelDBClient().updateTrainingStatus(application_id=application_id,
                                             status=TrainingStatus.FAILED)
        if proc_finished_url != "":
            requests.post(proc_finished_url, json={
                "application_id": application_id,
                "status": "failed"
            })
        return

    @staticmethod
    def _notifyTrainingSuccess(application_id: str,
                               proc_finished_url: str,
                               msg: str) -> None:
        """Notifies at the given url that the training has successfully finished.

        Args:
            application_id: unique id of the application
            proc_finished_url: url at which the notification will be sent
            msg: message describing the network status
        """
        logging.info(f'{application_id}: {msg}')
        ModelDBClient().updateTrainingStatus(application_id=application_id,
                                             status=TrainingStatus.SUCCESS)
        requests.post(proc_finished_url, json={
            "application_id": application_id,
            "status": "successful"
        })
        return


class WrongStatusForTrainingException(Exception):
    """Signifies that the network training status is improper to perform training 
    """
    pass


class CDOException(Exception):
    """Signifies that something went wrong while communicating with CDO
    """
    pass
