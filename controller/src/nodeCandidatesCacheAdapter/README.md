#Node Candidates Cache Adapter

Adapter utilizing a java library for saving Node Candidates to cache and obtaining it from there as well.

It requires setting environmental variables:
- CLASSPATH: a path to a compiled java library with MemcacheAdapter (and its dependencies) from ./java-src directory.
- MEMCACHE_HOST: host on which Memchache server listens.
- MEMCACHE_PORT: port on which Memchache server listens.

#Running Memcache Server

For testing purposes you can use command ``docker-compose up`` in directory dockerMemcacheServer to start the Memcache server application.
