package eu.melodic.upperware.utilitygenerator.node_candidates;

import eu.melodic.cache.NodeCandidates;
import eu.melodic.cache.exception.CacheException;
import eu.melodic.cache.impl.MemcacheServiceImpl;
import eu.melodic.cache.properties.CacheProperties;
import net.spy.memcached.MemcachedClient;

import java.io.IOException;
import java.net.InetSocketAddress;

public class MemcacheAdapter {
    private MemcacheServiceImpl service;

    public MemcacheAdapter(String host, Integer port, Integer ttl, Integer timeBetweenLoadAttempts, Integer numberOfLoadAttempts) throws IOException {
        CacheProperties cacheProperties = new CacheProperties();
        CacheProperties.Cache cache = new CacheProperties.Cache();
        cache.setHost(host);
        cache.setPort(port);
        cache.setTtl(ttl);
        cache.setTimeBetweenLoadAttempts(timeBetweenLoadAttempts);
        cache.setNumberOfLoadAttempts(numberOfLoadAttempts);
        cacheProperties.setCache(cache);

        MemcachedClient client = new MemcachedClient(new InetSocketAddress(host, port));
        service = new MemcacheServiceImpl(cacheProperties, client);
    }

    /*
     * This method is used to store Node Candidates object to the cache under a specific key.
     * The input parameters provided are the key and the NodeCandidates object to be stored.
     * The method throws an exception if it could not store the object in the cache.
     */
    public void store(String key, NodeCandidates value) throws CacheException {
        service.store(key, value);
    }

    /*
     * This method is used to load Node Candidates object from the cache under a specific key.
     * The input parameter provided is the key under which the NodeCandidates object is stored.
     * The method returns a NodeCandidates object or null if it could not load the object.
     */
    public NodeCandidates load(String key) {
        return service.load(key);
    }
}
