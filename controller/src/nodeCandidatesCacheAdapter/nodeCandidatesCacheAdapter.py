from jnius import autoclass, cast, JavaException
from pathlib import Path
import os

TTL = 86400
TIME_BETWEEN_LOAD_ATTEMPTS = 2
NUMBER_OF_LOAD_ATTEMPTS = 3
DEFAULT_MEMCACHE_HOST: str = "localhost"
DEFAULT_MEMCACHE_PORT: int = 11211
MemcacheAdapter = autoclass("eu.melodic.upperware.utilitygenerator.node_candidates.MemcacheAdapter")
FilecacheService = autoclass("eu.melodic.cache.impl.FilecacheService")


class NodeCandidatesCacheAdapter:
    """Adapter for utilizing a java library for caching Node Candidates.

    Requires environmental variables:
        CLASSPATH: a path to a compiled java library with MemcacheAdapter (and its dependencies) from ./java-src directory.
        MEMCACHE_HOST: host on which Memchache server listens.
        MEMCACHE_PORT: port on which Memchache server listens.
    """

    @staticmethod
    def _createMemcacheAdapter() -> MemcacheAdapter:
        """Creates a memcache adapter with values from environmental variables if they were defined or default values otherwise.

        Returns:
            Memcache Adapter object for loading and storing node candidates to Memcache.
        """
        try:
            return MemcacheAdapter(os.getenv('MEMCACHE_HOST', DEFAULT_MEMCACHE_HOST),
                                   int(os.getenv('MEMCACHE_PORT', DEFAULT_MEMCACHE_PORT)),
                                   TTL, TIME_BETWEEN_LOAD_ATTEMPTS, NUMBER_OF_LOAD_ATTEMPTS)
        except JavaException as err:
            print("Error occurred while creating Memcache Adapter")
            print(err)
        return None

    @staticmethod
    def exportNodeCandidatesToFile(node_candidates_cache_key: str,
                                   file_path: Path) -> bool:
        """This method exports Node Candidates stored under a particular key into the file system
        at a specific path.
        
        Args:
            node_candidates_cache_key: the Node Candidates cache key from which to retrieve the model
            file_path: the path in the file system to store the Node Candidates.
        
        Returns:
            True or False depending on whether the Node Candidates exist and have been successfully saved in the file system.
        """
        filecache_service = FilecacheService()
        memcache_adapter = NodeCandidatesCacheAdapter._createMemcacheAdapter()
        if memcache_adapter is None:
            return False

        node_candidates = memcache_adapter.load(node_candidates_cache_key)

        try:
            filecache_service.store(file_path.absolute().as_posix(), node_candidates)
        except JavaException as err:
            print("Error occurred while storing the Node Candidates")
            print(err)
            return False
        return True

    @staticmethod
    def importNodeCandidatesFromFile(file_path: Path,
                                     node_candidates_cache_key: str) -> bool:
        """This method is used to immediately store Node Candidates from the file system to
        the cache under a specific key.
        
        Args:
            file_path: the path at the file system where the Node Candidates reside
            node_candidates_cache_key: the cache key under which Node Candidates will be stored.
            
        Returns:
            True or False depending on whether Node Candidates have been successfully loaded in and then stored in the cache.
        """
        filecache_service = FilecacheService()
        memcache_adapter = NodeCandidatesCacheAdapter._createMemcacheAdapter()
        if memcache_adapter is None:
            return False

        node_candidates = filecache_service.load(file_path.absolute().as_posix())
        try:
            memcache_adapter.store(node_candidates_cache_key, node_candidates)
        except JavaException as err:
            print("Error occurred while storing the Node Candidates")
            print(err)
            return False
        return True
