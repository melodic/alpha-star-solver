#CDO Adapter

Adapter for utilizing a java library for communication with the CDO database.

It requires setting environmental variables:
- CLASSPATH: a path to a compiled java library with CDOAdapter (and its dependencies) from ./java-src directory.
- PAASAGE_CONFIG_DIR: a path to cdo config files from .paasage directory
- esb.url: url of the Enterprise Service Bus

#Running CDO Server

For testing purposes you can use command ``docker-compose up`` in directory dockerCDOServer to start the CDO server application.