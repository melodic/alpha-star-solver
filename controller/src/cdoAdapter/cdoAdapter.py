import os
import logging
from numbers import Number
from pathlib import Path
from typing import Dict

from jnius import autoclass, JavaException

CDOAdapterClass = autoclass("eu.melodic.upperware.utilitygenerator.cdo.CDOAdapter")
ArrayList = autoclass("java.util.ArrayList")


class CDOAdapter:
    """Adapter for utilizing a java library for communication with the CDO database.

    Requires environmental variables:
        CLASSPATH: a path to a compiled java library with CDOAdapter (and its dependencies) from ./java-src directory.
        PAASAGE_CONFIG_DIR: a path to cdo config files from .paasage directory
        esb.url: url of the Enterprise Service Bus
    """

    @staticmethod
    def exportResourceToFile(resource_CDO_path: str,
                             resource_file_path: Path) -> bool:
        """This method exports a CDO model stored in a particular resource path into the file
        system at a specific path.

        Args:
            resource_CDO_path: the CDO resource path from which to retrieve the model
            resource_file_path: the path in the file system to store the model

        Returns:
            True or False depending on whether the resource exists and has been successfully saved in the file system.

        Raises:
            jnius.JavaException: when it could not find node at given resource_CDO_path
        """
        return CDOAdapterClass.exportResourceToFile(resource_CDO_path, resource_file_path.absolute().as_posix())

    @staticmethod
    def importResourceFromFile(resource_file_path: Path,
                               resource_CDO_path: str) -> bool:
        """This method is used to immediately store a model from the file system to the CDO
        Repository at a specific resource path.

        Args:
            resource_file_path: path at the file system where the model resides
            resource_CDO_path: the CDO resource path on which the model will be stored

        Returns:
            True or False depending on whether the model has been successfully loaded in main memory
            and then stored in the CDO Repository.

        Raises:
            jnius.JavaException: when it could not find a file under the given resource_file_path or the file
            contains data which is not supported by the CDO
        """
        return CDOAdapterClass.importResourceFromFile(resource_file_path.absolute().as_posix(), resource_CDO_path)

    @staticmethod
    def saveSolutionToCDO(solution: Dict[str, Number],
                          cp_CDO_path: str,
                          camel_model_file_path: Path,
                          cp_file_path: Path,
                          node_candidates_file_path: Path,
                          application_id: str,
                          notification_uri: str,
                          request_uuid: str) -> None:
        """This method saves the given solution to CDO and notifies ESB that a solution has been produced.

        Args:
            solution: dictionary containing pairs (name, value) describing a solution to the CP
            cp_CDO_path: the path of CDO resource containing the CP
            camel_model_file_path: the path to the file containing the CAMEL model
            cp_file_path: the path to the file containing the CP
            node_candidates_file_path: the path to the file containing node candidates
            application_id: unique id of the application
            notification_uri: uri at which ESB will be notified that a solution has been produced
            request_uuid: uuid of the request

        Raises:
            jnius.JavaException: when it could not find node at given resource_CDO_path or the files specified in the
            parameters does not contain valid data
            KeyError: when the esb.url environmental variable has not been set
        """
        variable_list = ArrayList()
        for (name, value) in solution.items():
            variable_list.add(CDOAdapterClass.createVariableValueDTO(name, round(value)))

        esb_url = os.environ['esb.url']

        CDOAdapterClass.saveSolutionToCDO(variable_list, cp_CDO_path, camel_model_file_path.absolute().as_posix(),
                                          cp_file_path.absolute().as_posix(),
                                          node_candidates_file_path.absolute().as_posix(),
                                          application_id, notification_uri, request_uuid, esb_url)

    @staticmethod
    def notifySolutionNotApplied(application_id: str,
                                 notification_uri: str,
                                 uuid: str) -> None:
        """Notifies the ESB that the solution has not been applied

        Args:
            application_id: unique id of the application
            notification_uri: uri of the ESB at which a notification will be sent
            uuid: request uuid which will be passed in the notification to ESB
        """
        try:
            esb_url = os.environ['esb.url']
            CDOAdapterClass.notifySolutionNotApplied(application_id, notification_uri, uuid, esb_url)
        except (KeyError, JavaException) as e:
            logging.error(f"Failed to notify ESB about not applying the solution.")

    @staticmethod
    def createCacheKey(cdo_cp_path: str) -> str:
        """Calculates and returns the cache key for node candidates based on the constraint problem CDO path

        Args:
            cdo_cp_path: path in the CDO for the constraint problem

        Returns:
            The calculated cache key
        """
        return CDOAdapterClass.createCacheKey(cdo_cp_path)
