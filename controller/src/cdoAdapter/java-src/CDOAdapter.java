package eu.melodic.upperware.utilitygenerator.cdo;

import eu.melodic.models.commons.NotificationResult;
import eu.melodic.models.commons.NotificationResultImpl;
import eu.melodic.models.commons.Watermark;
import eu.melodic.models.commons.WatermarkImpl;
import eu.melodic.models.services.cpSolver.ConstraintProblemSolutionNotificationRequest;
import eu.melodic.models.services.cpSolver.ConstraintProblemSolutionNotificationRequestImpl;
import eu.melodic.upperware.utilitygenerator.ASUtilityGeneratorApplication;
import eu.melodic.upperware.utilitygenerator.cdo.cp_model.DTO.VariableValueDTO;
import eu.melodic.upperware.utilitygenerator.cdo.cp_model.DTO.VariableValueDTOFactory;
import eu.paasage.mddb.cdo.client.CDOClient;
import eu.paasage.mddb.cdo.client.exp.CDOClientX;
import eu.paasage.mddb.cdo.client.exp.CDOClientXImpl;
import eu.paasage.mddb.cdo.client.exp.CDOSessionX;
import eu.paasage.upperware.metamodel.cp.*;
import eu.paasage.upperware.metamodel.types.*;
import eu.passage.upperware.commons.model.tools.CPModelTool;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CommitException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

import static eu.melodic.models.commons.NotificationResult.StatusType.ERROR;
import static eu.melodic.models.commons.NotificationResult.StatusType.SUCCESS;
import static eu.passage.upperware.commons.model.tools.CPModelTool.*;

@Slf4j
//@Service
public class CDOAdapter {

    private static void saveBestSolutionInCDO(ConstraintProblem cp, double maxUtility, List<VariableValueDTO> bestSolution) {
        log.info("Saving best solution in CDO.....");

        if (!isInitialDeployment(cp)) {
            updateUtilityOfDeployedSolution(cp);
        }

        List<CpVariableValue> values = cp.getCpVariables()
                .stream()
                .map(var -> createCpVariableValue(bestSolution, var))
                .collect(Collectors.toList());

        log.info("Solution with best utility {}:", maxUtility);
        for (VariableValueDTO variableValueDTO : bestSolution) {
            log.info("\t{}: {}", variableValueDTO.getName(), variableValueDTO.getValue());
        }

        cp.getSolution().add(createSolution(maxUtility, values));
    }

    private static CpVariableValue createCpVariableValue(List<VariableValueDTO> bestSolution, CpVariable var) {
        log.debug("Considering variable: {}", var.getId());
        Domain dom = var.getDomain();
        if (dom instanceof RangeDomain) {
            RangeDomain rd = (RangeDomain) dom;
            NumericValueUpperware from = rd.getFrom();
            if (from instanceof IntegerValueUpperware) {
                Number variableValue = findVariableValue(bestSolution, var);
                return CPModelTool.createCpVariableValue(var, createIntegerValueUpperware(variableValue.intValue()));
            } else if (from instanceof LongValueUpperware) {
                Number variableValue = findVariableValue(bestSolution, var);
                return CPModelTool.createCpVariableValue(var, createLongValueUpperware(variableValue.longValue()));
            } else if (from instanceof DoubleValueUpperware) {
                Number variableValue = findVariableValue(bestSolution, var);
                return CPModelTool.createCpVariableValue(var, createDoubleValueUpperware(variableValue.doubleValue()));
            } else {
                Number variableValue = findVariableValue(bestSolution, var);
                return CPModelTool.createCpVariableValue(var, createFloatValueUpperware(variableValue.floatValue()));
            }
        } else if (dom instanceof NumericDomain) {
            NumericDomain nd = (NumericDomain) dom;
            BasicTypeEnum type = nd.getType();
            if (type.equals(BasicTypeEnum.INTEGER)) {
                Number variableValue = findVariableValue(bestSolution, var);
                return CPModelTool.createCpVariableValue(var, createIntegerValueUpperware(variableValue.intValue()));
            } else if (type.equals(BasicTypeEnum.LONG)) {
                Number variableValue = findVariableValue(bestSolution, var);
                return CPModelTool.createCpVariableValue(var, createLongValueUpperware(variableValue.longValue()));
            } else if (type.equals(BasicTypeEnum.DOUBLE)) {
                Number variableValue = findVariableValue(bestSolution, var);
                return CPModelTool.createCpVariableValue(var, createDoubleValueUpperware(variableValue.doubleValue()));
            } else {
                Number variableValue = findVariableValue(bestSolution, var);
                return CPModelTool.createCpVariableValue(var, createFloatValueUpperware(variableValue.floatValue()));
            }
        }
        throw new RuntimeException("Unsupported method type: " + dom.getClass());
    }

    private static Number findVariableValue(List<VariableValueDTO> bestSolution, CpVariable var) {
        return bestSolution
                .stream()
                .filter(variableValueDTO -> variableValueDTO.getName().equals(var.getId()))
                .findFirst().orElseThrow(() -> new RuntimeException("Could not find VariableValue for " + var.getId()))
                .getValue();

    }

    private static void updateUtilityOfDeployedSolution(ConstraintProblem cp) {
        log.debug("Updating utility of deployed solution = 0.0");
        Solution deployedSolution = cp.getSolution().get(cp.getDeployedSolutionId());
        log.debug("Previous utility of deployed solution was {}", ((DoubleValueUpperware) deployedSolution.getUtilityValue()).getValue());
        deployedSolution.setUtilityValue(createDoubleValueUpperware(0.0));
    }

    private static Optional<ConstraintProblem> getCPFromCDO(String pathName, CDOTransaction trans) {
        CDOResource resource = trans.getResource(pathName);
        return resource.getContents()
                .stream()
                .filter(eObject -> eObject instanceof ConstraintProblem)
                .map(eObject -> (ConstraintProblem) eObject)
                .findFirst();
    }

    public static void notifySolutionProduced(String camelModelID, String notificationUri, String uuid, String esbUrl) {
        log.info("Sending solution available notification");
        NotificationResult result = prepareSuccessNotificationResult();
        ConstraintProblemSolutionNotificationRequest notification = prepareNotification(camelModelID, result, uuid);
        sendNotification(notification, notificationUri, esbUrl);
    }

    public static void notifySolutionNotApplied(String camelModelID, String notificationUri, String uuid, String esbUrl) {
        log.info("Sending solution NOT available notification");
        NotificationResult result = prepareErrorNotificationResult("Solution was not generated.");
        ConstraintProblemSolutionNotificationRequest notification = prepareNotification(camelModelID, result, uuid);
        sendNotification(notification, notificationUri, esbUrl);
    }

    private static void sendNotification(ConstraintProblemSolutionNotificationRequest notification, String notificationUri, String esbUrl) {
        if (esbUrl.endsWith("/")) {
            esbUrl = esbUrl.substring(0, esbUrl.length() - 1);
        }
        if (notificationUri.startsWith("/")) {
            notificationUri = notificationUri.substring(1);
        }
        try {
            log.debug("Sending notification to: {}", esbUrl);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.postForEntity(esbUrl + "/" + notificationUri, notification, String.class);
            log.debug("Notification sent.");
        } catch (RestClientException restException) {
            log.error("Error sending notification: ", restException);
        }
    }

    private static Watermark prepareWatermark(String uuid) {
        Watermark watermark = new WatermarkImpl();
        watermark.setUser("ASSolver");
        watermark.setSystem("ASSolver");
        watermark.setDate(new Date());
        watermark.setUuid(uuid);
        return watermark;
    }

    private static NotificationResult prepareSuccessNotificationResult() {
        NotificationResult result = new NotificationResultImpl();
        result.setStatus(SUCCESS);
        return result;
    }

    private static NotificationResult prepareErrorNotificationResult(String errorMsg) {
        NotificationResult result = new NotificationResultImpl();
        result.setErrorDescription(errorMsg);
        result.setStatus(ERROR);
        return result;
    }

    private static ConstraintProblemSolutionNotificationRequest prepareNotification(String camelModelID, NotificationResult result, String uuid) {
        ConstraintProblemSolutionNotificationRequest notification = new ConstraintProblemSolutionNotificationRequestImpl();
        notification.setApplicationId(camelModelID);
        notification.setResult(result);
        notification.setWatermark(prepareWatermark(uuid));
        return notification;
    }

    // Creates and returns a VariableValueDTO containing the given name and value
    public static VariableValueDTO createVariableValueDTO(String name, Number value){
        return VariableValueDTOFactory.createElement(name, value);
    }

    // Creates and returns a Node Candidates Cache key based on the cdoResourcePath
    public String createCacheKey(String cdoResourcePath) {
        return cdoResourcePath.substring(cdoResourcePath.indexOf("/") + 1);
    }

    // This method saves the given solution to CDO and notifies ESB that a solution has been produced.
    public static void saveSolutionToCDO(List<VariableValueDTO> solution, String cpCDOPath, String camelModelFilePath,
                                         String cpFilePath, String nodeCandidatesFilePath, String applicationId,
                                         String notificationUri, String requestUuid, String esbUrl){
        try {
            CDOClientX clientX = new CDOClientXImpl(Arrays.asList(TypesPackage.eINSTANCE, CpPackage.eINSTANCE));
            CDOSessionX sessionX = clientX.getSession();
            CDOTransaction trans = sessionX.openTransaction();

            ASUtilityGeneratorApplication asUtilityGeneratorApplication = new ASUtilityGeneratorApplication(camelModelFilePath, cpFilePath, nodeCandidatesFilePath);

            ConstraintProblem cp = getCPFromCDO(cpCDOPath, trans)
                    .orElseThrow(() -> new IllegalStateException("Constraint Problem does not exist in CDO"));

            double utility = asUtilityGeneratorApplication.evaluate(solution);
            if (utility > 0.0) {
                //save best solution
                saveBestSolutionInCDO(cp, utility, solution);
            } else {
                log.debug("New utility value {} is NOT greater than 0", utility);
            }

            trans.commit();
            trans.close();
            sessionX.closeSession();

            notifySolutionProduced(applicationId, notificationUri, requestUuid, esbUrl);

        } catch (CommitException e) {
            log.error("CPSolver returned exception.", e);
            notifySolutionNotApplied(applicationId, notificationUri, requestUuid, esbUrl);
        }
    }

    /* This method exports a CDO model stored in a particular resource path into the file
     * system at a specific path. The input parameters to be provided are the CDO resource
     * path from which to retrieve the model and the path in the file system to store it.
     * The method returns as output the value of true or false depending on whether the
     * resource exists and has been successfully saved in the file system.
     */
    public static boolean exportResourceToFile(String resourceCDOPath, String resourceFilePath){
        CDOClient client = new CDOClient();
        client.registerPackage(TypesPackage.eINSTANCE);
        client.registerPackage(CpPackage.eINSTANCE);

        boolean result = client.exportModel(resourceCDOPath, resourceFilePath);
        client.closeSession();

        return result;
    }

    /*
     * This method is used to immediately store a model from the file system to the CDO
     * Repository at a specific resource path. The input parameters provided are the path
     * at the file system where the model resides, the CDO resource path on which it will
     * be stored. The method returns as output true or false depending on whether the model
     * has been successfully loaded in main memory and then stored in the CDO Repository.
     */
    public static boolean importResourceFromFile(String resourceFilePath, String resourceCDOPath){
        CDOClient client = new CDOClient();
        client.registerPackage(TypesPackage.eINSTANCE);
        client.registerPackage(CpPackage.eINSTANCE);

        boolean result = client.importModel(resourceFilePath, resourceCDOPath, false);
        client.closeSession();

        return result;
    }
}