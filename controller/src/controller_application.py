import logging

from fastapi import FastAPI, BackgroundTasks, HTTPException

from .cdoAdapter.cdoAdapter import CDOAdapter
from .models.solveModels import SolveFromFileInfo, SolveInfo
from .models.trainModels import TrainInfo, TrainFromFileInfo, DataGenerationResult, NetworkTrainedInfo
from .solving_controller import SolvingController
from .training_controller import TrainingController

logging.basicConfig(level=logging.INFO)
app = FastAPI()


@app.post('/train')
async def train(train_info: TrainInfo,
                background_tasks: BackgroundTasks):
    """
    Train the network with models extracted from CDO
    """
    logging.info(f'Received train request for application id: {train_info.application_id}')
    train_from_file_info = None
    try:
        train_from_file_info = TrainingController.transformToTrainFromFileInfo(train_info)
    except KeyError:
        raise HTTPException(status_code=404,
                            detail="For this application id it couldn't download resources from CDO or Memcache")

    background_tasks.add_task(TrainingController.TrainfromFile, train_from_file_info=train_from_file_info)
    return "OK"


@app.post('/trainFromFile')
async def trainFromFile(train_from_file_info: TrainFromFileInfo,
                        background_tasks: BackgroundTasks):
    """
    Train the network with models extracted from files
    """
    logging.info(f'Received train from file request for application id: {train_from_file_info.application_id}')
    background_tasks.add_task(TrainingController.TrainfromFile, train_from_file_info=train_from_file_info)
    return "OK"


@app.post('/dataGenerated')
async def dataGenerated(data_generated_info: DataGenerationResult,
                        background_tasks: BackgroundTasks):
    """
    Endpoint for data generator to signal that data generation for a given application has finished
    """
    logging.info(
        f'Received notification that data generation has finished for application id: {data_generated_info.dataGenerationInfo.applicationId}')
    background_tasks.add_task(TrainingController.requestNetworkTraining, data_generated_info=data_generated_info)
    return "OK"


@app.post('/networkTrained')
async def networkTrained(network_trained_info: NetworkTrainedInfo,
                         background_tasks: BackgroundTasks):
    """
    Endpoint for supervised gym to signal that network training for a given application has finished
    """
    logging.info(
        f'Received notification that network training has finished for application id: {network_trained_info.application_id}')
    background_tasks.add_task(TrainingController.checkTrainingEffects, network_trained_info=network_trained_info)
    return "OK"


@app.post('/constraintProblemSolution')
async def constraintProblemSolution(solve_info: SolveInfo):
    """
    Predict configuration using a previously trained network with data extracted from Influx.
    """
    logging.info(f'Received predict configuration request for application id: {solve_info.application_id}')
    prediction = {}
    try:
        prediction = SolvingController.solve(solve_info=solve_info)
    except Exception as e:
        logging.error(f"{solve_info.application_id}: Error occurred: {e}")
        CDOAdapter.notifySolutionNotApplied(solve_info.application_id, solve_info.notification_uri, solve_info.request_uuid)
        raise
    return prediction


@app.post('/constraintProblemSolutionFromFile')
async def constraintProblemSolutionFromFile(solve_from_file_info: SolveFromFileInfo):
    """
    Predict configuration using a previously trained network with data extracted from file.
    """
    logging.info(
        f'Received predict configuration from file request for application id: {solve_from_file_info.application_id}')
    prediction = {}
    try:
        prediction = SolvingController.solveFromFile(solve_from_file_info=solve_from_file_info)
    except Exception as e:
        logging.error(f"{solve_from_file_info.application_id}: Error occurred: {e}")
        CDOAdapter.notifySolutionNotApplied(solve_from_file_info.application_id, solve_from_file_info.notification_uri, solve_from_file_info.request_uuid)
        raise
    return prediction
