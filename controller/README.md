# Controller
The controller application controls the AS Solver workflow and allows communication with it via REST.
The requests should have a JSON content with appropriate fields filled.
The required fields are also described in /src/models directory

# Rest end-points to communicate with our Solver

- [POST] /train - Train the network with models extracted from CDO
    - application_id: unique id of the application
    - cp_cdo_path: path to the constraint problem in CDO
    - camel_model_cdo_path: path to the camel model in CDO
    - proc_finished_url: URL at which a notification will be sent after training has finished
    - max_utility_error: maximal MAE error of the predicted configurations to deem a trained network successful
    - force_retrain: whether to force retraining of the model. Forcing retraining when another training is in progress may
                   lead to undefined behaviour.

- [POST] /constraintProblemSolution - Solve the constraint problem with models extracted from CDO
    - application_id: unique id of the application
    - problem_datetime: time for which the prediction will be calculated
    - cp_CDO_path: path to the constraint problem in CDO
    - notification_uri: uri of the ESB at which a notification will be sent
    - request_uuid: request uuid which will be passed in the notification to ESB
    
- [POST] /trainFromFile - Train the network with models extracted from files
    - application_id: unique id of the application
    - cp_file_path: path to the file containing constraint problem
    - camel_model_file_path: path to the file containing camel model
    - node_candidates_file_path: path to the file containing node candidates
    - proc_finished_url: URL at which a notification will be sent after training has finished
    - max_utility_error: maximal MAE error of the predicted configurations to deem a trained network successful
    - force_retrain: whether to force retraining of the model. Forcing retraining when another training is in progress may
                   lead to undefined behaviour.
    
- [POST] /constraintProblemSolutionFromFile - Solve the constraint problem with network input extracted from file
    - application_id: unique id of the application
    - input_file_path: path to the file containing the input for supervised gym
    - cp_CDO_path: path to the constraint problem in CDO
    - notification_uri: uri of the ESB at which a notification will be sent
    - request_uuid: request uuid which will be passed in the notification to ESB
    